//
//  ZonePageViewController.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 9/11/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit

class ZonePageViewController: UIViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    @IBOutlet var pageControl : UIPageControl?
    var index :Int = 0
    private let contentImages =  ["zone1",
        "zone2",
        "zone3","zone4","zone5"]
    
    private var pageViewController: UIPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPageViewController()
        //HealthManager.sharedInstance.saveWorkout()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    

    private func createPageViewController() {
        
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageViewController") as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        
        if contentImages.count > 0
        {
            let firstController = getContentController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: nil)
        }
        
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
        pageControl?.currentPage = 0
        self.view.bringSubviewToFront(pageControl!)
    
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        let itemController = viewController as! PageContentViewController
        
        index = itemController.itemIndex
        if index == 0
        {
            pageControl?.currentPage = index
        }
        
        pageControl?.currentPage = index
        index -= 1
        
        if itemController.itemIndex > 0 {
            return getContentController(itemController.itemIndex-1)
        }
        
        return nil

    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        
        let itemController = viewController as! PageContentViewController
        
        index = itemController.itemIndex
        
        pageControl?.currentPage = index
        
        if index != contentImages.count
        {
            index += 1
        }
        
        if itemController.itemIndex+1 < contentImages.count {
            return getContentController(itemController.itemIndex+1)
        }

        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController])
    {
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
        
    }
    
    //MARK: - load page content controller
    
    private func getContentController(itemIndex: Int) -> PageContentViewController? {
        if itemIndex < contentImages.count {
            if itemIndex == 0{
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageContent1") as! PageContentViewController
                pageItemController.itemIndex = itemIndex
                return pageItemController
            }
            else  if itemIndex == 1{
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageContent2") as! PageContentViewController
                pageItemController.itemIndex = itemIndex
                return pageItemController
            }
                
            else  if itemIndex == 2{
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageContent3") as! PageContentViewController
                pageItemController.itemIndex = itemIndex
                return pageItemController
            }
                
            else  if itemIndex == 3{
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageContent4") as! PageContentViewController
                pageItemController.itemIndex = itemIndex
                return pageItemController
            }
            else  if itemIndex == 4{
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("ZonePageContent5") as! PageContentViewController
                pageItemController.itemIndex = itemIndex
                return pageItemController
            }
        }
        
        return nil
    }

    
}
