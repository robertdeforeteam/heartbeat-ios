//
//  WorkoutVC.swift
//  HeartBeat
//
//  Created by Deep-MacMini on 8/19/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit
import HealthKit
import WatchConnectivity

class WorkoutVC: UIViewController , UITableViewDelegate , AddHistoryUpdateDelegate
{
    
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    var session : WCSession!
    var historyWorkout : [HKWorkout] = []
    
    @IBOutlet var historyTableView: UITableView!
    @IBOutlet var indicatorView : UIActivityIndicatorView!
    @IBOutlet var noSavedWorkoutLabel:UILabel!
    @IBOutlet var fetchingSavedWorkouts:UILabel!
    
    var timeArray : [Int] = []
    var intervalArr : [Float] = []
    
    var historyArray :[[HKWorkout]]=[];
    var monthArray : [String] = []

    var historyStructure:[[HKQuantitySample]]=[];
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        HealthManager.sharedInstance.isAnyNewWorkout = true
        historyTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        HealthManager.sharedInstance.updateHistoryDelegate = self
                 let nib = UINib(nibName: "HistoryViewCell", bundle: nil)
        historyTableView.registerNib(nib, forCellReuseIdentifier: "Cell");
        
        let nib1 = UINib(nibName: "HistoryHeaderView", bundle: nil)
        historyTableView.registerNib(nib1, forCellReuseIdentifier: "Header");
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
        HealthManager.sharedInstance.isAnyNewWorkout = true
        noSavedWorkoutLabel.hidden = true
        if HealthManager.sharedInstance.isAnyNewWorkout == true
        {
            fetchingSavedWorkouts.hidden = false
            indicatorView.hidden = false
            indicatorView.startAnimating()
            self.view.bringSubviewToFront(indicatorView)
            
            if self.historyStructure.count > 0
            {
                self.historyStructure.removeAll()
            }
            HealthManager.sharedInstance.reterieveSavedWorkouts()
            
           // HealthManager.sharedInstance.historyStartDate = NSDate()
       }
        else
        {
            let workOutArr = NSUserDefaults.standardUserDefaults().objectForKey("workouts") as? [HKWorkout]
            if(workOutArr?.count > 0 )
            {
                self.historyWorkout = workOutArr!
            }
            
            if(HealthManager.sharedInstance.lastSavedWorkOut != nil)
            {
                indicatorView.hidden = false
                indicatorView.startAnimating()
                if(self.historyWorkout.count > 0)
                {
                    self.historyWorkout.insert(HealthManager.sharedInstance.lastSavedWorkOut!, atIndex: 0)
                    HealthManager.sharedInstance.queryHeartRate.insert(true,atIndex:0)
                }
                else
                {
                    self.historyWorkout.append(HealthManager.sharedInstance.lastSavedWorkOut!)
                     HealthManager.sharedInstance.queryHeartRate.append(true)
                }
                
                let array = NSMutableArray()
                array.addObjectsFromArray(self.historyWorkout)
                
               // NSUserDefaults.standardUserDefaults().setObject(array, forKey: "workouts")
              //  NSUserDefaults.standardUserDefaults().synchronize()
            }
                var array :[HKWorkout]=[];
                self.historyArray.removeAll()
                for i in 0 ..< self.historyWorkout.count
                {
                    if i == 0
                    {
                        array.append(self.historyWorkout[i])
                    }
                    else
                    {
                        let current = self.historyWorkout[i]
                        let previous = self.historyWorkout[i-1]
                        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second,.Day,.Month,.Year]
                        
                        let components1 = NSCalendar.currentCalendar().components(unitFlags,fromDate: current.startDate)
                        let components2 = NSCalendar.currentCalendar().components(unitFlags,fromDate: previous.startDate)
                        
                        if components1.month == components2.month && components1.year == components2.year
                        {
                            array.append(current)
                        }
                        else
                        {
                            self.historyArray.append(array)
                            array.removeAll()
                            array.append(current)
                        }
                    }
                    
                    if i == self.historyWorkout.count - 1
                    {
                        self.historyArray.append(array)
                        array.removeAll()
                    }
                }
                historyTableView.reloadData();
                HealthManager.sharedInstance.lastSavedWorkOut = nil
            indicatorView.hidden = true
            indicatorView.stopAnimating()
            }
        
    }
 
    
    func historyUpdate(historyStructure:[HKWorkout])
    {
      //  print(historyStructure)
    
        if (historyStructure.count == 0){
            dispatch_async(dispatch_get_main_queue() ,{
                self.indicatorView.stopAnimating()
                self.indicatorView.hidden = true
                self.noSavedWorkoutLabel.hidden = false
                self.fetchingSavedWorkouts.hidden = true
            })
          
        } else{
            HealthManager.sharedInstance.isAnyNewWorkout = false
            
            dispatch_async(dispatch_get_main_queue() ,{
                self.fetchingSavedWorkouts.hidden = true
                self.indicatorView.stopAnimating()
                self.indicatorView.hidden = true
                if self.historyStructure.count > 0
                {
                    self.historyStructure.removeAll()
                    HealthManager.sharedInstance.queryHeartRate.removeAll()
                }
                self.historyWorkout=historyStructure;
                var array :[HKWorkout]=[];
                self.historyArray.removeAll()
                
                var queryHeartRate : [Bool] = []
                for i in 0 ..< self.historyWorkout.count
                {
                    if i == 0
                    {
                        array.append(self.historyWorkout[i])
                    }
                    else
                    {
                        let current = self.historyWorkout[i]
                        let previous = self.historyWorkout[i-1]
                        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second,.Day,.Month,.Year]
                        
                        let components1 = NSCalendar.currentCalendar().components(unitFlags,fromDate: current.startDate)
                        let components2 = NSCalendar.currentCalendar().components(unitFlags,fromDate: previous.startDate)
                        
                        if components1.month == components2.month && components1.year == components2.year
                        {
                            array.append(current)
                        }
                        else
                        {
                            self.historyArray.append(array)
                            array.removeAll()
                            array.append(current)
                        }
                    }
                    
                    if i == self.historyWorkout.count - 1
                    {
                        self.historyArray.append(array)
                        array.removeAll()
                    }
                    
                    queryHeartRate.append(false)
                }
                
                HealthManager.sharedInstance.queryHeartRate = queryHeartRate
                
                // print(historyArray.count)
                // print(historyArray)
                self.historyTableView.reloadData();

       
            })
         }
       
    }

    //MARK:- workout observer
    
   
    
 // MARK: TableView Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) ->Int
    {
        return historyArray.count
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->Int{
        //  print(historyArray[section].count)
        return historyArray[section].count;
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell : HistoryViewCell! = tableView.dequeueReusableCellWithIdentifier("Cell") as! HistoryViewCell
        if(cell == nil)
        {
            cell = NSBundle.mainBundle().loadNibNamed("Cell", owner: self, options: nil)![0] as! HistoryViewCell;
            cell.backgroundColor = UIColor.blackColor()
        }
        
        cell.contentView.userInteractionEnabled = false;
        
        let arr = historyArray[indexPath.section]
        //print(indexPath.row)
//        var arr1 : HKWorkout?
       //  print(indexPath.row)

        let arr1  = arr[indexPath.row]

        
        let timeStr = Helper.sharedInstance.zoneTimeValue(Int(arr1.duration))
      //  print(arr1.startDate)
        timeArray.append(calculateWorkoutInterval(arr1))

        cell.timeLabel?.text = timeStr as String
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let str :String = formatter.stringFromDate(arr1.startDate)
        cell.dateLabel?.text = str;
    
        return cell as HistoryViewCell
    }
    
    

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 22;
    }
   // MARK: TableView Delegate
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
      //  let header : HistoryHeaderView! = tableView.dequeueReusableCellWithIdentifier("Header") as! HistoryHeaderView
        let view = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 22))
        let label = UILabel(frame: CGRectMake(10, 5, tableView.frame.size.width, 18))
        label.font = UIFont.systemFontOfSize(14)
       
        
        let arr = historyArray[section]
        let arr1  = arr[0]
        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second,.Day,.Month,.Year]
        
        let components1 = NSCalendar.currentCalendar().components(unitFlags,fromDate:arr1.startDate)
        var monthStr = ""
        if components1.month == 1
        {
            monthStr = "Januaray";
        }
        else if components1.month == 2
        {
            monthStr = "Feburary";
        }
        else if components1.month == 3
        {
            monthStr = "March";
        }
        else if components1.month == 4
        {
            monthStr = "April";
        }
        else if components1.month == 5
        {
            monthStr = "May";
        }
        else if components1.month == 6
        {
            monthStr = "June";
        }
        else if components1.month == 7
        {
            monthStr = "July";
        }
        else if components1.month == 8
        {
            monthStr = "August";
        }
        else if components1.month == 9
        {
            monthStr = "September";
        }
        else if components1.month == 10
        {
            monthStr = "October";
        }
        else if components1.month == 11
        {
            monthStr = "November";
        }
        else if components1.month == 12
        {
            monthStr = "December";
        }
        
        label.text = monthStr
        view.addSubview(label)
        label.center = CGPointMake(label.center.x, view.center.y)
        view.backgroundColor = UIColor.grayColor() // Set your background color
        
        return view
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
      //  print("You selected cell #\(indexPath.row)!")
        
        let arr = historyArray[indexPath.section]
        let arr1  = arr[indexPath.row]
        
        let query =  HealthManager.sharedInstance.queryHeartRate[indexPath.row]
        HealthManager.sharedInstance.queryRate = query
        
        HealthManager.sharedInstance.selectedWorkOutData = arr1
        if(query == true)
        {
            HealthManager.sharedInstance.heartRateArray  = HealthManager.sharedInstance.heartRateArr[indexPath.row]
        }
        HealthManager.sharedInstance.totalWorkoutTimeInterval = Int(arr1.duration)

      //  print(HealthManager.sharedInstance.selectedWorkOutData?.startDate)
      //   print(HealthManager.sharedInstance.selectedWorkOutData?.endDate)
        
       
        Helper.sharedInstance.averageTimeHeartRate = intervalArr[indexPath.row]
        if Helper.sharedInstance.averageTimeHeartRate > 8 || Helper.sharedInstance.averageTimeHeartRate == 0
        {
            Helper.sharedInstance.averageTimeHeartRate = 3
        }

        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : HistoricalGraphController = storyboard.instantiateViewControllerWithIdentifier("HistoricalGraphController") as! HistoricalGraphController
        vc.query = query
        self.navigationController?.pushViewController(vc, animated: true);
        //self.presentViewController(vc, animated: true, completion: nil);
    }
    
    internal func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
//        if indexPath.row == historyStructure.count - 1
//        {
//            if HealthManager.sharedInstance.fetchHistoryCallAgain == true
//            {
//                fetchHistory()
//            }
//        
//        }
    }

    func calculateWorkoutInterval(data:HKWorkout) -> (Int)
    {
        let smpleStartDate = data.startDate
        let sampleEndDate = data.endDate
        
        var timArr : [Int] = []
        
        let timeInSecs = Helper.sharedInstance.arrivalTimeOfHeartRate(smpleStartDate , date2: sampleEndDate )
        timArr.append(timeInSecs)
        var sum = 0
        for i in 0  ..< timArr.count
        {
            sum = sum + timArr[i]
        }
        if timArr.count != 0
        {
            intervalArr.append(Float(sum/timArr.count))
        }
        else
        {
            intervalArr.append(Float(sum))
        }
       // print(intervalArr)
        return timeInSecs
    }
    
}
