//
//  HistoryViewCell.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 9/26/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit

class HistoryViewCell: UITableViewCell {
    
    @IBOutlet var dateLabel : UILabel?
    @IBOutlet var timeLabel : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
