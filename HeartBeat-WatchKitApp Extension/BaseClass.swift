//
//  BaseClass.swift
//  HeartBeat
//
//  Created by Deep-MacMini on 4/4/16.
//  Copyright © 2016 Deep-MacMini. All rights reserved.
//
import WatchKit
import Foundation
import WatchConnectivity
import HealthKit


class BaseClass: WKInterfaceController {

    let instanceDel = ExtensionDelegate.instance
    var heartRateQuery : HKAnchoredObjectQuery?
    
    var value = 0.0
   // var query : HKObserverQuery?

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
            }

    override func willDisappear(){
        super.willDisappear();
           }

      //MARK: App Suspension
    func setTimerValueFromWorkoutStartDate(){
        //let totalWorkoutTimeInSecs:Int = NSDate.timeIntervalSinceDate(self.workoutSessionStartDate) as! Int
       // let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]

     //   let components = NSCalendar.currentCalendar().components(unitFlags, fromDate:instanceDel.startDate, toDate: NSDate(), options:  NSCalendarOptions(rawValue: 0))
        
        
        let totalWorkoutWithPause = timeDifference(instanceDel.startDate, date2: NSDate())
        let totalPauseTime = instanceDel.totalPauseTimeDuringWorkout;
        
        let actualWorkoutTimeInSec = totalWorkoutWithPause - totalPauseTime;
        let (h,m,s) = secondsToHoursMinutesSeconds(actualWorkoutTimeInSec)
        
        
//        instanceDel.timeHour = components.hour
//        instanceDel.timeMin = components.minute
//        instanceDel.timeSec = components.second
        
        instanceDel.timeHour = h
        instanceDel.timeMin = m
        instanceDel.timeSec = s
        
        let ZoneLabelTime = NSString(format: "%02d:%02d:%02d", instanceDel.timeHour,instanceDel.timeMin,instanceDel.timeSec)
        //instanceDel.totalWorkoutTime = str as String
        instanceDel.totalWorkoutTime = ZoneLabelTime as String
        instanceDel.totalWorkoutTime(ZoneLabelTime as String)
       
        
    }

    //MARK: Anchor get/set
/*
    let AnchorKey = "HKClientAnchorKey"
    func getAnchor() -> HKQueryAnchor? {
        let encoded = NSUserDefaults.standardUserDefaults().dataForKey(AnchorKey)
        if(encoded == nil){
            return nil
        }
        let anchor = NSKeyedUnarchiver.unarchiveObjectWithData(encoded!) as? HKQueryAnchor
        return anchor
    }
    
    func saveAnchor(anchor : HKQueryAnchor) {
        let encoded = NSKeyedArchiver.archivedDataWithRootObject(anchor)
        NSUserDefaults.standardUserDefaults().setValue(encoded, forKey: AnchorKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    */
    func timeDifference(date1:NSDate, date2:NSDate)-> Int
    {
        var timeInSecs :Int = 0
        
        let calendar = NSCalendar.currentCalendar();
        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]
        let components = calendar.components(unitFlags, fromDate: date1, toDate: date2, options: [])
        
        let hour = Int(components.hour)
        let min = Int(components.minute)
        let sec = Int(components.second)
        
        if hour > 0
        {
            timeInSecs = timeInSecs + (hour * 3600)
        }
        
        if min > 0
        {
            timeInSecs = timeInSecs + (min * 60)
        }
        if sec > 0
        {
            timeInSecs = timeInSecs + sec
        }
        
        return timeInSecs
    }

    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }

}
