//
//  ZoneHistoryInterfaceController.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 11/8/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import Foundation
import HealthKit
import WatchKit
import CoreGraphics

class ZoneHistoryInterfaceController: BaseClass,ExtensionZoneHelperDelegate
{
    let barWidthMax = 33.0
    var zone0value = 0.0
    var zone1value = 0.0
    var zone2value = 0.0
    var zone3value = 0.0
    var zone4value = 0.0
    var zone5value = 0.0
    
    var str0 = "0.0%"
    var str1 = "0.0%"
    var str2 = "0.0%"
    var str3 = "0.0%"
    var str4 = "0.0%"
    var str5 = "0.0%"
    
    var timeStr0 = "0:00:00"
    var timeStr1 = "0:00:00"
    var timeStr2 = "0:00:00"
    var timeStr3 = "0:00:00"
    var timeStr4 = "0:00:00"
    var timeStr5 = "0:00:00"
    
    var z0Width:Double = 2
    var z1Width:Double = 2
    var z2Width:Double = 2
    var z3Width:Double = 2
    var z4Width:Double = 2
    var z5Width:Double = 2
    
    var zoneViewAwake = false
    var valueStartAppearing = false
    
    
    @IBOutlet var totalWorkouTimeLbl: WKInterfaceLabel?
    
    @IBOutlet var zoneLbl0 : WKInterfaceLabel?
    @IBOutlet var zoneLbl1 : WKInterfaceLabel?
    @IBOutlet var zoneLbl2 : WKInterfaceLabel?
    @IBOutlet var zoneLbl3 : WKInterfaceLabel?
    @IBOutlet var zoneLbl4 : WKInterfaceLabel?
    @IBOutlet var zoneLbl5 : WKInterfaceLabel?
    
    @IBOutlet var zoneTimeLbl0 : WKInterfaceLabel?
    @IBOutlet var zoneTimeLbl1 : WKInterfaceLabel?
    @IBOutlet var zoneTimeLbl2 : WKInterfaceLabel?
    @IBOutlet var zoneTimeLbl3 : WKInterfaceLabel?
    @IBOutlet var zoneTimeLbl4 : WKInterfaceLabel?
    @IBOutlet var zoneTimeLbl5 : WKInterfaceLabel?
    
    @IBOutlet var zoneGroup0 : WKInterfaceGroup?
    @IBOutlet var zoneGroup1 : WKInterfaceGroup?
    @IBOutlet var zoneGroup2 : WKInterfaceGroup?
    @IBOutlet var zoneGroup3 : WKInterfaceGroup?
    @IBOutlet var zoneGroup4 : WKInterfaceGroup?
    @IBOutlet var zoneGroup5 : WKInterfaceGroup?
    
    @IBOutlet var MainGroup0 : WKInterfaceGroup?
    @IBOutlet var MainGroup1 : WKInterfaceGroup?
    @IBOutlet var MainGroup2 : WKInterfaceGroup?
    @IBOutlet var MainGroup3 : WKInterfaceGroup?
    @IBOutlet var MainGroup4 : WKInterfaceGroup?
    @IBOutlet var MainGroup5 : WKInterfaceGroup?
    
    
    @IBOutlet var heartReadingLabel: WKInterfaceLabel?
    @IBOutlet var heartReadingPlaceholderLabel: WKInterfaceLabel?
    @IBOutlet var heart: WKInterfaceImage?
    
    var isHeartDullHidden:Bool = true
    var animationTimer:NSTimer?
    var isWillDisappearCalled:Bool = false
    var willScreenOnOffActivation:Bool = false
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        super.instanceDel.extensionZoneHelperDelegate = self
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivationZP")
        NSUserDefaults.standardUserDefaults().synchronize()
        //readingBpmLbl?.setHidden(false)
        if zoneViewAwake == false
        {
            //totalWorkouTimeLbl?.setText("0:00:00")
            
        }
        
    }
    
    override func didAppear()
    {
        super.didAppear()
        if  super.instanceDel.workoutPaused {
            
            heartReadingLabel?.setHidden(true)
            heartReadingPlaceholderLabel?.setHidden(false)
            isHeartDullHidden = false
            
        }
            
        else if !super.instanceDel.startWorkOut{
            resetView()
        }
        
    }
    override func willDisappear(){
        super.willDisappear();
        isWillDisappearCalled = true
    }
    
    func totalWorkoutTime(val:String){
        totalWorkouTimeLbl?.setText(val)
    }
    func resetView(){
        animationTimer?.invalidate()
        totalWorkouTimeLbl?.setText("0:00:00")
        //        heartDullGroup?.setHidden(false)
        //        heartBrightGroup?.setHidden(true)
        heartReadingLabel?.setText("")
        heartReadingPlaceholderLabel?.setHidden(false)
        heartReadingLabel?.setHidden(true)
        isHeartDullHidden = false
        //Set Percentage
        
        str0 = "0.0%"
        str1 = "0.0%"
        str2 = "0.0%"
        str3 = "0.0%"
        str4 = "0.0%"
        str5 = "0.0%"
        
       zone0value = 0.0
       zone1value = 0.0
       zone2value = 0.0
       zone3value = 0.0
       zone4value = 0.0
       zone5value = 0.0
      
        instanceDel.zone0Percentage = str0 as String
        instanceDel.zone1Percentage = str1 as String
        instanceDel.zone2Percentage = str2 as String
        instanceDel.zone3Percentage = str3 as String
        instanceDel.zone4Percentage = str4 as String
        instanceDel.zone5Percentage = str5 as String
        
        
        timeStr0 = zoneTimeValue(0)
        timeStr1 = zoneTimeValue(0)
        timeStr2 = zoneTimeValue(0)
        timeStr3 = zoneTimeValue(0)
        timeStr4 = zoneTimeValue(0)
        timeStr5 = zoneTimeValue(0)
        
       /* instanceDel.zone0Time =  timeStr0
        instanceDel.zone1Time  = timeStr1
        instanceDel.zone2Time  = timeStr2
        instanceDel.zone3Time  = timeStr3
        instanceDel.zone4Time  = timeStr4
        instanceDel.zone5Time  = timeStr5*/
        
        //Set Width
        z0Width = 2
        z1Width = 2
        z2Width = 2
        z3Width = 2
        z4Width = 2
        z5Width = 2
        
        //Set PercentTage of each zone to zero
        zoneLbl0?.setText(instanceDel.zone0Percentage as String)
        zoneLbl1?.setText(instanceDel.zone1Percentage as String)
        zoneLbl2?.setText(instanceDel.zone2Percentage as String)
        zoneLbl3?.setText(instanceDel.zone3Percentage as String)
        zoneLbl4?.setText(instanceDel.zone4Percentage as String)
        zoneLbl5?.setText(instanceDel.zone5Percentage as String)
        
        //Set Time
        setDefaultColorForTimeLabels()
        
        
        
        zoneTimeLbl0?.setText(instanceDel.zone0Time)
        zoneTimeLbl1?.setText(instanceDel.zone1Time)
        zoneTimeLbl2?.setText(instanceDel.zone2Time)
        zoneTimeLbl3?.setText(instanceDel.zone3Time)
        zoneTimeLbl4?.setText(instanceDel.zone4Time)
        zoneTimeLbl5?.setText(instanceDel.zone5Time)
        
        
        zoneGroup0?.setWidth(CGFloat(instanceDel.zone0Width))
        zoneGroup1?.setWidth(CGFloat(instanceDel.zone1Width))
        zoneGroup2?.setWidth(CGFloat(instanceDel.zone2Width))
        zoneGroup3?.setWidth(CGFloat(instanceDel.zone3Width))
        zoneGroup4?.setWidth(CGFloat(instanceDel.zone4Width))
        zoneGroup5?.setWidth(CGFloat(instanceDel.zone5Width))
        
    }
    
    func heartAnimation()
    {
        if self.instanceDel.workoutPaused || !instanceDel.startWorkOut {
            heartReadingLabel?.setHidden(true)
            heartReadingPlaceholderLabel?.setHidden(false)
            return
        }
        if isHeartDullHidden { // Bright image is shown
            heart?.setImage(UIImage(named: "HeartIconDull"))
            isHeartDullHidden = false
            //            heartDullGroup?.setHidden(false)
            //            heartBrightGroup?.setHidden(true)
        }else{
            heart?.setImage(UIImage(named: "HeartIconBright"))
            isHeartDullHidden = true
            //            heartDullGroup?.setHidden(true)
            //            heartBrightGroup?.setHidden(false)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        dispatch_async(dispatch_get_main_queue())
            {
                
                if self.instanceDel.startWorkOut && !self.instanceDel.workoutPaused{
                    
                    self.self.animationTimer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ZoneHistoryInterfaceController.heartAnimation), userInfo: nil, repeats: true)
                    
                    self.willScreenOnOffActivation = NSUserDefaults.standardUserDefaults().boolForKey("willScreenOnOffActivationZP")
                    print ("Key value: \(self.willScreenOnOffActivation)")
                    if   self.willScreenOnOffActivation {
                        
                        
                        self.heartReadingPlaceholderLabel?.setHidden(true)
                        self.heartReadingLabel?.setHidden(false)
                        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivationZP")
                        NSUserDefaults.standardUserDefaults().synchronize()
                        self.setTimerValueFromWorkoutStartDate()
                        // let str = NSString(format: "%02d:%02d:%02d", instanceDel.timeHour,instanceDel.timeMin,instanceDel.timeSec)
                        // heartReadingLabel?.setText(str as String)
                        self.heartReadingLabel?.setText(self.instanceDel.lastHeartBeatValue)
                        
                        
                        let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                        let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                        dispatch_async(backgroundQueue, {
                            
                            
                            //self.instanceDel.getHealthDataSavedWhileAppSuspendedAndUpdateUI()
                        })

                      
                    }
                    
                }else{
                    self.heart?.setImage(UIImage(named: "HeartIconDull"))
                    self.setDefaultColorForTimeLabels()
                }
        }
        totalWorkouTimeLbl!.setText(instanceDel.totalWorkoutTime)
        heartReadingLabel?.setText(instanceDel.lastHeartBeatValue)
        
        zoneLbl0?.setText(instanceDel.zone0Percentage as String)
        zoneLbl1?.setText(instanceDel.zone1Percentage as String)
        zoneLbl2?.setText(instanceDel.zone2Percentage as String)
        zoneLbl3?.setText(instanceDel.zone3Percentage as String)
        zoneLbl4?.setText(instanceDel.zone4Percentage as String)
        zoneLbl5?.setText(instanceDel.zone5Percentage as String)
        
        //Set Time Labels Default Color
        
        
        
        zoneTimeLbl0?.setText(instanceDel.zone0Time)
        zoneTimeLbl1?.setText(instanceDel.zone1Time)
        zoneTimeLbl2?.setText(instanceDel.zone2Time)
        zoneTimeLbl3?.setText(instanceDel.zone3Time)
        zoneTimeLbl4?.setText(instanceDel.zone4Time)
        zoneTimeLbl5?.setText(instanceDel.zone5Time)
        
        
        zoneGroup0?.setWidth(CGFloat(instanceDel.zone0Width))
        zoneGroup1?.setWidth(CGFloat(instanceDel.zone1Width))
        zoneGroup2?.setWidth(CGFloat(instanceDel.zone2Width))
        zoneGroup3?.setWidth(CGFloat(instanceDel.zone3Width))
        zoneGroup4?.setWidth(CGFloat(instanceDel.zone4Width))
        zoneGroup5?.setWidth(CGFloat(instanceDel.zone5Width))
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        if instanceDel.startWorkOut && !instanceDel.workoutPaused{
            animationTimer?.invalidate()
            
            if isWillDisappearCalled{
                //Screen Sliding
            }else{
                //Screen Goes off
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "willScreenOnOffActivationZP")
                NSUserDefaults.standardUserDefaults().synchronize()
                //  heartRateQuery!.updateHandler = nil
                
            }}
        isWillDisappearCalled = false
    }
    
//    func getHealthDataSavedWhileAppSuspendedAndUpdateUI(){
//        if let lastAnchor = getAnchor(){
//            super.instanceDel.anchor = lastAnchor
//        }else{
//            super.instanceDel.anchor =  HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
//        }
//        
//        let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
//        heartRateQuery = HKAnchoredObjectQuery(type: quantityType!, predicate: nil, anchor: instanceDel.anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
//            
//            // print("\(sampleObjects)")
//            guard let heartRateSamples = sampleObjects as? [HKQuantitySample] else {return}
//            for sample in heartRateSamples{
//                self.handleHeartBeatSample(sample)   //It will Set the value
//                
//                
//                //Write Code Here to Update the graphcs or check if querySample Auto Update
//            }
//            
//            super.instanceDel.anchor = newAnchor!
//            self.saveAnchor(super.instanceDel.anchor);
//            
//        }
//     
//        instanceDel.healthStore.executeQuery(heartRateQuery!)
//        
//    }
//    
    func heartRateAndTime(val:Double, time:Int ,start:Bool,count:Int)
    {
        
        let value = val
        
        let currentHeartRate = NSString(format: "%0d", Int(value))
        heartReadingLabel?.setText(currentHeartRate as String)
        heartReadingPlaceholderLabel?.setHidden(true)
        heartReadingLabel?.setHidden(false)
        
        
        // default colors
       setDefaultColorForTimeLabels()
        
        
        if value < 91.0  //rest zone
        {
             zone0value += 1
             zoneTimeLbl0?.setTextColor(UIColor(red: 33/255.0, green: 125/255.0, blue: 168/255.0, alpha: 1))
            
            
        }
        else if value >= 91.0 && value <= 107.0  //Warm Up
        {
            zone1value += 1
            zoneTimeLbl1?.setTextColor(UIColor(red: 33/255.0, green: 125/255.0, blue: 168/255.0, alpha: 1))
            
        }
        else  if value >= 108.0 && value <= 125.0
        {
            zone2value += 1
            zoneTimeLbl2?.setTextColor(UIColor(red: 51/255.0, green: 199/255.0, blue: 231/255.0, alpha: 1))
            
            
        }
        else  if value >= 126.0 && value <= 144.0
        {
            zone3value += 1
            zoneTimeLbl3?.setTextColor(UIColor(red: 243/255.0, green: 226/255.0, blue: 28/255.0, alpha: 1))
            
        }
        else  if value >= 145.0 && value <= 168.0
        {
            zone4value += 1
            zoneTimeLbl4?.setTextColor(UIColor(red: 225/255.0, green: 152/255.0, blue: 60/255.0, alpha: 1))
            
        }
        else  if value >= 169.0
        {
            zone5value += 1
            zoneTimeLbl5?.setTextColor(UIColor(red: 211/255.0, green:0/255.0,blue: 69/255.0, alpha: 1))
            
        }
        
        var count = zone0value + zone1value+zone2value+zone3value+zone4value+zone5value
        if count == 0
        {
            count = 1
        }
        
        // let totalWorkoutTime = zoneTimeValue(time)
        // totalWorkouTimeLbl?.setText(totalWorkoutTime)
        str0 = NSString(format: "%0.1f%@", Float((zone0value/count)*100),"%") as String
        str1 = NSString(format: "%0.1f%@", Float((zone1value/count)*100),"%") as String
        str2 = NSString(format: "%0.1f%@", Float((zone2value/count)*100),"%") as String
        str3 = NSString(format: "%0.1f%@", Float((zone3value/count)*100),"%") as String
        str4 = NSString(format: "%0.1f%@", Float((zone4value/count)*100),"%") as String
        str5 = NSString(format: "%0.1f%@", Float((zone5value/count)*100),"%") as String
        
        timeStr0 = zoneTimeValue(Int(Float(time)*Float(zone0value/count)))
        timeStr1 = zoneTimeValue(Int(Float(time)*Float(zone1value/count)))
        timeStr2 = zoneTimeValue(Int(Float(time)*Float(zone2value/count)))
        timeStr3 = zoneTimeValue(Int(Float(time)*Float(zone3value/count)))
        timeStr4 = zoneTimeValue(Int(Float(time)*Float(zone4value/count)))
        timeStr5 = zoneTimeValue(Int(Float(time)*Float(zone5value/count)))
        
        instanceDel.zone0Time  = timeStr0
        instanceDel.zone1Time  = timeStr1
        instanceDel.zone2Time  = timeStr2
        instanceDel.zone3Time  = timeStr3
        instanceDel.zone4Time  = timeStr4
        instanceDel.zone5Time  = timeStr5
        
        instanceDel.zone0Percentage = str0 as String
        instanceDel.zone1Percentage = str1 as String
        instanceDel.zone2Percentage = str2 as String
        instanceDel.zone3Percentage = str3 as String
        instanceDel.zone4Percentage = str4 as String
        instanceDel.zone5Percentage = str5 as String
        
        zoneTimeLbl0?.setText(timeStr0)
        zoneTimeLbl1?.setText(timeStr1)
        zoneTimeLbl2?.setText(timeStr2)
        zoneTimeLbl3?.setText(timeStr3)
        zoneTimeLbl4?.setText(timeStr4)
        zoneTimeLbl5?.setText(timeStr5)
        
        z0Width = (Double(zone0value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone0value)/(Double(count)))*barWidthMax : 2.0
        z1Width = (Double(zone1value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone1value)/(Double(count)))*barWidthMax : 2.0
        z2Width = (Double(zone2value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone2value)/(Double(count)))*barWidthMax : 2.0
        z3Width = (Double(zone3value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone3value)/(Double(count)))*barWidthMax : 2.0
        z4Width = (Double(zone4value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone4value)/(Double(count)))*barWidthMax : 2.0
        z5Width = (Double(zone5value)/(Double(count)))*barWidthMax >= 2 ? (Double(zone5value)/(Double(count)))*barWidthMax : 2.0
        
        instanceDel.zone0Width = z0Width
        instanceDel.zone1Width = z1Width
        instanceDel.zone2Width = z2Width
        instanceDel.zone3Width = z3Width
        instanceDel.zone4Width = z4Width
        instanceDel.zone5Width = z5Width
        
        zoneGroup0?.setWidth(CGFloat(z0Width))
        zoneGroup1?.setWidth(CGFloat(z1Width))
        zoneGroup2?.setWidth(CGFloat(z2Width))
        zoneGroup3?.setWidth(CGFloat(z3Width))
        zoneGroup4?.setWidth(CGFloat(z4Width))
        zoneGroup5?.setWidth(CGFloat(z5Width))
        
        zoneLbl0?.setText(str0 as String)
        zoneLbl1?.setText(str1 as String)
        zoneLbl2?.setText(str2 as String)
        zoneLbl3?.setText(str3 as String)
        zoneLbl4?.setText(str4 as String)
        zoneLbl5?.setText(str5 as String)
    }
    
    
    func zoneTimeValue( value : Int) -> (String)
    {
        var s = ""
        var secs = value
        var mins = 0
        var hour = 0
        if value < 60
        {
            s = NSString(format: "%@%02d", "0:00:",secs) as String
        }
        else
        {
            hour = secs/3600
            secs = secs%3600
            if secs != 0
            {
                mins = secs/60
                secs = secs%60
            }
            
            s = NSString(format: "%02d%@%02d%@%02d",hour,":",mins,":",secs) as String
        }
        return s
    }
    
//    func handleHeartBeatSample(sample:HKQuantitySample)
//    {
//        //self.value = Double(arc4random_uniform(80) + 100)  //for testing only
//        
//        if !instanceDel.workoutPaused{
//            var timeInSecs = 0
//            self.value = sample.quantity.doubleValueForUnit(instanceDel.heartRateUnit)
//            // print("heart rate \(self.value)")
//            
//            instanceDel.heartDataArray.append(self.value)
//            
//
//            
//            instanceDel.totalValueCount += 1
//            let currentRate = NSString(format: "%d", Int(self.value)) as String
//            
//            instanceDel.totalAvg = instanceDel.totalAvg + Double(currentRate)!
//            
//            //find max
//            if instanceDel.previousValue < Int(self.value)
//            {
//                instanceDel.previousValue = Int(self.value)
//                
//                let maxStr = NSString(format: "%@ %d", "MAX",instanceDel.previousValue) as String
//                
//                self.instanceDel.maximumHeartRate = maxStr
//            }
//            
//            let avg = (Int(instanceDel.totalAvg)/instanceDel.totalValueCount)
//            let avgStr = NSString(format: "%@ %d", "AVG",avg) as String
//            self.instanceDel.averageHeartRate = avgStr
//            
//            // self.test = self.test + 0.1
//            
//            
//            //self.value  = 150
//            timeInSecs = instanceDel.timeHour*60*60 + instanceDel.timeMin*60 + instanceDel.timeSec
//            self.instanceDel.lastHeartBeatValue = String(Int(self.value))
//            self.instanceDel.passValueToZoneHistory(self.value, time: timeInSecs, sessionEnd: false, count: instanceDel.totalValueCount)
//            self.instanceDel.addNewPointValue(self.value)
//            
//        }
//        
//    }
    
    
    func setDefaultColorForTimeLabels(){
        zoneTimeLbl0?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
        zoneTimeLbl1?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
        zoneTimeLbl2?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
        zoneTimeLbl3?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
        zoneTimeLbl4?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
        zoneTimeLbl5?.setTextColor(UIColor(red: 151/255, green:151/255, blue: 151/255, alpha: 1))
    }
    
    
}
