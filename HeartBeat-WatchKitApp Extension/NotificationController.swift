//
//  NotificationController.swift
//  HeartBeat-WatchKitApp Extension
//
//  Created by Rupinder-Mac on 10/12/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import WatchKit
import Foundation


class NotificationController: WKUserNotificationInterfaceController {

    override init() {
        // Initialize variables here.
        super.init()
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

   }
