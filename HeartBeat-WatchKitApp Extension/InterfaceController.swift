//
//  InterfaceController.swift
//  HeartBeat-WatchKitApp Extension
//
//  Created by Rupinder-Mac on 10/12/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//


//import SplunkMint
import WatchKit
import Foundation
import WatchConnectivity
import HealthKit

class InterfaceController: BaseClass, WCSessionDelegate, HKWorkoutSessionDelegate
{
    
    var startDate:NSDate?
    
    var queryStartFirst = false
    var firstSampleStartDate :NSDate?

    var isPause:Bool = false
    var timer = NSTimer()
    var heartRate :Double = 80.0
    var session :WCSession!
    
    @IBOutlet var startGroup : WKInterfaceGroup?
    @IBOutlet var endGroup: WKInterfaceGroup?
    @IBOutlet var heartReadingLabel: WKInterfaceLabel?
    @IBOutlet var heartReadingPlaceholderLabel: WKInterfaceLabel?
    @IBOutlet var heart: WKInterfaceImage?
    @IBOutlet var timerLabel : WKInterfaceLabel!
    @IBOutlet var pauseButton: WKInterfaceButton!
    
    var isHeartDullHidden:Bool = true
    var workoutSession:HKWorkoutSession? = nil
    
    
    var isActive:Bool = false;
    
    var isWillDisappearCalled:Bool = false
    var willScreenOnOffActivation:Bool = false
    
    var currentReading:CGFloat = 0.0
    var prevousReading:CGFloat = 0.0
    
     //MARK: View LifeCycle
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
     
        isActive = false;
        if isWillDisappearCalled{
            print("will disappear called")
        }else{
            //Screen Goes off
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "willScreenOnOffActivation")
            NSUserDefaults.standardUserDefaults().synchronize()
            //  heartRateQuery!.updateHandler = nil
            
        }
        isWillDisappearCalled = false
    }
    func session(session: WCSession, activationDidCompleteWithState activationState: WCSessionActivationState, error: NSError?){
        print("Activation Complete with activation state:" );
        
    }
    override func willDisappear(){
        super.willDisappear();
        isWillDisappearCalled = true

    }
    override func didAppear() {
        super.didAppear()

        // workout not started or paused state
        if !instanceDel.startWorkOut || instanceDel.workoutPaused {
            
            heartReadingLabel?.setHidden(true)
            heartReadingPlaceholderLabel?.setHidden(false)
            isHeartDullHidden = false
        }
        
        // coming from summary page
        if instanceDel.isComingFromSummaryPage {
            instanceDel.totalWorkoutTime = "0:00:00"
            instanceDel.resetView()
            instanceDel.isComingFromSummaryPage = false
            resetToStat()
            return
        }
    }
    
    func resetToStat(){
        endGroup?.setHidden(true);
        startGroup?.setHidden(false);
        heartReadingLabel?.setText("")
        heartReadingPlaceholderLabel?.setHidden(false)
        heartReadingLabel?.setHidden(true)
        isHeartDullHidden = false
        timerLabel.setText("")
        //pauseButtonImage
        
    }
    
    override func awakeWithContext(context: AnyObject?)
    {
        //healthStore.enableBackgroundDeliveryForType(HKObjectType.quantityTypeForIdentifier)
        super.awakeWithContext(context)
      
        let resumeImage =  UIImage(named: "Resume")
        let pauseImage = UIImage(named: "Pause")
        pauseButton.setBackgroundImage(resumeImage)
        pauseButton.setBackgroundImage(pauseImage)

        
        
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivation")
        NSUserDefaults.standardUserDefaults().synchronize()
        resetToStat()
        authorizeHealthKit { (success, error) -> Void in
            
        }
        
    }
       
   
    override func willActivate()
    {
        super.willActivate()
        
        //workout started and not paused state
        if instanceDel.startWorkOut && !instanceDel.workoutPaused{
      
                willScreenOnOffActivation = NSUserDefaults.standardUserDefaults().boolForKey("willScreenOnOffActivation")
                if willScreenOnOffActivation{
                    //Activate becuase of Screen On
                    
                    let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                    let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                    
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivation")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    setTimerValueFromWorkoutStartDate()
                    let str = NSString(format: "%02d:%02d:%02d", self.instanceDel.timeHour,self.instanceDel.timeMin,self.instanceDel.timeSec)
                    self.timerLabel.setText(str as String)
                    dispatch_async(backgroundQueue, {
                       
                        //self.instanceDel.getHealthDataSavedWhileAppSuspendedAndUpdateUI()

                    })
                
                }
        }else{
            heart?.setImage(UIImage(named: "HeartIconDull"))
            
        }
     
        heartReadingLabel?.setText(instanceDel.lastHeartBeatValue)
        isActive = true
    
        
        // This method is called when watch view controller is about to be visible to user
        if (WCSession.isSupported()) {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
    }
    
//    func getHealthDataSavedWhileAppSuspended(){
//        
//        if let lastAnchor = getAnchor(){
//            instanceDel.anchor = lastAnchor
//        }else{
//            instanceDel.anchor =  HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
//        }
//        
//        let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
//        heartRateQuery = HKAnchoredObjectQuery(type: quantityType!, predicate: nil, anchor: instanceDel.anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
//            
//            // print("\(sampleObjects)")
//            guard let heartRateSamples = sampleObjects as? [HKQuantitySample] else {return}
//            for sample in heartRateSamples{
//                self.handleHeartBeatSample(sample)
//                
//            }
//            
//            super.instanceDel.anchor = newAnchor!
//            self.saveAnchor(super.instanceDel.anchor);
//           // self.updateHeartRate(sampleObjects)
//            //All the below Logic should go here.
//        }
//  
//        instanceDel.healthStore.executeQuery(heartRateQuery!)
//    }

    
    @IBAction func pauseClick()
    {
        if (isActive) {
            if(instanceDel.workoutPaused)
            {
                
                self.instanceDel.healthStore.resumeWorkoutSession(workoutSession!)
                instanceDel.workoutPaused = false
               // instanceDel.stopTimer = false
                let image = UIImage(named: "Pause")
                
                pauseButton.setBackgroundImage(image)
                setTimer()
             //   pauseButton.setBackgroundColor(UIColor.clearColor())
              //  instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
              //  workoutSession!.delegate = self
             
              //  instanceDel.workoutSessionStartDate = NSDate()
               // updateHandler = true
               let  timeDifInSec = timeDifference(instanceDel.lastPauseDateTime!, date2:  instanceDel.workoutSessionStartDate)
               instanceDel.totalPauseTimeDuringWorkout +=  timeDifInSec
                
            }
            else
            {
                instanceDel.healthStore.pauseWorkoutSession(workoutSession!)
                instanceDel.lastPauseDateTime  = NSDate();
                heart?.setImage(UIImage(named: "HeartIconDull"))
               // isPause = true
              //  instanceDel.stopTimer = true
                instanceDel.workoutPaused = true
                let image = UIImage(named: "Resume")
                pauseButton.setBackgroundImage(image)
                timer.invalidate()

              //  pauseButton.setBackgroundColor(UIColor.clearColor())
              // instanceDel.healthStore.endWorkoutSession(workoutSession!)
            
          
                let applicationData = ["Value":"","Start":false,"loadWorkout":false,"Paused":true,"WorkoutSampleDate":NSDate(),"workOutStartDate":NSDate(),"workOutEndDate":NSDate()]
                
                self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
                    }, errorHandler: {(error ) -> Void in
                        print(error)
                        // catch any errors here
                })
                
               // healthStore.stopQuery(query!)
               // instanceDel.healthStore.stopQuery(heartRateQuery!)
               // updateHandler = false
            }
            
        }
    }
    
    @IBAction func endWorkoutBtnPressed()
    {
        instanceDel.healthStore.endWorkoutSession(workoutSession!)

    }
    
    
    
    func CleanUp() {
        instanceDel.resetView()
        instanceDel.heartDataArray.removeAll()
        instanceDel.samples.removeAll()
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivation")
        NSUserDefaults.standardUserDefaults().synchronize()
        //heartDataArray.removeAll()
    }
    
    func CheckAuthorization () -> Bool {
        if instanceDel.authorization == false
        {
            authorizeHealthKit { (success, error) -> Void in
                
            }
            
            let action = WKAlertAction.init(title: "ok", style: .Cancel, handler: { () -> Void in
                //alert
                self.dismissController()
            })
            let actions = [action]
            
            // var alert:WKAl
            self.presentAlertControllerWithTitle("", message: "Please ensure authorization to use Healthkit from iphone app", preferredStyle: WKAlertControllerStyle.Alert, actions: actions)
            return false
        }else{
            return true
        }
    }

     func Start()
    {
        self.CleanUp()
        
        if isActive{
            self.CheckAuthorization()
            }
            else
            {
                  instanceDel.workoutActivityType = .Walking
                let workoutConfiguration = HKWorkoutConfiguration()
                workoutConfiguration.activityType = .Walking
                workoutConfiguration.locationType = .Outdoor
                
                do {
                    workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                    workoutSession!.delegate = self
                    instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
  
                } catch let error as NSError{
                    print (error)
                }

                
                
                
            
        }
        
    }
    @IBAction func StartWalking()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
                instanceDel.workoutActivityType = .Walking
                let workoutConfiguration = HKWorkoutConfiguration()
                workoutConfiguration.activityType = .Walking
                workoutConfiguration.locationType = .Outdoor
                
                do {
                    workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                    workoutSession!.delegate = self
                    instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                    
                } catch let error as NSError{
                    print (error)
                }

            }
        }
    }
    
    @IBAction func StartRunning()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Running
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Running
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            
            }
        }
    }
    
    @IBAction func StartCycling()
    {
        
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Cycling
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Cycling
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            
        }
        }
    }
    
    @IBAction func StartStairStepping()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Stairs
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Stairs
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            }
            
        }
    }
    
    @IBAction func StartElliptical()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Elliptical
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Elliptical
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            }
            
        }
    }
    @IBAction func StartRowing()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Rowing
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Rowing
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            }
        }
    }
     
    @IBAction func StartOther()
    {
        self.CleanUp()
        
        if isActive{
            let isAuthorised =   self.CheckAuthorization()
            if isAuthorised {
            instanceDel.workoutActivityType = .Other
            let workoutConfiguration = HKWorkoutConfiguration()
            workoutConfiguration.activityType = .Other
            workoutConfiguration.locationType = .Outdoor
            
            do {
                workoutSession = try HKWorkoutSession(configuration: workoutConfiguration)
                workoutSession!.delegate = self
                instanceDel.healthStore.startWorkoutSession(self.workoutSession!)
                
            } catch let error as NSError{
                print (error)
            }
            }
        }
    }
    
    
    func authorizeHealthKit(completion: (( success:Bool, error:NSError!) -> Void)!)
    {
        
        // 1. Set the types you want to read from HK Store
        let healthKitTypesToRead = Set(arrayLiteral:
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
            HKObjectType.workoutType()
        )
        
        // 2. Set the types you want to write to HK Store
        let healthKitTypesToWrite = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            HKQuantityType.workoutType()
        )
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "com.apple.tutorials.healthkit", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return;
        }
        
        // 4.  Request HealthKit authorization
        
        self.instanceDel.healthStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
         
            if success == false {
                super.instanceDel.authorization = false
                let action = WKAlertAction.init(title: "ok", style: .Cancel, handler: { () -> Void in
                    //alert
                    self.dismissController()
                })
                let actions = [action]
                
               // var alert:WKAl
                self.presentAlertControllerWithTitle("", message: "App is not authorized to use Healthkit.Please ensure authorization to use Healthkit from iphone app", preferredStyle: WKAlertControllerStyle.Alert, actions: actions)
            }
            else
            {
                super.instanceDel.authorization = true
                print("authorization sucessful")
            }
        }
    }
    
    
    // MARK: WorkoutSession Delegates
    func workoutSession(workoutSession: HKWorkoutSession, didChangeToState toState: HKWorkoutSessionState, fromState: HKWorkoutSessionState, date: NSDate)
    {
        switch toState {
            case .Running:
                print("Session Running")
                if !instanceDel.startWorkOut{
                    workoutDidStart(date)
                    break;
                }
            
            case .Ended:
                print("Session Ended")
                workoutDidEnd(date)
                break;
            case .Paused:
                print("Session Pauseed")
                break;
            case .NotStarted:
                print("Session Not started");
                break;
           
            }
    }
    
    func workoutSession(workoutSession: HKWorkoutSession, didFailWithError error: NSError) {
        // Do nothing for now
        print("Session Did fail with error:" + error.description)
    }
    
    
    // MARK: WorkoutSession Delegates Helper Methods
    func workoutDidStart(date : NSDate)
    {
       
        
        self.startGroup?.setHidden(true)
        self.endGroup?.setHidden(false)
        self.timerLabel.setText("0:00:00")
        instanceDel.startWorkOut = true
        instanceDel.workoutPaused = false
        super.instanceDel.previousValue = 0
        instanceDel.workoutSessionStartDate = NSDate()
        super.instanceDel.totalValueCount = 0
        instanceDel.timeSec = 0
        instanceDel.timeMin = 0
        instanceDel.timeHour = 0
        instanceDel.totalAvg = 0.0
    //    instanceDel.stopTimer = false
        setTimer()
      //  updateHandler = true
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/YYYY HH:mm:ss a"
        formatter.locale = NSLocale.currentLocale()
        instanceDel.startDate = NSDate()
        instanceDel.totalPauseTimeDuringWorkout = 0
      
        self.createHeartRateStreamingQuery(date)
        let applicationData = ["Value":"","Start":true,"loadWorkout":false,"Paused":false,"WorkoutSampleDate":NSDate(),"workOutStartDate":NSDate(),"workOutEndDate":NSDate()]
        
        self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
            }, errorHandler: {(error ) -> Void in
                //  print(error)
                // catch any errors here
        })
    
    
    }
    
    func workoutDidEnd(date : NSDate)
    {
        let applicationData = ["Value":"","Start":false,"loadWorkout":true,"Paused":false,"workOutStartDate":self.instanceDel.startDate,"workOutEndDate":self.instanceDel.endDate]
        
        self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
            }, errorHandler: {(error ) -> Void in
                print(error)
                // catch any errors here
        })
        
        instanceDel.healthStore.stopQuery(heartRateQuery!)
        let image = UIImage(named: "Pause")
        
        self.pauseButton.setBackgroundImage(image)
        self.endGroup?.setHidden(true)
        timer.invalidate()
        instanceDel.startWorkOut = false
        isPause = false
       // instanceDel.stopTimer = true
        instanceDel.workoutPaused = false
        
        startDate = nil
        super.instanceDel.totalValueCount = 0
        super.instanceDel.totalAvg = 0.0
        instanceDel.timeSec = 0
        instanceDel.timeMin = 0
        instanceDel.timeHour = 0
        
        //  workoutSession = HKWorkoutSession(activityType: HKWorkoutActivityType.Walking, locationType: HKWorkoutSessionLocationType.Unknown)
        
        queryStartFirst = false
        
        //instanceDel.heartDataArray = heartDataArray
        /*
        let applicationData = ["Value":"","Start":false,"loadWorkout":false,"Paused":false,"WorkoutSampleDate":NSDate(),"workOutStartDate":NSDate(),"workOutEndDate":NSDate(),"HeartRateInfo":instanceDel.heartDataArray,"ActivityType":(instanceDel.workoutActivityType.rawValue)]
        self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
            }, errorHandler: {(error ) -> Void in
                print(error)
                // catch any errors here
        })
        */
        instanceDel.endDate = NSDate()
      //  updateHandler = false
        self.presentControllerWithName("SummaryInterfaceController", context: "  ")
        print("workout ended")
        
        /*
        workoutSession = HKWorkoutSession(activityType: HKWorkoutActivityType.Walking,locationType: HKWorkoutSessionLocationType.Unknown)*/
    }
    
    //This is exectured when StartButton is pressed
    func createHeartRateStreamingQuery(workoutStartDate: NSDate)
    {
        let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
        let datePredicate = HKQuery.predicateForSamplesWithStartDate(instanceDel.workoutSessionStartDate, endDate: nil, options: .StrictStartDate)
        
    
         //let devicePredicate = HKQuery.predicateForObjects(from:[HKDevice.localDevice()])

        let updateHandler: (HKAnchoredObjectQuery,[HKSample]?,[HKDeletedObject]?,HKQueryAnchor?,
            NSError?)-> Void  = {query, samples, deleteObjects, newAnchor, error  in
                
                if !self.instanceDel.workoutPaused {
                    
                    //samples will have all the values including the ones that are obtained 
                    //when session is paused
                    for  sample in samples! {
                        
//var v = sample as! HKQuantitySample
                        
                        
                
                        //let startDate = 11/10/2016
//                       let endDtae =  11/10/2016
                     /*   let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
                        
                        let bpm = HKUnit(fromString: "count/min")
                        let quantity = HKQuantity(unit: bpm, doubleValue: 120.0)
                        
                        let testSample = HKQuantitySample(type: quantityType!,
                         
                         quantity: quantity,
                                                              startDate: NSDate(),
                                                              endDate: NSDate(),
                                                              metadata: nil)
                        self.instanceDel.samples.append(testSample) */

                        
                       self.instanceDel.samples.append(sample)
                        self.didZoneUp(sample)
                       /* if (self.didZoneUp(sample)){
                           WKInterfaceDevice.currentDevice().playHaptic(.DirectionUp)
                        }
                        if (self.didZoneDown(sample)){
                             WKInterfaceDevice.currentDevice().playHaptic(.DirectionDown)
                        }*/
                        
                       //
                       //  WKInterfaceDevice.currentDevice().playHaptic(WKHapticType.Click)
                    }
                    
                   // self.updateHeartRate(self.instanceDel.samples)
                    
                    self.updateHeartRate(samples)
                }
                
        }
        
        heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: datePredicate, anchor: nil, limit: Int(HKObjectQueryNoLimit) , resultsHandler: updateHandler)
        heartRateQuery!.updateHandler = updateHandler
        instanceDel.healthStore.executeQuery(heartRateQuery!)
        
      /*  heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: datePredicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
          
        }
    
       

        
        heartRateQuery!.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
         
            
              super.instanceDel.anchor = newAnchor!
               super.saveAnchor(super.instanceDel.anchor)
                self.updateHeartRate(samples)
            
        }*/
      

    }
    
    
    
    //Update the UI of InterfaceController with the latest value of the heartBeat.
    func updateHeartRate(samples: [HKSample]?)
    {
        guard let heartRateSamples = samples as? [HKQuantitySample] else {return}
        
        guard let sample = heartRateSamples.first else{return}
       
        if queryStartFirst == false
        {
            self.handleHeartBeatSample(sample)
            firstSampleStartDate = sample.startDate
            queryStartFirst = true
        }
        else
        {
            let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]
            let components = NSCalendar.currentCalendar().components(unitFlags, fromDate:firstSampleStartDate!, toDate: sample.startDate, options:  NSCalendarOptions(rawValue: 0))
            if components.hour == 0 && components.minute == 0 && components.second == 0
            {
            }
            else
            {
                self.handleHeartBeatSample(sample) // it will set the value
                let heartRateValue = NSString(format: "%0d", Int(super.value))
                
                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                            self.heartReadingLabel?.setText(String(heartRateValue))
                                            self.heartReadingPlaceholderLabel?.setHidden(true)
                                            self.heartReadingLabel?.setHidden(false)
                                            self.firstSampleStartDate = sample.startDate
                                        })
              
            }
        }
    }
    
    
    func setTimer()
    {
        
        dispatch_async(dispatch_get_main_queue(), {
             self.timer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(InterfaceController.counterForTime), userInfo: nil, repeats: true)
        })

    }
    
    func counterForTime()
    {
        /*if (instanceDel.timeSec % 3 == 0){
            WKInterfaceDevice.currentDevice().playHaptic(.DirectionDown)
        }*/
        
        if self.instanceDel.workoutPaused {
            heartReadingPlaceholderLabel?.setHidden(false)
            heartReadingLabel?.setHidden(true)
            return
        }
        //code responsible for animation
        if isHeartDullHidden { // Bright image is shown
            heart?.setImage(UIImage(named: "HeartIconDull"))
            isHeartDullHidden = false
        }
        else{
            heart?.setImage(UIImage(named: "HeartIconBright"))
            isHeartDullHidden = true
        }
        
        if (instanceDel.timeSec < 59)
        {
            instanceDel.timeSec += 1
        }
        else if (instanceDel.timeSec >= 59 && instanceDel.timeMin < 59)
        {
            instanceDel.timeMin = instanceDel.timeMin + 1;
            instanceDel.timeSec = 0;
        }
        else if (instanceDel.timeMin >= 59 && instanceDel.timeHour < 23)
        {
            instanceDel.timeMin = 0;
            instanceDel.timeHour += 1
        }
        else
        {
            instanceDel.timeHour = 0;
            instanceDel.timeSec = 0;
            instanceDel.timeMin = 0;
        }
        let str = NSString(format: "%02d:%02d:%02d", instanceDel.timeHour,instanceDel.timeMin,instanceDel.timeSec)
        let ZoneLabelTime = NSString(format: "%01d:%02d:%02d", instanceDel.timeHour,instanceDel.timeMin,instanceDel.timeSec)
       
        instanceDel.totalWorkoutTime = ZoneLabelTime as String
        instanceDel.totalWorkoutTime(ZoneLabelTime as String)
        timerLabel.setText(str as String)
    }
    
    // When is this method called ????????
    // Ans:
    
    //Add each heart Beat value to an array. This array is passed in each message send to phone from watch.
    func handleHeartBeatSample(sample:HKQuantitySample)
    {
            if !instanceDel.workoutPaused
            {
            var timeInSecs = 0
            self.value = sample.quantity.doubleValueForUnit(instanceDel.heartRateUnit)
                
          //  self.value = 120.0
                
            var sampleStartDate :String?
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd/MM/YYYY HH:mm:ss a"
               // print(startDate)
               // print(sample.startDate)
            if(startDate != nil)
            {
                let timeInterval = timeDifference(startDate!, date2: sample.startDate)
                if(timeInterval > 5)
                {
                    let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second,.Day,.Month,.Year]
    
                    let components :NSDateComponents = NSCalendar.currentCalendar().components(unitFlags, fromDate: startDate!)
                   components.second = components.second + timeInterval
                    
                    let then = NSCalendar.currentCalendar().dateByAddingComponents(
                        components,
                        toDate:startDate!,
                        options: [])!
                    
                    sampleStartDate  = formatter.stringFromDate(then)
                }
                else
                {
                    sampleStartDate  = formatter.stringFromDate(sample.startDate)
                }
            }
            else
            {
                sampleStartDate  = formatter.stringFromDate(sample.startDate)
            }
            
            startDate = sample.startDate
            let sampleStr = NSString(format: "%2f%@%@",self.value,"s",sampleStartDate!)
            
           // heartDataArray.append(self.value)
            instanceDel.heartDataArray.append(self.value)
            
            let applicationData = ["Value":sampleStr,"Start":true,"loadWorkout":false,"Paused":false,"WorkoutSampleDate":startDate!,"workOutStartDate":NSDate(),"workOutEndDate":NSDate()]
           
            self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
                }, errorHandler: {(error ) -> Void in
                    //  print(error)
                    // catch any errors here
            })
                
            instanceDel.totalValueCount += 1
            let currentRate = NSString(format: "%d", Int(self.value)) as String
            
            instanceDel.totalAvg = instanceDel.totalAvg + Double(currentRate)!
            
            //find max
            if instanceDel.previousValue < Int(self.value)
            {
                instanceDel.previousValue = Int(self.value)
                
                let maxStr = NSString(format: "%@ %d", "MAX",instanceDel.previousValue) as String
                
                self.instanceDel.maximumHeartRate = maxStr
            }
            
            let avg = (Int(instanceDel.totalAvg)/instanceDel.totalValueCount)
            let avgStr = NSString(format: "%@ %d", "AVG",avg) as String
            self.instanceDel.averageHeartRate = avgStr
            
            timeInSecs = instanceDel.timeHour*60*60 + instanceDel.timeMin*60 + instanceDel.timeSec
            self.instanceDel.lastHeartBeatValue = String(Int(self.value))
            self.instanceDel.passValueToZoneHistory(self.value, time: timeInSecs, sessionEnd: false, count: instanceDel.totalValueCount)
            self.instanceDel.addNewPointValue(self.value)
        }
    }
    
    func PlayUpSound(){
       WKInterfaceDevice.currentDevice().playHaptic(.DirectionUp)
    }
    func didZoneUp(sample: HKSample)
    {
        
        currentReading = CGFloat((sample as! HKQuantitySample).quantity.doubleValueForUnit(instanceDel.heartRateUnit))
        
        let prevReadingZone = self.getZoneForReading(prevousReading)
        let currentReadingZone = self.getZoneForReading(currentReading)
        prevousReading = currentReading
        
        if (instanceDel.samples.count >= 2 ){
            if currentReadingZone > prevReadingZone{
               
                dispatch_async(dispatch_get_main_queue(), {
                    self.PlayUpSound()
                    NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(InterfaceController.PlayUpSound), userInfo: nil, repeats: false)
                })
             
            }
            else if  currentReadingZone < prevReadingZone{
                WKInterfaceDevice.currentDevice().playHaptic(.DirectionDown)
            }

        }
        
    }

    func didZoneDown(sample: HKSample)->Bool{
    
        currentReading = CGFloat((sample as! HKQuantitySample).quantity.doubleValueForUnit(instanceDel.heartRateUnit))
       
        let prevReadingZone = self.getZoneForReading(prevousReading)
        let currentReadingZone = self.getZoneForReading(currentReading)
        
        
         prevousReading = currentReading
        if currentReadingZone < prevReadingZone{
            if ( instanceDel.samples.count >= 2){
                return true
            } else{
                return false
            }
        
        }
        else{
            return false
        }

    }
    
    func getZoneForReading(a:CGFloat)->Int
    {
        if a <= 90
        {
            return 1
        }
        else if a >= 91 && a <= 107 //91 to 107
        {
            return   2
            //  CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if a >= 108 && a <= 125 //108 to 125
        {
            return 3
            // CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if  a >= 126 && a <= 144
        {
            return 4
            //CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if  a >= 145 && a <= 168
        {
            return 5
            // CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else
        {
            return 6
            // CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
       
    }
}
