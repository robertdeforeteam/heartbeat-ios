//
//  InterfaceController.swift
//  VimoHeartRate WatchKit App Extension
//
//  Created by Ethan Fan on 6/25/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

import Foundation
import HealthKit
import WatchKit
import CoreGraphics


class DrawLine: BaseClass, ExtensionGraphViewDelegate{
    
    //var timer = NSTimer()
    var animationTimer:NSTimer?
    var timeSec = 0
    var timeMin = 0
    var timeHour = 0
    
    var value1:Float?
    var value2:Float?
    var value3:Float?
    var value4:Float?
    var value5:Float?
    
    var sum = 0.0
    
    var timer1 :NSTimer?
    

    @IBOutlet var intensityGroup: WKInterfaceGroup?
   
    @IBOutlet var intensityLabel : WKInterfaceLabel?
    @IBOutlet var zoneLabel : WKInterfaceLabel?
    
    var timerValue = 0
    
    var valueStartAppearing = false
    var heartRateArray : [Float] = []
   // var kGraphHeight: Float = 250
    //var kDefaultGraphWidth: Float = 900
    var kOffsetX: Float = 0
    var kStepX: Float = 60
    var kOffsetY: Float = 40
    var kStepY: Float = 21.5
    
    var width :Float = 272
    
    var kGraphBottom: Float = 190
    var kGraphTop: Float = 0
    var kCircleRadiusC : CGFloat = 0.07
    var changeBottomLineIndex : Bool = false
    var bottomLineIndex :Int = 0
    var textIndex :Int = 0
    var Index :Int = 0

    
    var currentBarValue:Double=0
    //For Bars
    var kBarStepX: Float = 21
    var kBarTop: Float = 10
    var kBarWidth: Float = 16.0
    
    //For LineGraph
    var kCircleRadius: Float = 1
    
    var barGraphArray :[Double] = []
    var graphArray : [Double] = []
    var time :[String] = []
    var newpoint:Double = 0.2
    
    

    @IBOutlet var heartReadingPlaceholderLabel: WKInterfaceLabel?
    @IBOutlet var heart: WKInterfaceImage?
    @IBOutlet var heartReadingLabel: WKInterfaceLabel?
    
    @IBOutlet var timeLabel1 : WKInterfaceLabel!
    
    @IBOutlet private weak var imageView: WKInterfaceImage!
    @IBOutlet private weak var groupView: WKInterfaceGroup!
    var isHeartDullHidden:Bool = false
    var isWillDisappearCalled:Bool = false
    var context1:CGContext!
    var willScreenOnOffActivation:Bool = false
    func totalWorkoutTime(val:String){
        timeLabel1?.setText(val)
    }
    override init() {
        super.init()
        let size = CGSizeMake(272, 190)
        UIGraphicsBeginImageContext(size)
        context1 = UIGraphicsGetCurrentContext()
        CGContextSetStrokeColorWithColor(context1, UIColor.darkGrayColor().CGColor);
        CGContextBeginPath(context1);
        createHorizontalLines(context1)
        bottomline(context1)
    }
    
    override func awakeWithContext(context: AnyObject?)
    {
        super.awakeWithContext(context)
        instanceDel.extensionGraphViewDelegate = self
        intensityLabel?.setText("")
        zoneLabel?.setText("")
        
    }
//    func getHealthDataSavedWhileAppSuspendedAndUpdateUI(){
//        if let lastAnchor = getAnchor(){
//            super.instanceDel.anchor = lastAnchor
//        }else{
//            super.instanceDel.anchor =  HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
//        }
//        
//        let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
//        heartRateQuery = HKAnchoredObjectQuery(type: quantityType!, predicate: nil, anchor: instanceDel.anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
//            
//            // print("\(sampleObjects)")
//            guard let heartRateSamples = sampleObjects as? [HKQuantitySample] else {return}
//            for sample in heartRateSamples{
//                self.handleHeartBeatSample(sample)   //It will Set the value
//            }
//            
//            super.instanceDel.anchor = newAnchor!
//            self.saveAnchor(super.instanceDel.anchor);
//            
//        }
//        instanceDel.healthStore.executeQuery(heartRateQuery!)
//        
//    }
    func clearGraph(){
        barGraphArray.removeAll()
        CGContextClearRect(context1, CGRectMake(0, 0, 272, 200));
        CGContextFlush(context1)
    }
    override func willActivate() {
        super.willActivate()
        if instanceDel.startWorkOut && !instanceDel.workoutPaused{
            animationTimer =  NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(DrawLine.heartAnimation), userInfo: nil, repeats: true)
           
                willScreenOnOffActivation = NSUserDefaults.standardUserDefaults().boolForKey("willScreenOnOffActivation")
                if willScreenOnOffActivation{
                    //Activate becuase of Screen On
                    heartReadingPlaceholderLabel?.setHidden(true)
                    heartReadingLabel?.setHidden(false)
                    NSUserDefaults.standardUserDefaults().setBool(false, forKey: "willScreenOnOffActivation")
                    NSUserDefaults.standardUserDefaults().synchronize()
                    setTimerValueFromWorkoutStartDate()
                   // let str = NSString(format: "%02d:%02d:%02d", instanceDel.timeHour,instanceDel.timeMin,instanceDel.timeSec)
                   //  heartReadingLabel?.setText(str as String)
                   heartReadingLabel?.setText(instanceDel.lastHeartBeatValue)
                    
                    
                   
                    let qualityOfServiceClass = QOS_CLASS_BACKGROUND
                    let backgroundQueue = dispatch_get_global_queue(qualityOfServiceClass, 0)
                    dispatch_async(backgroundQueue, {
                    
                        // self.instanceDel.getHealthDataSavedWhileAppSuspendedAndUpdateUI()
                    })

                }



        }else{
            heart?.setImage(UIImage(named: "HeartIconDull"))
        }

        if instanceDel.workoutPaused {
            
            heartReadingLabel?.setHidden(true)
            heartReadingPlaceholderLabel?.setHidden(false)
            isHeartDullHidden = false
            
        }
        else if !instanceDel.startWorkOut{
           
            resetView()
           
            //return;
        }

        
       
        heartReadingLabel?.setText(instanceDel.lastHeartBeatValue)
        timeLabel1.setText(instanceDel.totalWorkoutTime)
        createHorizontalLines(context1)
        bottomline(context1)
        createBar(context1)
        drawBar(context1);
        
      /*  if instanceDel.startWorkOut == true
        {
            if instanceDel.workoutPaused == false
            {
                if instanceDel.timerStart == false
                {
                    setTimer()
                }
            }
            else
            {
                timeSec = 0
                timeMin = 0
                timeHour = 0
                instanceDel.timerStart = false
                timer.invalidate()
            }
        }
        else
        {
            timeSec = 0
            timeMin = 0
            timeHour = 0
            instanceDel.timerStart = false
            timer.invalidate()
        }*/
        
        
    }
 
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
        if instanceDel.startWorkOut && !instanceDel.workoutPaused{
            animationTimer?.invalidate()
            
            if isWillDisappearCalled{
                //Screen Sliding
            }else{
                //Screen Goes off
                NSUserDefaults.standardUserDefaults().setBool(true, forKey: "willScreenOnOffActivation")
                NSUserDefaults.standardUserDefaults().synchronize()
                //  heartRateQuery!.updateHandler = nil
                
            }

        }
        isWillDisappearCalled = false
        
    }
    override func didAppear() {
//        if instanceDel.workoutPaused {
//            
//            heartReadingLabel?.setHidden(true)
//            heartReadingPlaceholderLabel?.setHidden(false)
//            isHeartDullHidden = false
//            
//        }
//        if !instanceDel.startWorkOut{
//            resetView()
//        }
//        
    
    }
    
    override func willDisappear() {
        
     //   clearGraph()
//        if !instanceDel.startWorkOut{
//            resetView()
//        }
        super.willDisappear()
        isWillDisappearCalled = true

    }
    
    
    func resetView() {
       // timer.invalidate()
        clearGraph()
        heartReadingLabel?.setText("")
        heartReadingPlaceholderLabel?.setHidden(false)
        heartReadingLabel?.setHidden(true)
        isHeartDullHidden = false
        
        intensityLabel?.setText("")
        zoneLabel?.setText("")
        timeLabel1.setText("0:00:00")
        
    }
    
    func heartAnimation(){
        
        if self.instanceDel.workoutPaused || !instanceDel.startWorkOut {
            heartReadingPlaceholderLabel?.setHidden(false)
            heartReadingLabel?.setHidden(true)
            
            return
        }
        if isHeartDullHidden { // Bright image is shown
            heart?.setImage(UIImage(named: "HeartIconDull"))
            isHeartDullHidden = false
            //            heartDullGroup?.setHidden(false)
            //            heartBrightGroup?.setHidden(true)
        }else{
            heart?.setImage(UIImage(named: "HeartIconBright"))
            isHeartDullHidden = true
            //            heartDullGroup?.setHidden(true)
            //            heartBrightGroup?.setHidden(false)
        }
    }
    
    //------------For Grid----------------------
    func createVarticalLines(context : CGContextRef)
    {
        let howMany = Int((Float(272) - kOffsetX) / kStepX)+timerValue
        for index in 0...howMany
        {
            let valueX: Float = (kOffsetX + Float(index) * kStepX)-Float(timerValue);
            CGContextMoveToPoint(context, CGFloat(valueX), CGFloat(kGraphTop))
            CGContextAddLineToPoint(context, CGFloat(valueX), CGFloat(kGraphBottom))
            
            CGContextSetLineWidth(context1, 1)
            CGContextSetStrokeColorWithColor(context, UIColor.darkGrayColor().CGColor)
            
            CGContextStrokePath(context1)

        }
    }
    
    func createHorizontalLines(context : CGContextRef)
    {
//        let howMany = Int((kGraphBottom - kGraphTop - kOffsetY) / kStepY)
        for index in 1...5{
           // let valueY: Float = (kGraphBottom - kOffsetY - Float(index) * kStepY)
           // NSLog("Y:%f", valueY)
   
            
            CGContextSetLineWidth(context, 1)
            if index == 5
            {
                value5 = 4;
                //value1 = 82; //173-91
                CGContextSetStrokeColorWithColor(context,UIColor(red: 211/255.0, green: 0/255.0, blue: 69/255.0, alpha: 1.0).CGColor)
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value5!))
                CGContextAddLineToPoint(context, CGFloat(272), CGFloat(value5!))
            }
            else  if index == 4
            {
               
                 value4 = 28;
                CGContextSetStrokeColorWithColor(context,UIColor(red: 248/255.0, green: 167/255.0, blue: 66/255.0, alpha: 1.0).CGColor)
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value4!))
                CGContextAddLineToPoint(context, CGFloat(272), CGFloat(value4!))
            }
            else if index == 3
            {
                value3 = 47;
                CGContextSetStrokeColorWithColor(context,UIColor(red: 246/255.0, green: 229/255.0, blue: 25/255.0, alpha: 1.0).CGColor)
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value3!))
                CGContextAddLineToPoint(context, CGFloat(272), CGFloat(value3!))
            }
            else if index == 2
            {
                value2 = 65;
                CGContextSetStrokeColorWithColor(context,UIColor(red: 47/255.0, green: 181/255.0, blue: 219/255.0, alpha: 1.0).CGColor)
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value2!))
                CGContextAddLineToPoint(context, CGFloat(272), CGFloat(value2!))
            }
            else if index == 1
            {
               // value5 = 4;
                
                value1 = 82;
                CGContextSetStrokeColorWithColor(context,UIColor(red: 28/255.0, green: 105/255.0, blue: 151/255.0, alpha: 1.0).CGColor)
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value1!))
                CGContextAddLineToPoint(context, CGFloat(272), CGFloat(value1!))
            }
              CGContextStrokePath(context)
        }

    }
    //[WKInterfaceDevice currentDevice].screenBounds
    func createBar(context:CGContextRef)
    {
        if barGraphArray.count > 0
        {
            for var index = barGraphArray.count-1; index >= 0; index -= 1
            {
                let width = self.width
                let offSet = Int(kBarStepX)
                
                let barX:Float = width - Float((barGraphArray.count - index)  * offSet)
                let barY:Float = 173;
                let barHeight =  barGraphArray[index]
                //barHeight = barHeight * (100.0/169)
                
//                if(barHeight < 91)
//                {
//                    barHeight = barHeight*Double(value1!/91)
//                }
//                else if(barHeight>=91.0 && barHeight<107)
//                {
//                    barHeight = barHeight*Double(value2!/107)
//                }
//                else if(barHeight>=108 && barHeight<126)
//                {
//                    barHeight = barHeight*Double(value3!/125)
//                }
//                else if(barHeight>=126.0 && barHeight<145.0)
//                {
//                    barHeight = barHeight*Double(value4!/144)
//                }
//                else if(barHeight >= 145 && barHeight < 169)
//                {
//                    barHeight = barHeight*Double(value5!/169)
//                }
//                else if ( barHeight >= 169){
//                    barHeight = barHeight*Double(value5!/180)
//                }
                
              /*  if(barHeight < 91)
                {
                    barHeight = (barHeight/91.0) * Double(value1!)  //Rest Zone
                }
                else if(barHeight>=91.0 && barHeight<108)
                {
                    barHeight = (barHeight/108.0) * Double(value2!)  //Warm UP
                }
                else if(barHeight>=108 && barHeight<126)
                {
                    barHeight = (barHeight/126.0) * Double(value3!) //Endurance
                }
                else if(barHeight>=126.0 && barHeight < 145.0)
                {
                    barHeight = (barHeight/145.0) * Double(value4!) //Cardio
                }
                else if(barHeight >= 145 && barHeight < 169) //
                {
                    barHeight = (barHeight/169.0) * Double(value5!) //Power
                }
                else if ( barHeight >= 169){
                    barHeight = barHeight*Double(value5!/180)   //VO2 Max
                }*/
                
                let barRect:CGRect = CGRectMake(CGFloat(barX), CGFloat(barY), CGFloat(kBarWidth), (-1)*CGFloat(barHeight));
                
                currentBarValue =  Double(barGraphArray[index])
                barcolorwithvalue(context, a: currentBarValue)
                CGContextMoveToPoint(context, CGRectGetMinX(barRect), CGRectGetMinY(barRect));
                CGContextAddLineToPoint(context, CGRectGetMaxX(barRect), CGRectGetMinY(barRect));
                CGContextAddLineToPoint(context, CGRectGetMaxX(barRect), CGRectGetMaxY(barRect));
                CGContextAddArcToPoint(context, CGRectGetMinX(barRect), CGRectGetMaxY(barRect), CGRectGetMaxX(barRect), CGRectGetMaxY(barRect), 8.0);
                CGContextAddLineToPoint(context, CGRectGetMinX(barRect), CGRectGetMaxY(barRect));
                CGContextClosePath(context);
                CGContextFillPath(context);
            }
        }
    }
    
    func bottomline(context:CGContextRef)
    {
        CGContextSetLineWidth(context, 20)
        let ypoint : CGFloat = 173
        CGContextMoveToPoint(context, 0, ypoint)
        
        CGContextMoveToPoint(context, CGFloat(0), CGFloat(ypoint))
        CGContextAddLineToPoint(context, CGFloat(272), CGFloat(ypoint))
        
        CGContextSetLineWidth(context1, 2)
      
        CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
      
        CGContextStrokePath(context1)
    }
    
    //--------For color of bar
    
    func barcolorwithvalue(context:CGContextRef, a:Double)
    {
        if a<=90
        {
            CGContextSetRGBFillColor(context,155/255.0,155/255.0,155/255.0,1)
        }
        else if a>=91 && a<=107
        {
            CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if a>=108 && a<=125
        {
            CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if a>=126 && a<=144
        {
            CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if a>=145 && a<=168
        {
            CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if a>=169
        {
            CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
    }
    
    
    func drawBar(context:CGContextRef)-> Void
    {
        var cgimage :CGImageRef?  = CGBitmapContextCreateImage(context)
        let uiimage = UIImage(CGImage: cgimage!)
        
        UIGraphicsEndImageContext()
        imageView?.setImage(uiimage)
        cgimage = nil
    }
    

    
    func valuechanged(a:Double)
    {
        
       // readingGroup?.setHidden(true)
        
      //  currentHeartRateLabel?.setHidden(false)
        intensityGroup?.setAlpha(1.0)
        // print(barGraphArray)
        
        //  print(barGraphArray)
       // createtext()
        
        barGraphArray.append(a)
        
        
        
        
        //  print(barGraphArray)
        
        selectZone(a)
        
        //bpmLbl?.setHidden(true)
        let currentRate = NSString(format: "%d",Int(a)) as String

        heartReadingLabel?.setText(currentRate)
        heartReadingPlaceholderLabel?.setHidden(true)
        heartReadingLabel?.setHidden(false)

        //currentHeartRateLabel?.setText(currentRate)
        //currentHeartRateLabel?.setTextColor(UIColor.whiteColor())
        
        CGContextSetStrokeColorWithColor(context1, UIColor.darkGrayColor().CGColor);
        if(context1 != nil)
        {
            CGContextClearRect(context1, CGRectMake(0, 0, 272, 200));
            
            CGContextFlush(context1)
            //context1 = nil
            CGContextBeginPath(context1);
           
            //createVarticalLines(context1)
            createHorizontalLines(context1)
            bottomline(context1)
            createBar(context1)
            drawBar(context1);
        }
    }
    
    
    
    func selectZone(a:Double)
    {
        let value = a
        let age = 22
        let maxHeartRate :Double = Double(220-age)
        var intensity  = Double(value/maxHeartRate)
        intensity = intensity*100
        
        let intensityStr = NSString(format: " %d%@", Int(intensity),"%")
        intensityLabel?.setText(intensityStr as String)
        
        if value < 91
        {
            zoneLabel?.setText(" REST")
            zoneLabel?.setTextColor(UIColor(red: 1.0, green:1.0, blue: 1.0, alpha: 1))
            intensityLabel?.setTextColor(UIColor(red: 1.0, green:1.0, blue: 1.0, alpha: 1))
        }
        else if value >= 91 && value <= 107
        {
            zoneLabel?.setText(" WARM UP")
            zoneLabel?.setTextColor(UIColor(red: 33/255.0, green: 125/255.0, blue: 168/255.0, alpha: 1))
            intensityLabel?.setTextColor(UIColor(red: 33/255.0, green: 125/255.0, blue: 168/255.0, alpha: 1))
        }
        else if value >= 108 && value <= 125
        {
            zoneLabel?.setText(" ENDURANCE")
            zoneLabel?.setTextColor(UIColor(red: 51/255.0, green: 199/255.0, blue: 231/255.0, alpha: 1))
             intensityLabel?.setTextColor(UIColor(red: 51/255.0, green: 199/255.0, blue: 231/255.0, alpha: 1))
        }
        else if value >= 126 && value <= 144
        {
            zoneLabel?.setText(" CARDIO")
            zoneLabel?.setTextColor(UIColor(red: 243/255.0, green: 226/255.0, blue: 28/255.0, alpha: 1))
            intensityLabel?.setTextColor(UIColor(red: 243/255.0, green: 226/255.0, blue: 28/255.0, alpha: 1))
        }
        else if value >= 145 && value <= 168
        {
            zoneLabel?.setText(" POWER")
            zoneLabel?.setTextColor(UIColor(red: 225/255.0, green: 152/255.0, blue: 60/255.0, alpha: 1))
            intensityLabel?.setTextColor(UIColor(red: 225/255.0, green: 152/255.0, blue: 60/255.0, alpha: 1))
        }
        else if value >= 169 && value <= 180
        {
            zoneLabel?.setText(" VO2 MAX")
            zoneLabel?.setTextColor(UIColor(red: 211/255.0, green:0/255.0,blue: 69/255.0, alpha: 1))
            intensityLabel?.setTextColor(UIColor(red: 211/255.0, green:0/255.0,blue: 69/255.0, alpha: 1))
        }
    }
    
//    func handleHeartBeatSample(sample:HKQuantitySample)
//    {
//        //self.value = Double(arc4random_uniform(80) + 100)  //for testing only
//        
//        if !instanceDel.workoutPaused{
//            var timeInSecs = 0
//            self.value = sample.quantity.doubleValueForUnit(instanceDel.heartRateUnit)
//            // print("heart rate \(self.value)")
//            self.instanceDel.heartDataArray.append(self.value)
//            
//            let formatter = NSDateFormatter()
//            formatter.dateFormat = "dd/MM/YYYY HH:mm:ss a"
//            let sampleStartDate :String = formatter.stringFromDate(sample.startDate)
//            let sampleStr = NSString(format: "%f%@%@",Float(self.value),"s",sampleStartDate)
//
//           
//            instanceDel.totalValueCount += 1
//            let currentRate = NSString(format: "%d", Int(self.value)) as String
//            
//            instanceDel.totalAvg = instanceDel.totalAvg + Double(currentRate)!
//            
//            //find max
//            if instanceDel.previousValue < Int(self.value)
//            {
//                instanceDel.previousValue = Int(self.value)
//                
//                let maxStr = NSString(format: "%@ %d", "MAX",instanceDel.previousValue) as String
//                
//                self.instanceDel.maximumHeartRate = maxStr
//            }
//            
//            let avg = (Int(instanceDel.totalAvg)/instanceDel.totalValueCount)
//            let avgStr = NSString(format: "%@ %d", "AVG",avg) as String
//            self.instanceDel.averageHeartRate = avgStr
//            
//            // self.test = self.test + 0.1
//            
//            
//            //self.value  = 150
//            timeInSecs = instanceDel.timeHour*60*60 + instanceDel.timeMin*60 + instanceDel.timeSec
//            self.instanceDel.lastHeartBeatValue = String(Int(self.value))
//            self.instanceDel.passValueToZoneHistory(self.value, time: timeInSecs, sessionEnd: false, count: instanceDel.totalValueCount)
//            self.instanceDel.addNewPointValue(self.value)
//            
//        }
//        
//    }

    
}


