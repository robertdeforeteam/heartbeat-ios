//
//  ExtensionDelegate.swift
//  HeartBeat-WatchKitApp Extension
//
//  Created by Rupinder-Mac on 10/12/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import WatchKit
import HealthKit

protocol ExtensionGraphViewDelegate
    
{
    func valuechanged( val:Double)
    func totalWorkoutTime(val:String)
    func resetView()
    
}

protocol ExtensionZoneHelperDelegate
{
    func heartRateAndTime(val:Double, time:Int ,start:Bool,count:Int)
    func totalWorkoutTime(val:String)
    func resetView()
    
}



class ExtensionDelegate {
    
    var age: Int = 22
    var samples : [HKSample] = []
    var workoutActivityType : HKWorkoutActivityType = .Running
    var value = 0.0
    var heartRateQuery : HKAnchoredObjectQuery?
    var heartDataArray : [Double]  = []
    
    var startDate = NSDate()
    var endDate = NSDate()
    
    var workoutPaused = false
    var startWorkOut = false
   // var stopTimer = false
    var timerStart = false
    
    var totalWorkoutTime = "0:00:00"
    var totalPauseTimeDuringWorkout = 0
    var lastPauseDateTime : NSDate!
    var maximumHeartRate = ""
    var averageHeartRate = ""
    var lastHeartBeatValue = ""
    
    var zone0Percentage = "0.0%"
    var zone1Percentage = "0.0%"
    var zone2Percentage = "0.0%"
    var zone3Percentage = "0.0%"
    var zone4Percentage = "0.0%"
    var zone5Percentage = "0.0%"
   
    
    var zone0Time = "0:00:00"
    var zone1Time = "0:00:00"
    var zone2Time = "0:00:00"
    var zone3Time = "0:00:00"
    var zone4Time = "0:00:00"
    var zone5Time = "0:00:00"
    
    var zone0Width = 0.0
    var zone1Width = 0.0
    var zone2Width = 0.0
    var zone3Width = 0.0
    var zone4Width = 0.0
    var zone5Width = 0.0
    
    var isComingFromSummaryPage:Bool = false
    
    static let instance = ExtensionDelegate()
    var extensionGraphViewDelegate: ExtensionGraphViewDelegate?
    var extensionZoneHelperDelegate: ExtensionZoneHelperDelegate?
    let healthStore = HKHealthStore()
    var authorization = false
    
    let heartRateUnit = HKUnit.countUnit().unitDividedByUnit(HKUnit.minuteUnit())
    
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    
    var timeSec = 0
    var timeMin = 0
    var timeHour = 0
   
    var workoutSessionStartDate : NSDate = NSDate()
    var previousValue:Int = 0
    var totalValueCount:Int = 0
    var totalAvg:Double = 0.0
    
    func addNewPointValue(a:Double)
    {
        //This data is passed to GraphView
        self.extensionGraphViewDelegate?.valuechanged(a)
    }
    func passValueToZoneHistory(a:Double,time:Int,sessionEnd:Bool,count:Int)
    {
        //This data is passed to ZoneView
        self.extensionZoneHelperDelegate?.heartRateAndTime(a, time:time, start:sessionEnd,count: count)
    }
    func totalWorkoutTime(val:String){
        // This is the time for Zone
         self.extensionZoneHelperDelegate?.totalWorkoutTime(val)
         self.extensionGraphViewDelegate?.totalWorkoutTime(val)
    }
    func resetView(){
        
       
       totalWorkoutTime = "0:00:00"
       maximumHeartRate = ""
       averageHeartRate = ""
      
       zone0Percentage = "0.0%"
       zone1Percentage = "0.0%"
       zone2Percentage = "0.0%"
       zone3Percentage = "0.0%"
       zone4Percentage = "0.0%"
       zone5Percentage = "0.0%"
        
       zone0Time = "0:00:00"
       zone1Time = "0:00:00"
       zone2Time = "0:00:00"
       zone3Time = "0:00:00"
       zone4Time = "0:00:00"
       zone5Time = "0:00:00"
      
        
        zone0Width = 2.0
        zone1Width = 2.0
        zone2Width = 2.0
        zone3Width = 2.0
        zone4Width = 2.0
        zone5Width = 2.0

        heartDataArray.removeAll()
        samples.removeAll()
        self.extensionZoneHelperDelegate?.resetView()
        self.extensionGraphViewDelegate?.resetView()
    }
    
    /*
    func getHealthDataSavedWhileAppSuspendedAndUpdateUI(){
        if let lastAnchor = getAnchor(){
            self.anchor = lastAnchor
        }else{
            self.anchor =  HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
        }
        
        let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        heartRateQuery = HKAnchoredObjectQuery(type: quantityType!, predicate: nil, anchor: self.anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            // print("\(sampleObjects)")
            guard let heartRateSamples = sampleObjects as? [HKQuantitySample] else {return}
            for sample in heartRateSamples{
                self.handleHeartBeatSample(sample)   //It will Set the value
                
                
                //Write Code Here to Update the graphcs or check if querySample Auto Update
            }
            
            self.anchor = newAnchor!
            self.saveAnchor(self.anchor);
            
        }
        
        self.healthStore.executeQuery(heartRateQuery!)
        
    }*/
    
    /*
    func handleHeartBeatSample(sample:HKQuantitySample)
    {
        //self.value = Double(arc4random_uniform(80) + 100)  //for testing only
        
        if !self.workoutPaused{
            var timeInSecs = 0
            self.value = sample.quantity.doubleValueForUnit(self.heartRateUnit)
            // print("heart rate \(self.value)")
            
          //  self.value = 50.0 +  Double(arc4random_uniform(UInt32(150)))
            
            
            self.heartDataArray.append(self.value)
            self.totalValueCount += 1
            let currentRate = NSString(format: "%d", Int(self.value)) as String
            
            self.totalAvg = self.totalAvg + Double(currentRate)!
            
            //find max
            if self.previousValue < Int(self.value)
            {
                self.previousValue = Int(self.value)
                
                let maxStr = NSString(format: "%@ %d", "MAX",self.previousValue) as String
                
                self.maximumHeartRate = maxStr
            }
            
            let avg = (Int(self.totalAvg)/self.totalValueCount)
            let avgStr = NSString(format: "%@ %d", "AVG",avg) as String
            self.averageHeartRate = avgStr
            
            // self.test = self.test + 0.1
            
            
            //self.value  = 150
            timeInSecs = self.timeHour*60*60 + self.timeMin*60 + self.timeSec
            self.lastHeartBeatValue = String(Int(self.value))
            self.passValueToZoneHistory(self.value, time: timeInSecs, sessionEnd: false, count: self.totalValueCount)
            self.addNewPointValue(self.value)
            
        }
        
    }
 
     */
    
    

    /*
    let AnchorKey = "HKClientAnchorKey"
    func getAnchor() -> HKQueryAnchor? {
        let encoded = NSUserDefaults.standardUserDefaults().dataForKey(AnchorKey)
        if(encoded == nil){
            return nil
        }
        let anchor = NSKeyedUnarchiver.unarchiveObjectWithData(encoded!) as? HKQueryAnchor
        return anchor
    }
    
    func saveAnchor(anchor : HKQueryAnchor) {
        let encoded = NSKeyedArchiver.archivedDataWithRootObject(anchor)
        NSUserDefaults.standardUserDefaults().setValue(encoded, forKey: AnchorKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    */
    
    func getUserAge(){
       
        
        do {
            let dateComponet =  try healthStore.dateOfBirthComponents()
            let dob = dateComponet.date
            let todaysDate = NSDate()
            
            
            let seconds =  todaysDate.timeIntervalSinceDate(dob!) as Double
            let years =  seconds / ( 60.0 * 60.0 * 24.0 * 365.0)
            self.age = Int(years)
        } catch  {
            
        }
    
    }
    
    
    
    

}
