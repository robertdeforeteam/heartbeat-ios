//
//  SummaryInterfaceController.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 11/13/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import WatchKit
import Foundation
import HealthKit
import WatchConnectivity

enum MyError: ErrorType {case Err}

class SummaryInterfaceController: WKInterfaceController,WCSessionDelegate {
    
    // let healthStore = HKHealthStore()
    
    var session :WCSession!
    
    @IBOutlet var totalCaloriesLbl : WKInterfaceLabel?
    @IBOutlet var activeCaloriesLbl : WKInterfaceLabel?
    
    @IBOutlet var workoutTimeLbl : WKInterfaceLabel?
    @IBOutlet var maxHeartLbl : WKInterfaceLabel?
    @IBOutlet var avgHeartLbl : WKInterfaceLabel?
    
    @IBOutlet var zoneTime0Lbl : WKInterfaceLabel?
    @IBOutlet var zoneTime1Lbl : WKInterfaceLabel?
    @IBOutlet var zoneTime2Lbl : WKInterfaceLabel?
    @IBOutlet var zoneTime3Lbl : WKInterfaceLabel?
    @IBOutlet var zoneTime4Lbl : WKInterfaceLabel?
    @IBOutlet var zoneTime5Lbl : WKInterfaceLabel?
    
    @IBOutlet var zonePercentage0Lbl : WKInterfaceLabel?
    @IBOutlet var zonePercentage1Lbl : WKInterfaceLabel?
    @IBOutlet var zonePercentage2Lbl : WKInterfaceLabel?
    @IBOutlet var zonePercentage3Lbl : WKInterfaceLabel?
    @IBOutlet var zonePercentage4Lbl : WKInterfaceLabel?
    @IBOutlet var zonePercentage5Lbl : WKInterfaceLabel?
    
    @IBOutlet var zone0WdithGrp : WKInterfaceGroup?
    @IBOutlet var zone1WdithGrp : WKInterfaceGroup?
    @IBOutlet var zone2WdithGrp : WKInterfaceGroup?
    @IBOutlet var zone3WdithGrp : WKInterfaceGroup?
    @IBOutlet var zone4WdithGrp : WKInterfaceGroup?
    @IBOutlet var zone5WdithGrp : WKInterfaceGroup?
    
    /*   private var activeEnergyBurned: Double = 0.0 {
    didSet {
    didSetActiveEnergyBurned(oldValue)
    }
    }
    private var restingEnergyBurned: Double = 0.0 {
    didSet {
    didSetRestingEnergyBurned(oldValue)
    }
    }
    private var energyConsumed: Double = 0.0 {
    didSet {
    didSetEnergyConsumed(oldValue)
    }
    }
    private var netEnergy: Double = 0.0 {
    didSet {
    didSetNetEnergy(oldValue)
    }
    }*/
    
    var age:Int?
    let instanceDel = ExtensionDelegate.instance
    var biologicalSex:HKBiologicalSexObject?
    var totalEnergyConsumption = 0.0
    var activeCalories = 0.0
    var usersHeight = 0.0
    var restingCalories = 0.0
    var weightInKg = 0.0
    
     func session(session: WCSession, activationDidCompleteWithState activationState: WCSessionActivationState, error: NSError?){
        
        
    }
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // refreshStatistics()
        
        
        // Configure interface objects here.
    }
    
    override func willActivate()
    {

        // This method is called when watch view controller is about to be visible to user
        if (WCSession.isSupported()) {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
        }
        super.willActivate()
        workoutTimeLbl?.setText(instanceDel.totalWorkoutTime)
        maxHeartLbl?.setText(instanceDel.maximumHeartRate)
        avgHeartLbl?.setText(instanceDel.averageHeartRate)
        
       
        zoneTime0Lbl?.setText(instanceDel.zone0Time)
        zoneTime1Lbl?.setText(instanceDel.zone1Time)
        zoneTime2Lbl?.setText(instanceDel.zone2Time)
        zoneTime3Lbl?.setText(instanceDel.zone3Time)
        zoneTime4Lbl?.setText(instanceDel.zone4Time)
        zoneTime5Lbl?.setText(instanceDel.zone5Time)
        
        
        zonePercentage0Lbl?.setText(instanceDel.zone0Percentage)
        zonePercentage1Lbl?.setText(instanceDel.zone1Percentage)
        zonePercentage2Lbl?.setText(instanceDel.zone2Percentage)
        zonePercentage3Lbl?.setText(instanceDel.zone3Percentage)
        zonePercentage4Lbl?.setText(instanceDel.zone4Percentage)
        zonePercentage5Lbl?.setText(instanceDel.zone5Percentage)
        
        let widthZone0 = CGFloat(instanceDel.zone0Width) >= 2 ? CGFloat(instanceDel.zone0Width) : 2
        let widthZone1 = CGFloat(instanceDel.zone1Width) >= 2 ? CGFloat(instanceDel.zone1Width) : 2
        let widthZone2 = CGFloat(instanceDel.zone2Width) >= 2 ? CGFloat(instanceDel.zone2Width) : 2
        let widthZone3 = CGFloat(instanceDel.zone3Width) >= 2 ? CGFloat(instanceDel.zone3Width) : 2
        let widthZone4 = CGFloat(instanceDel.zone4Width) >= 2 ? CGFloat(instanceDel.zone4Width) : 2
        let widthZone5 = CGFloat(instanceDel.zone5Width) >= 2 ? CGFloat(instanceDel.zone5Width) : 2
        
        zone0WdithGrp?.setWidth(widthZone0)
        zone1WdithGrp?.setWidth(widthZone1)
        zone2WdithGrp?.setWidth(widthZone2)
        zone3WdithGrp?.setWidth(widthZone3)
        zone4WdithGrp?.setWidth(widthZone4)
        zone5WdithGrp?.setWidth(widthZone5)
        
        maxHeartLbl?.setText(instanceDel.maximumHeartRate)
        avgHeartLbl?.setText(instanceDel.averageHeartRate)
    }
    /*func refreshStatistics() {
    
    let energyConsumedType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
    let activeEnergyBurnType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
    
    //let totalEnergy = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBasalEnergyBurned)
    // First, fetch the sum of energy consumed samples from HealthKit. Populate this by creating your
    // own food logging app or using the food journal view controller.
    self.fetchSumOfSamplesTodayForType(energyConsumedType!, unit: HKUnit.kilocalorieUnit()) {totalJoulesConsumed, error in
    
    // Next, fetch the sum of active energy burned from HealthKit. Populate this by creating your
    // own calorie tracking app or the Health app.
    self.fetchSumOfSamplesTodayForType(activeEnergyBurnType!, unit: HKUnit.kilocalorieUnit()) {activeEnergyBurned, error in
    
    // Last, calculate the user's basal energy burn so far today.
    self.fetchTotalBasalBurn {basalEnergyBurn, error in
    
    if basalEnergyBurn == nil {
    NSLog("An error occurred trying to compute the basal energy burn. In your app, handle this gracefully. Error: \(error)")
    }
    
    // Update the UI with all of the fetched values.
    dispatch_async(dispatch_get_main_queue()) {
    self.activeEnergyBurned = activeEnergyBurned
    
    self.restingEnergyBurned = basalEnergyBurn?.doubleValueForUnit(HKUnit.kilocalorieUnit()) ?? 0.0
    
    self.energyConsumed = totalJoulesConsumed
    
    if self.energyConsumed == 0.0
    {
    let weight = 75.0
    self.energyConsumed = (weight*4*24)/4.18
    }
    
    self.netEnergy = self.energyConsumed - self.activeEnergyBurned - self.restingEnergyBurned
    
    let totalEnergy = NSString(format: "%0.2f", self.energyConsumed)
    let activeEnergy = NSString(format: "%0.2f", self.netEnergy)
    self.totalCaloriesLbl?.setText(totalEnergy as String)
    self.activeCaloriesLbl?.setText(activeEnergy as String)
    }
    }
    }
    }
    }*/
    
    /*    private func fetchSumOfSamplesTodayForType(quantityType: HKQuantityType, unit: HKUnit, completion completionHandler: ((Double, NSError?)->Void)?) {
    let predicate = predicateForSamplesToday()
    
    let query = HKStatisticsQuery(quantityType: quantityType, quantitySamplePredicate: predicate, options: .CumulativeSum) {query, result, error in
    let sum = result?.sumQuantity()
    
    if completionHandler != nil {
    let value: Double = sum?.doubleValueForUnit(unit) ?? 0.0
    
    completionHandler!(value, error)
    }
    }
    
    self.healthStore.executeQuery(query)
    }*/
    
    // Calculates the user's total basal (resting) energy burn based off of their height, weight, age,
    // and biological sex. If there is not enough information, return an error.
    /* private func fetchTotalBasalBurn(completion: (HKQuantity?, NSError?)->Void) {
    let todayPredicate = predicateForSamplesToday()
    
    let weightType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
    let heightType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
    
    aapl_mostRecentQuantitySampleOfType(weightType!, predicate: nil) {weight, error in
    if weight == nil {
    completion(nil, error)
    
    return
    }
    
    self.aapl_mostRecentQuantitySampleOfType(heightType!, predicate: todayPredicate) {height, error in //NOTE: this error may have NSError from aapl_mostRecentQuantitySampleOfType
    if height == nil {
    completion(nil, error)
    
    return
    }
    
    let dateOfBirth: NSDate?
    do {
    dateOfBirth = try self.healthStore.dateOfBirth()
    } catch let error as NSError {
    completion(nil, error)
    
    return
    } catch {
    fatalError()
    }
    
    let biologicalSexObject: HKBiologicalSexObject?
    do {
    biologicalSexObject = try self.healthStore.biologicalSex()
    } catch let error as NSError {
    completion(nil, error)
    
    return
    } catch {
    fatalError()
    }
    
    // Once we have pulled all of the information without errors, calculate the user's total basal energy burn
    let basalEnergyBurn = self.calculateBasalBurnTodayFromWeight(weight!, height: height!, dateOfBirth: dateOfBirth!, biologicalSex: biologicalSexObject!)
    
    completion(basalEnergyBurn, nil)
    }
    }
    }*/
    
    /*  func aapl_mostRecentQuantitySampleOfType(quantityType: HKQuantityType, predicate: NSPredicate?, completion: ((HKQuantity?, NSError?)->Void)?) {
    let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
    
    // Since we are interested in retrieving the user's latest sample, we sort the samples in descending order, and set the limit to 1. We are not filtering the data, and so the predicate is set to nil.
    let query = HKSampleQuery(sampleType: quantityType, predicate: nil, limit: 1, sortDescriptors: [timeSortDescriptor]) {query, results, error in
    if results == nil {
    completion?(nil, error)
    
    return
    }
    
    if completion != nil {
    // If quantity isn't in the database, return nil in the completion block.
    let quantitySample = results!.first as? HKQuantitySample
    let quantity = quantitySample?.quantity
    
    completion!(quantity, error)
    }
    }
    
    healthStore.executeQuery(query)
    }*/
    
    
    /* private func calculateBasalBurnTodayFromWeight(weight: HKQuantity, height: HKQuantity, dateOfBirth: NSDate, biologicalSex: HKBiologicalSexObject) -> HKQuantity {
    // Only calculate Basal Metabolic Rate (BMR) if we have enough information about the user
    
    // Note the difference between calling +unitFromString: vs creating a unit from a string with
    // a given prefix. Both of these are equally valid, however one may be more convenient for a given
    // use case.
    let heightInCentimeters = height.doubleValueForUnit(HKUnit(fromString: "cm"))
    let weightInKilograms = weight.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo))
    //
    let now = NSDate()
    let ageComponents = NSCalendar.currentCalendar().components(.Year, fromDate: dateOfBirth, toDate: now, options: .WrapComponents)
    let ageInYears = ageComponents.year
    
    // BMR is calculated in kilocalories per day.
    let BMR = self.calculateBMRFromWeight(weightInKilograms, height: heightInCentimeters, age: ageInYears, biologicalSex: biologicalSex.biologicalSex)
    
    // Figure out how much of today has completed so we know how many kilocalories the user has burned.
    let startOfToday = NSCalendar.currentCalendar().startOfDayForDate(now)
    let endOfToday = NSCalendar.currentCalendar().dateByAddingUnit(.Day, value: 1, toDate: startOfToday, options: [])
    
    let secondsInDay = endOfToday!.timeIntervalSinceDate(startOfToday)
    let percentOfDayComplete = now.timeIntervalSinceDate(startOfToday) / secondsInDay
    
    let kilocaloriesBurned = BMR * percentOfDayComplete
    
    return HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: kilocaloriesBurned)
    }*/
    
    //MARK: - Convenience
    
    private func predicateForSamplesToday() -> NSPredicate {
        let calendar = NSCalendar.currentCalendar()
        
        let now = NSDate()
        
        let startDate = calendar.startOfDayForDate(now)
        let endDate = calendar.dateByAddingUnit(.Day, value: 1, toDate: startDate, options: [])
        
        return HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .StrictStartDate)
    }
    
    /// Returns BMR value in kilocalories per day. Note that there are different ways of calculating the
    /// BMR. In this example we chose an arbitrary function to calculate BMR based on weight, height, age,
    /// and biological sex.
    /* private func calculateBMRFromWeight(weightInKilograms: Double, height heightInCentimeters: Double, age ageInYears: Int, biologicalSex: HKBiologicalSex) -> Double {
    var BMR: Double = 0.0
    
    // The BMR equation is different between males and females.
    if biologicalSex == .Male {
    BMR = 66.0 + (13.8 * weightInKilograms) + (5.0 * heightInCentimeters) - (6.8 * Double(ageInYears))
    } else {
    BMR = 655 + (9.6 * weightInKilograms) + (1.8 * heightInCentimeters) - (4.7 * Double(ageInYears))
    }
    
    return BMR
    }*/
    
    //MARK: - NSEnergyFormatter
    
    private lazy var energyFormatter: NSEnergyFormatter = {
        let formatter = NSEnergyFormatter()
        formatter.unitStyle = .Long
        formatter.forFoodEnergyUse = true
        formatter.numberFormatter.maximumFractionDigits = 2
        return formatter
    }()
    
    //MARK: - Setter Overrides
    
    /*  private func didSetActiveEnergyBurned(oldValue: Double) {
    
    
    }
    
    private func didSetEnergyConsumed(oldValue: Double) {
    
    
    }
    
    private func didSetRestingEnergyBurned(oldValue: Double) {
    
    
    }
    
    private func didSetNetEnergy(oldValue: Double) {
    
    
    }*/
    
    @IBAction func save()
    {
        saveWorkout()
        instanceDel.resetView()
    }
    
    // MARK: Workout Methods
    
    func saveWorkout()
    {
        //        let workoutTime = instanceDel.totalWorkoutTime
        //
        //        let arr = workoutTime.componentsSeparatedByString(":")
        //
        //        let hr = Int(arr[0])
        //        let min = Int(arr[1])
        //        let sec = Int(arr[2])
        
        //        let hr = instanceDel.timeHour
        //        let min = instanceDel.timeMin
        //        let sec = instanceDel.timeSec
        
        // let distanceQuantity = HKQuantity(unit: HKUnit.mileUnit(), doubleValue: 0.0)
        // let caloriesQuantity = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue:self.activeEnergyBurned)
        
        //?????
        
        
        
        let workout = HKWorkout(activityType: instanceDel.workoutActivityType, startDate: instanceDel.workoutSessionStartDate, endDate: instanceDel.endDate)
        instanceDel.healthStore.saveObject(workout, withCompletion: {_,_ in 
            
            
            //SAve all Samples to this workout
            self.instanceDel.healthStore.addSamples(self.instanceDel.samples, toWorkout: workout, completion: { (success, error) in
                //
                
               /* let applicationData = ["Value":"","Start":false,"loadWorkout":true,"Paused":false,"workOutStartDate":self.instanceDel.startDate,"workOutEndDate":self.instanceDel.endDate,"HeartRateInfo":self.instanceDel.heartDataArray,"ActivityType":(self.instanceDel.workoutActivityType.rawValue)]
                
                self.session.sendMessage(applicationData, replyHandler: {applicationData -> Void in
                    }, errorHandler: {(error ) -> Void in
                        print(error)
                        // catch any errors here
                })*/
                
                self.dismissController()
            });
     
        })
      
     
        
        
    }
    
    
    /*   func getTotalEnergyConsumption(weight:Double)
    {
    //let total energy = weight
    }*/
    
    /*func readBurnedCalories(sampleType:HKSampleType , completion: ((HKSample!, NSError!) -> Void)!)
    {
    let now   = NSDate()
    let periodComponents = NSDateComponents()
    // periodComponents.month = -1
    periodComponents.minute = -1
    
    let then = NSCalendar.currentCalendar().dateByAddingComponents(
    periodComponents,
    toDate:now,
    options: [])!
    print(now)
    print(then)
    
    let past = NSDate.distantPast()
    
    let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
    
    let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
    
    let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
    { (sampleQuery, results, error ) -> Void in
    
    if error != nil {
    completion(nil,error)
    return;
    }
    else
    {
    let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
    self.readTotalEnergyConsumed(heartSampleType!, completion: { (mostRecentHeartRate, error) -> Void in
    
    if( error != nil )
    {
    print("Error reading heart rate from HealthKit Store: \(error.localizedDescription)")
    return;
    }
    });
    }
    }
    healthStore.executeQuery(sampleQuery)
    }
    
    func readTotalEnergyConsumed(sampleType:HKSampleType , completion: ((HKSample!, NSError!) -> Void)!)
    {
    let now   = NSDate()
    let periodComponents = NSDateComponents()
    // periodComponents.month = -1
    periodComponents.minute = -1
    
    let then = NSCalendar.currentCalendar().dateByAddingComponents(
    periodComponents,
    toDate:now,
    options: [])!
    print(now)
    print(then)
    
    let past = NSDate.distantPast()
    
    let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
    
    let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
    
    let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
    { (sampleQuery, results, error ) -> Void in
    
    if let _ = error {
    completion(nil,error)
    return;
    }
    }
    healthStore.executeQuery(sampleQuery)
    }*/
    
    //MARK: View LifeCycle Methods
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        
        super.didDeactivate()
    }
    
    override func willDisappear() {
        super.willDisappear()
        instanceDel.isComingFromSummaryPage = true
        instanceDel.resetView()
        
    }
    @IBAction func deleteButtonPressed()
    {
        let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        self.deleteHeartRateData(heartSampleType!, startDate: instanceDel.startDate, endDate: NSDate())
         instanceDel.resetView()
    }
    
    /*func saveHeartRateSample(heartRate:Double, date:NSDate )
    {
    // 1. Create a Heartrate Sample
    
    let heartRateUnit = HKUnit(fromString: "kcal")
    HKUnit.minuteUnit()
    let heartRateType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
    let heartRateQuantity = HKQuantity(unit: heartRateUnit, doubleValue: heartRate)
    let heartRateSample = HKQuantitySample(type: heartRateType!, quantity: heartRateQuantity, startDate: instanceDel.startDate , endDate: date)
    
    // 2. Save the sample in the store
    instanceDel.healthStore.saveObject(heartRateSample, withCompletion: { (success, error) -> Void in
    if( error != nil )
    {
    print("Error saving Heartrate sample: \(error!.localizedDescription)")
    }
    else
    {
    print("Heartrate sample saved successfully!")
    }
    })
    }*/
    
    /*func updateUsersWeight()
    {
    // Query to get the user's latest weight, if it exists.
    let weightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!
    
    aapl_mostRecentQuantitySampleOfType(weightType, predicate: nil) {mostRecentQuantity, error in
    if mostRecentQuantity == nil {
    NSLog("Either an error occured fetching the user's weight information or none has been stored yet. In your app, try to handle this gracefully.")
    
    dispatch_async(dispatch_get_main_queue()) {
    // self.weightValueLabel.text = NSLocalizedString("Not available", comment: "")
    }
    } else {
    // Determine the weight in the required unit.
    let weightUnit = HKUnit.gramUnit()
    var usersWeight = mostRecentQuantity!.doubleValueForUnit(weightUnit)
    usersWeight = 75.0
    
    // Update the user interface.
    dispatch_async(dispatch_get_main_queue()) {
    
    self.weightInKg = usersWeight/1000
    
    self.totalEnergyConsumption = (self.weightInKg*4*24)/4.18
    
    let totalEnergyStr =  NSString(format: "%f", self.totalEnergyConsumption)
    
    self.totalCaloriesLbl?.setText(totalEnergyStr as String)
    
    }
    }
    }
    }
    
    func updateUsersHeight()
    {
    let lengthFormatter = NSLengthFormatter()
    lengthFormatter.unitStyle = NSFormattingUnitStyle.Long
    
    let heightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!
    
    // Query to get the user's latest height, if it exists.
    aapl_mostRecentQuantitySampleOfType(heightType, predicate: nil) {mostRecentQuantity, error in
    if mostRecentQuantity == nil {
    NSLog("Either an error occured fetching the user's height information or none has been stored yet. In your app, try to handle this gracefully.")
    
    dispatch_async(dispatch_get_main_queue()) {
    //self.heightValueLabel.text = NSLocalizedString("Not available", comment: "")
    }
    } else {
    // Determine the height in the required unit.
    let heightUnit = HKUnit.meterUnit()
    self.usersHeight = mostRecentQuantity!.doubleValueForUnit(heightUnit)
    self.usersHeight = self.usersHeight/100.0
    
    self.updateUsersWeight()
    
    // Update the user interface.
    dispatch_async(dispatch_get_main_queue()) {
    // self.heightValueLabel.text = NSNumberFormatter.localizedStringFromNumber(usersHeight, numberStyle: NSNumberFormatterStyle.NoStyle)
    }
    }
    }
    }*/
    
    /* func readProfile()
    {
    var error:NSError?
    // 1. Request birthday and calculate age
    do {
    let birthDay = try healthStore.dateOfBirth()
    let today = NSDate()
    _ = NSCalendar.currentCalendar()
    let differenceComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDay, toDate: today, options: NSCalendarOptions(rawValue: 0) )
    age = differenceComponents.year
    print("Age",age)
    } catch let error1 as NSError {
    error = error1
    }
    if error != nil {
    print("Error reading Birthday: \(error)")
    }
    
    // 2. Read biological sex
    do {
    biologicalSex = try healthStore.biologicalSex()
    print("bio",biologicalSex)
    } catch let error1 as NSError {
    error = error1
    biologicalSex = nil
    };
    
    if error != nil {
    print("Error reading Biological Sex: \(error)")
    }
    
    age = 30
    usersHeight = 140.0
    weightInKg = 70.0
    
    usersHeight = 6.25 * usersHeight
    weightInKg = 9.99 * weightInKg
    
    let Age = 4.92 * Double(age!)
    restingCalories = weightInKg + usersHeight - Age + 5
    }*/
    
    
    func deleteHeartRateData(heartRateSampleType:HKSampleType,startDate:NSDate,endDate:NSDate)
    {
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
        
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        
        let sampleQuery = HKSampleQuery(sampleType: heartRateSampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                
                if let _ = error {
                    self.dismissController()
                    return;
                }
                
                for result in results! {
                    if let sample = result as? HKQuantitySample {
                        self.instanceDel.healthStore.deleteObject(sample, withCompletion: { (deleted, error) -> Void in
                        })
                    }
                }
        }
        instanceDel.healthStore.executeQuery(sampleQuery)
        self.dismissController()
    }
}


