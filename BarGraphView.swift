//
//  GraphView.swift
//  GraphDemo
//
//  Created by Rohit Garg on 05/09/15.
//  Copyright (c) 2015 Rohit Garg. All rights reserved.
//

import Foundation
import UIKit

class BarGraphView: UIView ,AddUpdateObserverDelegate, updateGraphEverySecDelegate,clearGraphDelegate
{
    var updateValuesInRealTime = false
    var timeInSecs : Int = 0
    
    var drawFirstTime = false
    //For Lines
    var kGraphHeight: Float = 250
    //var kDefaultGraphWidth: Float = 900
    var kOffsetX: Float = 0
    var kStepX: Float = 60
    var kOffsetY: Float = 60
    var kStepY: Float = 23
    
    var kGraphBottom: Float = 300
    var kGraphTop: Float = 0
    var kCircleRadiusC : CGFloat = 0.07
    var changeBottomLineIndex : Bool = false
    var bottomLineIndex :Int = 0
    var textIndex :Int = 0
    var Index :Int = 0
    
    
    var value1:Float?
    var value2:Float?
    var value3:Float?
    var value4:Float?
    var value5:Float?
    
    var currentBarValue:Double=0
    //For Bars
    var kBarStepX: Float = 30
    var kBarTop: Float = 10
    var kBarWidth: Float = 16.0
    
    //For LineGraph
    var kCircleRadius: Float = 3
    
    var touchAreasArrayForBar :[CGRect] = []
    let dataArray: [Float] = [0.7, 0.4, 0.9, 1.0, 0.0, 0.85, 0.11, 0.75, 0.53, 0.44, 0.88, 0.77]
    var barGraphArray :[Float] = []
    var graphArray : [Double] = []
    var time :[String] = []
    var scrollView: UIScrollView!
    var context:CGContextRef?
    
    override func drawRect(rect: CGRect)
    {
        context = UIGraphicsGetCurrentContext()!
        HealthManager.sharedInstance.updateObserverDelegate = self
        HealthManager.sharedInstance.updateGraphSecDelegate = self
        HealthManager.sharedInstance.clearBarGraphDelegate = self
        
        CGContextSetLineWidth(context!, 0.6);
        CGContextSetStrokeColorWithColor(context!, UIColor.darkGrayColor().CGColor);
        
        self.createHorizontalLines(context!);
        CGContextStrokePath(context!);
        CGContextSetLineDash(context!, 0.0, nil, 0); // to remove the doted from next draw
        
        self.createBar(context!);
        self.bottomline(context!)
    }
    
    func updateGraphEverySecond()
    {
        dispatch_async(dispatch_get_main_queue())
            {
                self.setNeedsDisplay()
                self.layer.displayIfNeeded()
                
                if self.updateValuesInRealTime
                {
                    HealthManager.sharedInstance.observerDelegateForRealTime?.updateObserver(self.graphArray)
                    self.updateValuesInRealTime = false
                }
        }
    }
    
    //MARK:- clear graph method
    
    func clearGraphOnStart()
    {
        dispatch_async(dispatch_get_main_queue())
            {
        self.graphArray.removeAll()
        self.time.removeAll()
        self.barGraphArray.removeAll()
        self.setNeedsDisplay()
        self.layer.displayIfNeeded()
        }
    }
    
    
    // ------- real time value delegate----------
    
    func updateObserverCall(heartRate:Double ,sampleTime:String)
    {
        HealthManager.sharedInstance.graphCount = barGraphArray.count
        var heartrate = (heartRate)/180
        if heartrate > 1
        {
            heartrate = 1
        }
        barGraphArray.append(Float(heartrate - 0.1))
        graphArray.append(Double(heartRate - 0.05))
        updateValuesInRealTime = true
        print(time)
        time.append(sampleTime)
        
        dispatch_async(dispatch_get_main_queue())
        {
                self.setNeedsDisplay()
                self.layer.displayIfNeeded()
                HealthManager.sharedInstance.observerDelegateForRealTime?.updateObserver(self.graphArray)
        }
}
    
    
    //------------For Grid----------------------
    func createVarticalLines(context : CGContextRef)
    {
        let value = HealthManager.sharedInstance.timerValue
        let howMany = Int((Float(self.frame.width) - kOffsetX) / kStepX) + value;
        for index in 0...howMany
        {
            let valueX: Float = (kOffsetX + Float(index) * kStepX) + Float(value)*0.05
            CGContextMoveToPoint(context, CGFloat(valueX), CGFloat(kGraphTop));
            CGContextAddLineToPoint(context, CGFloat(valueX), CGFloat(kGraphBottom));
        }
    }
    
    func createHorizontalLines(context : CGContextRef)
    {
        for index in 1...5{
            //let valueY: Float = (Float(index) * kStepY)
//            CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(valueY))
//            CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(valueY))
            CGContextSetLineWidth(context, 1)
            if index == 1
            {
                value1 = 33;
                //red
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value1!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value1!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 211/255.0, green: 0/255.0, blue: 69/255.0, alpha: 1.0).CGColor)
            }
            else  if index == 2
            {
                 value2 = 57;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value2!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value2!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 248/255.0, green: 167/255.0, blue: 66/255.0, alpha: 1.0).CGColor)
            }
            else if index == 3
            {
                 value3 = 76;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value3!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value3!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 246/255.0, green: 229/255.0, blue: 25/255.0, alpha: 1.0).CGColor)
            }
            else if index == 4
            {
                 value4 = 94;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value4!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value4!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 47/255.0, green: 181/255.0, blue: 219/255.0, alpha: 1.0).CGColor)
            }
            else if index == 5
            {
                 value5 = 111;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value5!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value5!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 28/255.0, green: 105/255.0, blue: 151/255.0, alpha: 1.0).CGColor)
            }
            CGContextStrokePath(context)
        }
        
    }
    
    
    //------------For Bar-----------------------
    
    func setscrollView(scrolView: UIScrollView){
        scrollView = scrolView;
    }
    
    func drawBar(context:CGContextRef,rect:CGRect)-> Void
    {
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMinY(rect));
        CGContextAddLineToPoint(context, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
        CGContextAddLineToPoint(context, CGRectGetMinX(rect), CGRectGetMaxY(rect));
        CGContextClosePath(context);
        CGContextFillPath(context);
    }
    
    func createBar(context:CGContextRef)
    {
        if barGraphArray.count > 0
        {
            self.clearsContextBeforeDrawing = true;
            
            let maxBarHeight: Float = Float(kGraphHeight - kBarTop - kOffsetY);
           
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd/MM/YYYY HH:mm:ss a"

            for var index = barGraphArray.count-1; index>=0;index -= 1
            {
                if(index == barGraphArray.count-1)
                {
                    let str1 :String = time[index]
                    let str2 = formatter.stringFromDate(NSDate())
                    let date1 = formatter.dateFromString(str1)
                    let date2 = formatter.dateFromString(str2)
                
                    if(HealthManager.sharedInstance.graphCount == barGraphArray.count)
                    {
                        timeInSecs = HealthManager.sharedInstance.barXValue!
                    }
                    else
                    {
                        timeInSecs = Helper.sharedInstance.arrivalTimeOfHeartRate(date1!, date2: date2!)
                    }
                    HealthManager.sharedInstance.barXValue = timeInSecs
                }
                else
                {
                    timeInSecs = timeInSecs + 4
                }
              
                let width = Float(self.frame.width)
                
                let barX : Float = width - (kOffsetX + (kBarStepX * Float(timeInSecs))/5)
    
                let barY:Float =  (maxBarHeight - maxBarHeight * Float(barGraphArray[index]))+6;
                
                //let barHeight =  Float(graphArray[index])-3;
                let barHeight =  Float(graphArray[index]);
                
                let barRect:CGRect = CGRectMake(CGFloat(barX), CGFloat(barY), CGFloat(kBarWidth), CGFloat(barHeight));
                
                touchAreasArrayForBar.append(barRect);
                currentBarValue =  Double(graphArray[index])
                self.barcolorwithvalue(context, a: currentBarValue)
                
                drawBar(context, rect: barRect)
            }
        }
    }
    
    
    //------------For Line----------------------
    
    func bottomline(context:CGContextRef)
    {
        
        CGContextSetLineWidth(context, 20)
        let ypoint = self.frame.size.height
        CGContextMoveToPoint(context, 0, ypoint)
        
        CGContextMoveToPoint(context, CGFloat(0), CGFloat(ypoint))
        CGContextAddLineToPoint(context, CGFloat(self.frame.size.width), CGFloat(ypoint))
        
        CGContextSetLineWidth(context, 1)
        
        CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
        
        
        CGContextStrokePath(context)
    }
    
    func setViewDimensions(){
        scrollView.contentSize = CGSizeMake(CGFloat(kOffsetX + Float(dataArray.count) * kStepX), CGFloat(0));
        if(CGFloat(kOffsetX + Float(dataArray.count) * kStepX) > UIScreen.mainScreen().bounds.width-50){
            self.frame = CGRectMake(0, 0, CGFloat(kOffsetX + Float(dataArray.count) * kStepX), self.frame.height)
        }
    }
    
    
    //--------For color of bar
    
    func barcolorwithvalue(context:CGContextRef, a:Double)
    {
        if a<91
        {
            CGContextSetRGBFillColor(context,155/255.0,155/255.0,155/255.0,1)
        }
        else if a>=91 && a<=107
        {
            CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if a>=108 && a<=125
        {
            CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if a>=126 && a<=144
        {
            CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if a>=145 && a<=168
        {
            CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if a>=169
        {
            CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
    }
}
