//
//  ProfileInfoViewController.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 11/18/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit
import HealthKit

class ProfileInfoViewController: UIViewController, updateProfileData {

    private var tabBarViewController: UITabBarController?
     var window: UIWindow?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        HealthManager.sharedInstance.updateProfileDelegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(animated: Bool)
    {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before n   avigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func updateAboutNewprofile()
    {
        if HealthManager.sharedInstance.biologicalSex != nil && HealthManager.sharedInstance.usersAge != nil && HealthManager.sharedInstance.usersHeight != nil && HealthManager.sharedInstance.usersWeight != nil
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ProfileInfoSaved")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            self.window = UIApplication.sharedApplication().windows[0]
            self.tabBarViewController = self.storyboard!.instantiateViewControllerWithIdentifier("TabBarViewController") as? UITabBarController
            self.window?.rootViewController = self.tabBarViewController
            
        }
    }
}
