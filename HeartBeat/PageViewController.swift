//
//  PageViewController.swift
//  HeartBeat
//
//  Created by Deep-MacMini on 8/23/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit
import HealthKit

class PageViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIAlertViewDelegate{
    
    @IBOutlet var pageControl : UIPageControl?

    
  
    var pageIndex :Int = 0
   

    private var pageViewController: UIPageViewController?
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - View Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.authorizeHealthKit()
        //indicatorView?.hidden = true
        createPageViewController()
       
    }
    
    private func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewControllerWithIdentifier("PageController") as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        
        let realTimeVC = self.storyboard!.instantiateViewControllerWithIdentifier("RealTimeVC")
    //    let historyNavVC = self.storyboard!.instantiateViewControllerWithIdentifier("HistoryNav")
        
      //  let firstController = getContentController(0)!
        let startingViewControllers: NSArray = [realTimeVC]
        pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Reverse, animated: true, completion: nil)
       
                
        pageViewController = pageController
        addChildViewController(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
        pageControl?.currentPage = 0
        self.view.bringSubviewToFront(pageControl!)
       
    }
    
    
    func authorizeHealthKit(){
        HealthManager.sharedInstance.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {}
        }
    }
    
    // MARK: - btn click
    
       // MARK: - UIPageViewControllerDataSource
    /*
     @IBAction func closeBtnClick(sender : UIButton)
     {
     indicatorView?.hidden = false
     indicatorView?.startAnimating()
     self.view.bringSubviewToFront(indicatorView!)
     
     HealthManager.sharedInstance.authorizeHealthKit { (authorized,  error) -> Void in
     if authorized {
     
     let userDefaults = NSUserDefaults.standardUserDefaults()
     userDefaults.setBool(true, forKey: "HasHealthKitPermission")
     userDefaults.synchronize()
     
     self.indicatorView?.hidden = true
     self.indicatorView?.stopAnimating()
     
     print("HealthKit authorization received.")
     //                HealthManager.sharedInstance.healthKitStore.enableBackgroundDeliveryForType(HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!, frequency:HKUpdateFrequency.Immediate, withCompletion: { (suceeded, error) -> Void in
     //
     //                    if suceeded
     //                    {
     //                        print("enable background property")
     var age :Int = 0
     
     HealthManager.sharedInstance.readProfile()
     self.window = UIApplication.sharedApplication().windows[0]
     
     if HealthManager.sharedInstance.usersAge != nil && HealthManager.sharedInstance.usersHeight != nil && HealthManager.sharedInstance.usersWeight != nil && HealthManager.sharedInstance.biologicalSex != nil
     {
     
     NSUserDefaults.standardUserDefaults().setBool(true, forKey: "ProfileInfoSaved")
     
     NSUserDefaults.standardUserDefaults().setObject(age, forKey: "Age")
     NSUserDefaults.standardUserDefaults().synchronize()
     
     self.tabBarViewController = self.storyboard!.instantiateViewControllerWithIdentifier("TabBarViewController") as? UITabBarController
     
     self.window?.rootViewController = self.tabBarViewController
     age = 24
     }
     else
     {
     self.profileController = self.storyboard!.instantiateViewControllerWithIdentifier("ProfileInfoViewController")
     self.window?.rootViewController = self.profileController
     age = 0
     }
     
     self.window?.makeKeyAndVisible()
     }
     else
     {
     print("HealthKit authorization denied!")
     
     
     
     //                let alertView = UIAlertView(title: "", message: "HealthKit authorization denied!", delegate: self, cancelButtonTitle: "Close")
     //                alertView.show()
     
     let alert = UIAlertController(title: "", message: "HealthKit authorization denied!", preferredStyle: UIAlertControllerStyle.Alert)
     alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:nil))
     
     dispatch_async(dispatch_get_main_queue())
     {
     self.indicatorView?.hidden = true
     self.indicatorView?.stopAnimating()
     self.presentViewController(alert, animated: true, completion: nil)
     
     }
     if error != nil {
     print("\(error)")
     }
     }
     }
     }
     
     @IBAction func nextBtnClick(sender : UIButton)
     {
     if pageIndex != contentImages.count
     {
     pageIndex += 1
     }
     print(pageIndex)
     pageControl?.currentPage = pageIndex
     if pageIndex < contentImages.count{
     let firstController = getContentController(pageIndex)!
     let startingViewControllers: NSArray = [firstController]
     pageViewController!.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewControllerNavigationDirection.Forward, animated: true, completion: nil)
     }
     }
     
     */

   
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        if viewController.isKindOfClass(UINavigationController) {
              return getContentController(0)
        } else {
            return nil;
        }
      
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?
    {
        if viewController.isKindOfClass(RealTimeVC) {
            return getContentController(2)
        } else {
            return nil;
        }    }
    /*
    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController])
    {
    
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool)
    {
    
    }
    */
    //MARK: - load page content controller
    
    private func getContentController(itemIndex: Int) -> UIViewController? {
      
            if itemIndex == 0 {
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("RealTimeVC")
                
                return pageItemController
            }
            else  {
                let pageItemController = self.storyboard!.instantiateViewControllerWithIdentifier("HistoryNav")
                return pageItemController
            }
    }
    
    
  
    

}
