//
//  AppDelegate.swift
//  HeartBeat
//
//  Created by Deep-MacMini on 8/18/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.


//com.visualdirective.heartwatch
//rdefore@mac.com

import UIKit
import HealthKit


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        
        return true
     /*   let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController : UIViewController?
        
        rootController = storyboard.instantiateViewControllerWithIdentifier("PageViewController")
        UIApplication.sharedApplication().statusBarHidden = true
        UIApplication.sharedApplication().idleTimerDisabled = true
        self.window!.rootViewController = rootController
        self.window?.makeKeyAndVisible()
        return true*/
        
        
        
        //let quantityType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!
       
      /*   let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController : UIViewController?
       let value = NSUserDefaults.standardUserDefaults().boolForKey("HasHealthKitPermission")
        if value == true
        {
            let infoSaveValue = NSUserDefaults.standardUserDefaults().boolForKey("ProfileInfoSaved")
            
            if infoSaveValue == true
            { 
                rootController = storyboard.instantiateViewControllerWithIdentifier("TabBarViewController")
            }
            else
            {
                rootController = storyboard.instantiateViewControllerWithIdentifier("ProfileInfoViewController")
            }

         
        }
        else
        {
            rootController = storyboard.instantiateViewControllerWithIdentifier("PageViewController")
        }

        rootController = storyboard.instantiateViewControllerWithIdentifier("PageViewController")
        UIApplication.sharedApplication().statusBarHidden = true
        UIApplication.sharedApplication().idleTimerDisabled = true
        self.window!.rootViewController = rootController
        self.window?.makeKeyAndVisible()
        return true*/
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        
      /*  let controller = self.window?.rootViewController
        
        let restorationStr = controller?.restorationIdentifier
        if restorationStr == "ProfileInfoViewController"
        {
            HealthManager.sharedInstance.readProfile()
        }*/
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, handleWatchKitExtensionRequest userInfo: [NSObject : AnyObject]?, reply: ([NSObject : AnyObject]?) -> Void)
    {
        
    }


}

