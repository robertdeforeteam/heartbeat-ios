//
//  HistoricalGraphController.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 9/28/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import Foundation
import UIKit
import HealthKit

class HistoricalGraphController : UIViewController ,AddWorkoutUpdateDelegate
{
    let VERTICAL_LINE = 1
    let HORIZONTAL_LINE = 2
    var query :Bool?
    
    var quantityArray :[CGFloat] = []
    
    @IBOutlet var zoneDescLbl1 : UILabel?
    @IBOutlet var zoneDescLbl2 : UILabel?
    @IBOutlet var zoneDescLbl3 : UILabel?
    @IBOutlet var zoneDescLbl4 : UILabel?
    @IBOutlet var zoneDescLbl5 : UILabel?
    
    @IBOutlet var activityIndicator : UIActivityIndicatorView?
    
    @IBOutlet var zoneTimeLbl1 : UILabel?
    @IBOutlet var zoneTimeLbl2 : UILabel?
    @IBOutlet var zoneTimeLbl3 : UILabel?
    @IBOutlet var zoneTimeLbl4 : UILabel?
    @IBOutlet var zoneTimeLbl5 : UILabel?
    
    @IBOutlet var zoneView : UIView?
    @IBOutlet var zone1View : UIView?
    @IBOutlet var zone2View : UIView?
    @IBOutlet var zone3View : UIView?
    @IBOutlet var zone4View : UIView?
    @IBOutlet var zone5View : UIView?
    
    
    @IBOutlet var zoneDescView : UIView?
    @IBOutlet var dateView : UIView?
    
    @IBOutlet var zone1Desc_View : UIView?
    @IBOutlet var zone2Desc_View : UIView?
    @IBOutlet var zone3Desc_View : UIView?
    @IBOutlet var zone4Desc_View : UIView?
    @IBOutlet var zone5Desc_View : UIView?
    
    
    @IBOutlet var dateLbl : UILabel?
    @IBOutlet var timeLbl : UILabel?
    @IBOutlet var max_avgLbl : UILabel?
    
    @IBOutlet var scrollView : UIScrollView?
 //   @IBOutlet weak var graphView: GraphView!
    
    var sampleStartDateArray : [NSDate] = []
    var sampleStopDateArray : [NSDate] = []
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        zoneDescView?.hidden = true;
        dateView?.hidden = true;
        
//      self.createTestData()
//      self.plotGraphsStatsTest()
        HealthManager.sharedInstance.historyStructure.removeAll()
        
        //graphView.hidden = true;
        //scrollView!.hidden = true
        
        self.activityIndicator!.hidden = false;
        self.view.bringSubviewToFront(self.activityIndicator!)
        self.activityIndicator?.startAnimating()
        self.zoneDescView?.hidden = true;
        self.dateView?.hidden = true;

        HealthManager.sharedInstance.workoutUpdateDelegate = self
        let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        HealthManager.sharedInstance.readMostRecentSample(heartSampleType!, startDate: (HealthManager.sharedInstance.selectedWorkOutData?.startDate)!, endDate: (HealthManager.sharedInstance.selectedWorkOutData?.endDate)!)
        {
            (result, error) -> Void in
        }
    }
    
    
    func findPerZoneValue(valueArray:NSArray)
    {
        let total  = Int(valueArray.count)
        
        if total != 0
        {
            var z1 = 0
            var z2 = 0
            var z3 = 0
            var z4 = 0
            var z5 = 0
            
            var secsZ1 :Float = 0.0
            var secsZ2 :Float = 0.0
            var secsZ3 :Float = 0.0
            var secsZ4 :Float = 0.0
            var secsZ5 :Float = 0.0
            
            for i in 0 ..< valueArray.count
            {
                let value :Float = Float(valueArray[i] as! NSNumber)
                if value >= 91.0 && value <= 107.0
                {
                    z1 += 1
                    secsZ1 = (Float(HealthManager.sharedInstance.totalWorkoutTimeInterval))*(Float(z1)/Float(total))
                }
                else  if value >= 108.0 && value <= 125.0
                {
                    z2 += 1
                    secsZ2 = (Float(HealthManager.sharedInstance.totalWorkoutTimeInterval))*(Float(z2)/Float(total))
                }
                else  if value >= 126.0 && value <= 144.0
                {
                    z3 += 1
                    secsZ3 = (Float(HealthManager.sharedInstance.totalWorkoutTimeInterval))*(Float(z3)/Float(total))
                }
                else  if value >= 145.0 && value <= 168.0
                {
                    z4 += 1
                    secsZ4 = (Float(HealthManager.sharedInstance.totalWorkoutTimeInterval))*(Float(z4)/Float(total))
                }
                else  if value >= 169.0
                {
                    z5 += 1
                    secsZ5 = (Float(HealthManager.sharedInstance.totalWorkoutTimeInterval))*(Float(z5)/Float(total))
                }
            }
            
            if secsZ1 != 0
            {
                zoneTimeLbl1?.text = Helper.sharedInstance.zoneTimeValue(Int(secsZ1))
            }
            if secsZ2 != 0
            {
                zoneTimeLbl2?.text = Helper.sharedInstance.zoneTimeValue(Int(secsZ2))
            }
            if secsZ3 != 0
            {
                zoneTimeLbl3?.text = Helper.sharedInstance.zoneTimeValue(Int(secsZ3))
            }
            if secsZ4 != 0
            {
                zoneTimeLbl4?.text = Helper.sharedInstance.zoneTimeValue(Int(secsZ4))
            }
            if secsZ5 != 0
            {
                zoneTimeLbl5?.text = Helper.sharedInstance.zoneTimeValue(Int(secsZ5))
            }
            
            let Z1 = Float(z1)
            let Z2 = Float(z2)
            let Z3 = Float(z3)
            let Z4 = Float(z4)
            let Z5 = Float(z5)
            
            zoneDescLbl1!.text = NSString(format: "%0.1f%@", ((Z1/Float(total))*100),"%")  as String
            zoneDescLbl2!.text = NSString(format: "%0.1f%@", ((Z2/Float(total))*100),"%")  as String
            zoneDescLbl3!.text = NSString(format: "%0.1f%@", ((Z3/Float(total))*100),"%")  as String
            zoneDescLbl4!.text = NSString(format: "%0.1f%@", ((Z4/Float(total))*100),"%")  as String
            zoneDescLbl5!.text = NSString(format: "%0.1f%@", ((Z5/Float(total))*100),"%")  as String
            
            calulateZoneHeartPercentage(Z1/Float(total)*100, zone2Value: Z2/Float(total)*100, zone3Value:Z3/Float(total)*100, zone4Value: Z4/Float(total)*100, zone5Value: Z5/Float(total)*100)
        }
        else
        {
            zoneDescLbl1!.text = NSString(format: "%d%@", 0,"%")  as String
            zoneDescLbl2!.text = NSString(format: "%d%@", 0,"%")  as String
            zoneDescLbl3!.text = NSString(format: "%d%@", 0,"%")  as String
            zoneDescLbl4!.text = NSString(format: "%d%@", 0,"%")  as String
            zoneDescLbl5!.text = NSString(format: "%d%@", 0,"%")  as String
            
            calulateZoneHeartPercentage(0, zone2Value: 0, zone3Value: 0, zone4Value: 0, zone5Value: 0)
        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
        
      
   
        /*
        
        HealthManager.sharedInstance.workoutUpdateDelegate = self
    
            let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
            
            HealthManager.sharedInstance.readMostRecentSample(heartSampleType!, startDate: (HealthManager.sharedInstance.selectedWorkOutData?.startDate)!, endDate: (HealthManager.sharedInstance.selectedWorkOutData?.endDate)!)
                {
                    (result, error) -> Void in
                }
 
 */
        
          }
    
    func createTestData(){
        for _ in 1...300 {
            let randomValue = CGFloat(Int(arc4random_uniform(160) + 60))
            self.quantityArray.append(randomValue)
        }
    }
    
    @IBAction func backClick(sender: UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    func calculateFrameOrigin(var width:Float) ->Float
    {
        if width > 40 && width < 50
        {
            width = width - 5
        }
        else if width >= 50 && width < 80
        {
            width = width - 20
        }
        else if width >= 80
        {
            width = width - 30
        }
        return width
    }
    
    func calulateZoneHeartPercentage(zone1Value : Float,zone2Value:Float,zone3Value:Float,zone4Value:Float,zone5Value:Float)
    {
        var zone1Width : Float = 0
        var zone2Width : Float = 0
        var zone3Width : Float = 0
        var zone4Width : Float = 0
        var zone5Width : Float = 0
        
        let zoneWidth :Float = Float((zoneView?.frame.width)!)
        
        zone1Width = (zoneWidth*zone1Value)/100
        zone2Width = (zoneWidth*zone2Value)/100
        zone3Width = (zoneWidth*zone3Value)/100
        zone4Width = (zoneWidth*zone4Value)/100
        zone5Width = (zoneWidth*zone5Value)/100
        
        zone1View?.frame  = CGRectMake((zone1View?.frame.origin.x)!, (zone1View?.frame.origin.y)!, CGFloat(zone1Width), (zone1View?.frame.size.height)!)
        zone2View?.frame  = CGRectMake((zone2View?.frame.origin.x)!, (zone2View?.frame.origin.y)!, CGFloat(zone2Width), (zone2View?.frame.size.height)!)
        zone3View?.frame  = CGRectMake((zone3View?.frame.origin.x)!, (zone3View?.frame.origin.y)!, CGFloat(zone3Width), (zone3View?.frame.size.height)!)
        zone4View?.frame  = CGRectMake((zone4View?.frame.origin.x)!, (zone4View?.frame.origin.y)!, CGFloat(zone4Width), (zone4View?.frame.size.height)!)
        zone5View?.frame  = CGRectMake((zone5View?.frame.origin.x)!, (zone5View?.frame.origin.y)!, CGFloat(zone5Width), (zone5View?.frame.size.height)!)
        
        zone1View?.layer.cornerRadius = 3.0
        zone2View?.layer.cornerRadius = 3.0
        zone3View?.layer.cornerRadius = 3.0
        zone4View?.layer.cornerRadius = 3.0
        zone5View?.layer.cornerRadius = 3.0
        
        zone1Desc_View?.frame = CGRectMake(CGFloat(zone1Width+10), (zone1Desc_View?.frame.origin.y)!, 100, (zone1Desc_View?.frame.size.height)!)
        
        zone2Desc_View?.frame = CGRectMake(CGFloat(zone2Width+10), (zone2Desc_View?.frame.origin.y)!, 100, (zone2Desc_View?.frame.size.height)!)
        
        zone3Desc_View?.frame = CGRectMake(CGFloat(zone3Width+10), (zone3Desc_View?.frame.origin.y)!, 100, (zone3Desc_View?.frame.size.height)!)
        
        zone4Desc_View?.frame = CGRectMake(CGFloat(zone4Width+10), (zone4Desc_View?.frame.origin.y)!, 100, (zone4Desc_View?.frame.size.height)!)
        
        zone5Desc_View?.frame = CGRectMake(CGFloat(zone5Width+10), (zone5Desc_View?.frame.origin.y)!, 100, (zone5Desc_View?.frame.size.height)!)
        
    }
    
    
    //depends upon quantityArray
    func plotBarGrap(){
        
        let widthOfBar = CGFloat(10.0)  //widthOfBar
        let deltaX = CGFloat(5.0)   //spacingBetweenBArs
        var offsetX = CGFloat(10.0)
        
  
        
        for var heartReadingValue in self.quantityArray {
            
        
            if heartReadingValue > (self.scrollView?.frame.size.height)! {
                heartReadingValue = (self.scrollView?.frame.size.height)!
            }
            
            let heightOfBar:CGFloat =  heartReadingValue
            let view = UIView(frame: CGRectMake(offsetX ,(self.scrollView?.frame.size.height)! - heightOfBar, widthOfBar, heightOfBar))
            offsetX += (widthOfBar + deltaX)
            //  view.center.x =
            view.backgroundColor = Helper.sharedInstance.getBarColor(heartReadingValue)
            view.tag = VERTICAL_LINE
            self.scrollView?.addSubview(view)
        }
    }
    
    func createHorizontalLines(){
        let helper:Helper = Helper.sharedInstance
        let graphViewHeight = Int((self.scrollView?.frame.size.height)!)
        
     //   let deltaY =  17
     /*   let greyLineY = graphViewHeight - 90 ;
        let darkBlueLineY = greyLineY - deltaY ;
        let lightBlueLineY = greyLineY - 2 * deltaY ;
        let yelloLineY = greyLineY - 3 * deltaY ;
        let orangeLineY = greyLineY - 4 * deltaY ;
        let redLineY = greyLineY - 5 * deltaY;*/
        
       // let greyLineY = graphViewHeight - 90 ;
        
        let darkBlueLineY = graphViewHeight - 90 ;
        let lightBlueLineY = graphViewHeight - 107 ;
        let yelloLineY = graphViewHeight - 125 ;
        let orangeLineY = graphViewHeight - 144;
        let redLineY = graphViewHeight - 168;
        
        
        var widthOfHorizontalLine = self.scrollView?.contentSize.width
        if widthOfHorizontalLine < view.frame.size.width {
            widthOfHorizontalLine = view.frame.size.width
        }
        
       // let greyLineView = UIView(frame: CGRectMake(0,CGFloat(greyLineY),(self.scrollView?.contentSize.width)!, 1))
        let darkBlueLineView = UIView(frame: CGRectMake(0,CGFloat(darkBlueLineY),(widthOfHorizontalLine)!, 1))
        let lightBlueLineView = UIView(frame: CGRectMake(0,CGFloat(lightBlueLineY),(widthOfHorizontalLine)!, 1))
        let yellowLineView = UIView(frame: CGRectMake(0,CGFloat(yelloLineY),(widthOfHorizontalLine)!, 1))
        let orangeLineView = UIView(frame: CGRectMake(0,CGFloat(orangeLineY),(widthOfHorizontalLine)!, 1))
        let redLineView = UIView(frame: CGRectMake(0,CGFloat(redLineY),(widthOfHorizontalLine)!, 1))
        let baseLineView = UIView(frame: CGRectMake(0,CGFloat(graphViewHeight),(widthOfHorizontalLine)!, 1))
        
        
        
      //  greyLineView.backgroundColor = helper.greyColor
        darkBlueLineView.backgroundColor = helper.darkBlueColor
        lightBlueLineView.backgroundColor = helper.lightBlueColor
        yellowLineView.backgroundColor = helper.yellowColor
        orangeLineView.backgroundColor = helper.orangeColor
        redLineView.backgroundColor = helper.redColor
        baseLineView.backgroundColor = UIColor.whiteColor()
        
     //   greyLineView.tag = HORIZONTAL_LINE
        darkBlueLineView.tag = HORIZONTAL_LINE
        lightBlueLineView.tag = HORIZONTAL_LINE
        yellowLineView.tag = HORIZONTAL_LINE
        orangeLineView.tag = HORIZONTAL_LINE
        redLineView.tag = HORIZONTAL_LINE
        baseLineView.tag = HORIZONTAL_LINE
        
       // self.scrollView?.addSubview(greyLineView)
        self.scrollView?.addSubview(darkBlueLineView)
        self.scrollView?.addSubview(lightBlueLineView)
        self.scrollView?.addSubview(yellowLineView)
        self.scrollView?.addSubview(orangeLineView)
        self.scrollView?.addSubview(redLineView)
        self.scrollView?.addSubview(baseLineView)
    }
    
    func createZonePercentageGraph(){
   
            
            let time = HealthManager.sharedInstance.totalWorkoutTimeInterval
            
       /*     var width = (Int(self.graphView.frame.size.width)/60)*time
            if width > 8000
            {
                width = width / 60
                
                HealthManager.sharedInstance.historyWorkoutTimeType  = 2
                if width > 8000
                {
                    width = width/60
                    HealthManager.sharedInstance.historyWorkoutTimeType  = 3
                    
                    if width > 8000
                    {
                        width = 8000
                    }
                }
            }
            else
            {
                HealthManager.sharedInstance.historyWorkoutTimeType  = 1
            }
            
            if HealthManager.sharedInstance.historyWorkoutTimeType  == 1
            {
                self.graphView.frame = CGRectMake(self.graphView.frame.origin.x, self.graphView.frame.origin.y,
                                                  CGFloat(width+300), self.graphView.frame.size.height)
                
            }
            else
            {
                if width <= Int(self.graphView.frame.size.width)
                {
                    width = Int(self.graphView.frame.size.width)
                    self.graphView.frame = CGRectMake(self.graphView.frame.origin.x, self.graphView.frame.origin.y,
                                                      CGFloat(width+800), self.graphView.frame.size.height)
                }
                else
                {
                    self.graphView.frame = CGRectMake(self.graphView.frame.origin.x, self.graphView.frame.origin.y,
                                                      CGFloat(width+300), self.graphView.frame.size.height)
                }
            }
            
            */
        
            
            //self.graphView.setNeedsDisplay()
           // self.graphView.layer.displayIfNeeded()
            
          //  HealthManager.sharedInstance.historyStructure.removeAll()
          //  HealthManager.sharedInstance.historyStructure = historyStructure
            
            
           /* var graphviewSize = self.graphView.frame.size
            graphviewSize.width = graphviewSize.width + 30
            self.graphView.frame.size = graphviewSize
            
            self.scrollView!.contentSize = CGSizeMake(self.scrollView!.frame.size.width, self.scrollView!.frame.size.height-80)
            var size = self.scrollView!.contentSize
            size.width = self.graphView.frame.size.width + 20
            self.scrollView!.contentSize = size*/
            
      
          //  self.quantityArray.removeAll()
            
         /*   if(self.query == false)
            {
                for sample in historyStructure
                {
                    let heartRateUnit = HKUnit(fromString: "count/min")
                    let heartRate : Double = sample.quantity.doubleValueForUnit(heartRateUnit)
                    
                    self.quantityArray.append(CGFloat(heartRate))
                }
            }
            else
            {
                for value in HealthManager.sharedInstance.heartRateArray
                {
                    self.quantityArray.append(CGFloat(value))
                }
            }
 
 */
            
            self.findPerZoneValue(self.quantityArray)
            

        
    }
    
    func showStats(){
        self.scrollView?.hidden = false
       // self.graphView.hidden = false;
        self.zoneDescView?.hidden = false;
        self.dateView?.hidden = false;
        
        
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let str :String = formatter.stringFromDate((HealthManager.sharedInstance.selectedWorkOutData?.startDate)!)
        self.dateLbl?.text = str
        
        let timeStr = Helper.sharedInstance.zoneTimeValue(HealthManager.sharedInstance.totalWorkoutTimeInterval)
        self.timeLbl?.text = timeStr as String
        
        self.max_avgLbl?.hidden = true
        
        self.activityIndicator!.hidden = true;
        self.activityIndicator!.stopAnimating()
        self.scrollView!.hidden = false
        self.zoneDescView!.hidden = false
        self.dateView!.hidden = false
        
        /*    if self.quantityArray.count > 1
         {
         self.max_avgLbl?.hidden = false
         let avg = Helper.sharedInstance.findAvg(self.quantityArray)
         let max = Helper.sharedInstance.findMax(self.quantityArray)
         
         if HealthManager.sharedInstance.TotalCalories == nil
         {
         self.max_avgLbl?.text = NSString(format:"%@ %@  %@ %@","AVG",String(avg),"MAX",String(max)) as String
         }
         else
         {
         self.max_avgLbl?.text = NSString(format:"%@ %@  %@ %@ %@ %0.1f","AVG",String(avg),"MAX",String(max),"TOTAL CAL",HealthManager.sharedInstance.TotalCalories!) as String
         }
         }
         
         else if self.quantityArray.count == 1
         {
         self.max_avgLbl?.hidden = false
         let value = (self.quantityArray.last)!
         
         if HealthManager.sharedInstance.TotalCalories != nil
         {
         self.max_avgLbl?.text = NSString(format:"%@ %@  %@ %@","AVG",String(value),"MAX",String(value)) as String
         }
         else
         {
         self.max_avgLbl?.text = NSString(format:"%@ %@  %@ %@ %@ %0.1f","AVG",String(value),"MAX",String(value),"TOTAL CAL",HealthManager.sharedInstance.TotalCalories!) as String
         }
         
         }*/
    }
    // delegate
    func workoutDetailUpdate(historyStructure:[HKQuantitySample])
    {
        for sample in historyStructure
        {
            let heartRateUnit = HKUnit(fromString: "count/min")
            let heartRate : Double = sample.quantity.doubleValueForUnit(heartRateUnit)
            
            self.quantityArray.append(CGFloat(heartRate))
            
        }

        let totalEntries = self.quantityArray.count
        let contentSizeWidth = CGFloat(15 * totalEntries)
        self.scrollView?.contentSize = CGSizeMake(contentSizeWidth, (self.scrollView?.frame.size.height)!)
        
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.createHorizontalLines()
            self.plotBarGrap()
            self.showStats()
            self.createZonePercentageGraph()
        })
     
    }
    
    func plotGraphsStatsTest(){
        
        let totalEntries = self.quantityArray.count
        var contentSizeWidth = CGFloat(15 * totalEntries)
       
        if contentSizeWidth < self.view.frame.width {
            contentSizeWidth = self.view.frame.width
        }
        
        self.scrollView?.contentSize = CGSizeMake(contentSizeWidth, (self.scrollView?.frame.size.height)!)
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.createHorizontalLines()
            self.plotBarGrap()
            self.showStats()
            self.createZonePercentageGraph()
        })
    }
    
 
}
