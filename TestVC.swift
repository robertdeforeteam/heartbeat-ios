//
//  TestVC.swift
//  HeartBeat
//
//  Created by Priya on 02/10/16.
//  Copyright © 2016 Deep-MacMini. All rights reserved.
//

import Foundation
import UIKit
import HealthKit
import CoreGraphics
import WatchConnectivity

class TestVC: UIViewController , WCSessionDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported())
        {
           let session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func session(session: WCSession, activationDidCompleteWithState activationState: WCSessionActivationState, error: NSError?){
        
    }
    
    /** ------------------------- iOS App State For Watch ------------------------ */
    
    /** Called when the session can no longer be used to modify or add any new transfers and, all interactive messages will be cancelled, but delegate callbacks for background transfers can still occur. This will happen when the selected watch is being changed. */
    func sessionDidBecomeInactive(session: WCSession){
        
    }
    
    /** Called when all delegate callbacks for the previously selected watch has occurred. The session can be re-activated for the now selected watch using activateSession. */
    func sessionDidDeactivate(session: WCSession){
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
