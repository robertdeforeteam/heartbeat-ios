//
//  WatchGraph.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 10/14/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import WatchKit
import CoreGraphics

class WatchGraph: WKInterfaceImage
{
    var kGraphHeight: Float = 200
    //var kDefaultGraphWidth: Float = 900
    var kOffsetX: Float = 0
    var kStepX: Float = 10
    var kOffsetY: Float = 10
    var kStepY: Float = 5
    
    var kGraphBottom: Float = 150
    var kGraphTop: Float = 0
    var kCircleRadiusC : CGFloat = 0.07
    var changeBottomLineIndex : Bool = false
    var bottomLineIndex :Int = 0
    var textIndex :Int = 0
    var Index :Int = 0
    
    var currentBarValue:Double=0
    //For Bars
    var kBarStepX: Float = 10
    var kBarTop: Float = 5
    var kBarWidth: Float = 1
    
    //For LineGraph
    var kCircleRadius: Float = 1
    
    var touchAreasArrayForBar :[CGRect] = []
    let dataArray: [Float] = [0.7, 0.4, 0.9, 1.0, 0.0, 0.85, 0.11, 0.75, 0.53, 0.44, 0.88, 0.77]
    var barGraphArray :[Float] = []
    var graphArray : [Double] = []
    var time :[String] = []
    
    
    //------------For Grid----------------------
    func createVarticalLines(context : CGContextRef)
    {
        let howMany = Int((Float(120) - kOffsetX) / kStepX)
        for index in 0...howMany
        {
            let valueX: Float = (kOffsetX + Float(index) * kStepX);
            CGContextMoveToPoint(context, CGFloat(valueX), CGFloat(kGraphTop));
            CGContextAddLineToPoint(context, CGFloat(valueX), CGFloat(kGraphBottom));
        }
    }
    
    func createHorizontalLines(context : CGContextRef)
    {
        let howMany = Int((kGraphBottom - kGraphTop - kOffsetY) / kStepY)
        for index in 1...howMany{
            let valueY: Float = (kGraphBottom - kOffsetY - Float(index) * kStepY)
            CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(valueY))
            CGContextAddLineToPoint(context, CGFloat(100), CGFloat(valueY))
        }
    }
}
