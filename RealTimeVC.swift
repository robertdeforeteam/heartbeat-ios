//
//  realTimeVC.swift
//  HeartBeat
//
//  Created by Deep-MacMini on 8/19/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//


import Foundation
import UIKit
import HealthKit
import CoreGraphics
import WatchConnectivity




//class RealTimeVC : UIViewController , WCSessionDelegate, UpdateObserverDelegate
class RealTimeVC : UIViewController , WCSessionDelegate
{
    

    let HORIZONTAL_LINE = 1
    let VERTICAL_LINE = 2
    
    
    var startTimer = false
    var heartInfo = 0.10
    
    var sec = 0
    var min = 0
    var hrs = 0
    
    @IBOutlet var  barGraphView :UIView?
    @IBOutlet var heartimageView :UIImageView?
    @IBOutlet var testLabel : UILabel?
    @IBOutlet var timerLabel : UILabel?
    
    @IBOutlet var intensityLabel : UILabel?
    @IBOutlet var intensityTextLabel : UILabel?
    
    @IBOutlet var currentHeartRateView : UIView?
    @IBOutlet var maxHeartRateView : UIView?
    
    @IBOutlet var zoneLabel : UILabel?
    @IBOutlet var maxHeartRateLbl : UILabel?
    @IBOutlet var avgHeartRateLbl : UILabel?
    @IBOutlet var currentHeartRateLbl : UILabel?
    
    @IBOutlet var readingLbl : UILabel?
    
    @IBOutlet var scrollView : UIScrollView?
    @IBOutlet weak var graphView: BarGraphView!
    
    
    var session : WCSession!
    var heartRate = 50.0
    var timer = NSTimer()
    
    var timer1 = NSTimer()
    
    
    
    //Graph Variables
    
    var deltaXBars:CGFloat = 5.0
    var widthOfBar:CGFloat = 10.0
    var heartReadingValue:CGFloat = 0.0
    var heartReadingsArray :[CGFloat] = []
    var isWorkoutPaused = false
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //HealthManager.sharedInstance.observerDelegateForRealTime = self
       // graphView.setscrollView(scrollView!);
        zoneLabel?.hidden = true
        intensityTextLabel?.hidden = true
        intensityLabel?.hidden = true
        readingLbl?.hidden = false
        maxHeartRateView?.hidden = true
        currentHeartRateView?.hidden = true
        self.createHorizontalLines()
        
      //testing of graph
       
      //  self.startWorkoutTimer();
      //  self.generateAndPlotRandomReadingAfterRandomTime()
       
      //self.testPause()
    }
    
    func pauseResumeTest(){
      isWorkoutPaused = !isWorkoutPaused
    }
    func testPause(){
        NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector:
            #selector(RealTimeVC.pauseResumeTest), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(animated: Bool)
    {
        if (WCSession.isSupported())
        {
            session = WCSession.defaultSession()
            session.delegate = self
            session.activateSession()
            
        }
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    func startWorkoutTimer(){
        timer1 = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(RealTimeVC.update), userInfo: nil, repeats: true)
        
    }
    
    //WCSessionProtocol
  
    
    func session(session: WCSession, activationDidCompleteWithState activationState: WCSessionActivationState, error: NSError?){
        
    }
    
    func sessionDidBecomeInactive(session: WCSession){
        print("Print Something")
    }
    
    func sessionDidDeactivate(session: WCSession){
        print("Print Something")
    }
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void)
    {
        print("value passed to iphone app")
        let counterValue = message["Value"] as! String
        let startValue = message["Start"] as! Bool
        let isPaused = message["Paused"] as! Bool
        let loadWorkout = message["loadWorkout"] as! Bool
        let workOutStartDate = message["workOutStartDate"] as! NSDate
       //  let workOutEndDate = message["workOutEndDate"] as! NSDate
       //  let v =  message["ActivityType"] as! Int
       //  HKWorkoutType(raw)
        
       //  ContainAllREadings
       //         let heartArr = message["HeartRateInfo"] as! [Double]
        
        
        if startValue == true
        {
            
          //  let heartSampleDate = compArr.last
            
            //Use this to update the UI instantaneously (otherwise, takes a little while)
            dispatch_async(dispatch_get_main_queue())
                {
                    if self.startTimer == false
                    {
                        let workSampleDate = message["WorkoutSampleDate"] as! NSDate
                        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]
                        let differenceComponents = NSCalendar.currentCalendar().components(unitFlags, fromDate: workSampleDate, toDate: workOutStartDate, options: NSCalendarOptions(rawValue: 0) )
                        if(self.hrs == 0 && self.min == 0 && self.sec == 0)
                        {
                            self.hrs = differenceComponents.hour
                            self.min = differenceComponents.minute
                            self.sec = differenceComponents.second+1
                        }
                        else
                        {
                            self.sec = self.sec + differenceComponents.second+1
                            self.min = self.min + differenceComponents.minute
                            self.hrs = self.hrs + differenceComponents.hour
                            
                        }
                        self.startWorkoutTimer()
                        self.startTimer = true
                    }
                    
                    if counterValue != "" {
                        let compArr : [String] = counterValue.componentsSeparatedByString("s")
                        let heartInfo = compArr.first
                        self.testLabel?.hidden = true
                        self.heartReadingValue = CGFloat(Double(heartInfo!)!)
                        self.heartReadingsArray.append(self.heartReadingValue)
                        self.plotReading()
                        self.showStats()
                    }
                  
                    
                   // HealthManager.sharedInstance.updateRealTimeUI(heartRate!, smpleDate: heartSampleDate!)
            }
        }
        else
        {
            //else will be called if workout is paused or ended
            timer1.invalidate()
            startTimer = false
            
            
            // Workout not in runnin state,
            if(isPaused == false)
            {
                //HealthManager.sharedInstance.isPause = false
                
                //loadWorkout value is true only when SAve Workout selected in watch
                if(loadWorkout == true) //
                {
                    
                    HealthManager.sharedInstance.isAnyNewWorkout = true
                    

                }
                dispatch_async(dispatch_get_main_queue())
                    {
                        self.testLabel?.hidden = false
                        self.zoneLabel?.text = ""
                        self.zoneLabel?.textColor = UIColor.whiteColor()
                        
                        self.intensityLabel?.text = ""
                        self.maxHeartRateLbl?.text = ""
                        self.avgHeartRateLbl?.text = ""
                        
                        self.readingLbl?.hidden = false
                        self.currentHeartRateView?.hidden = true
                        self.maxHeartRateView?.hidden = true
                        
                        self.currentHeartRateLbl?.text = ""
                        self.intensityTextLabel?.hidden = true
                        self.intensityLabel?.hidden = true
                        self.zoneLabel?.hidden = true
                        
                        self.hrs = 0
                        self.min = 0
                        self.sec = 0
                        self.timerLabel?.text = "00:00:00"
                        self.clearBarsOnGraphAfterWorkoutEnd()
                }
            
                //HealthManager.sharedInstance.clearBarGraphDelegate?.clearGraphOnStart()
            }
            else
            {
                //HealthManager.sharedInstance.isPause = true
            }
        }
    }
   
    /*
    func updateObserver(array:NSArray)
    {
        self.readingLbl?.hidden = true
        self.currentHeartRateView?.hidden = false
        self.maxHeartRateView?.hidden = false
        let heartRate = (array.lastObject) as? Double
        self.currentHeartRateLbl?.text = NSString(format: "%0.0f", (heartRate!)) as String
        self.intensityTextLabel?.hidden = false
        self.intensityLabel?.hidden = false
        self.zoneLabel?.hidden = false
        
        let age = 22
        let maxHeartRate :Double = Double(220-age)
        let currentBpm = array.lastObject as! Double
        print(currentBpm/maxHeartRate)
        var intensity  = Double(currentBpm/maxHeartRate)
        
        if heartRate < 91.0
        {
            self.zoneLabel?.text = "Rest"
            self.zoneLabel?.textColor = UIColor.whiteColor()
        }
        else if heartRate>=91.0 && heartRate <= 107.0
        {
            self.zoneLabel?.text = "Warm-up"
            self.zoneLabel?.textColor =  UIColor(
                red: 33/255.0,
                green: 125/255.0,
                blue: 168/255.0,
                alpha: 1.0)
        }
        else if heartRate>=108.0 && heartRate <= 125.0
        {
            self.zoneLabel?.text = "Endurance"
            self.zoneLabel?.textColor =  UIColor(
                red: 51/255.0,
                green: 199/255.0,
                blue: 231/255.0,
                alpha: 1.0)
        }
        else if heartRate>125.0 && heartRate <= 144.0
        {
            self.zoneLabel?.text = "Cardio"
            self.zoneLabel?.textColor =  UIColor(
                red: 243/255.0,
                green: 226/255.0,
                blue: 28/255.0,
                alpha: 1.0)
        }
        else if heartRate>=145.0 && heartRate <= 169.0
        {
            self.zoneLabel?.text = "Power"
            self.zoneLabel?.textColor =  UIColor(
                
                red: 225/255.0,
                green: 152/255.0,
                blue: 60/255.0,
                alpha: 1.0)
        }
        else if heartRate>169.0
        {
            self.zoneLabel?.text = "VO2 MAX"
            self.zoneLabel?.textColor =  UIColor(
                red: 211/255.0,
                green: 0/255.0,
                blue: 69/255.0,
                alpha: 1.0)
        }
        if intensity > 1.0
        {
            intensity = 1.0
        }
        self.intensityLabel?.text = NSString(format: "%d%@", Int(intensity*100),"%") as String
        
        let avg = Helper.sharedInstance.findAvg(self.heartReadingsArray)
        let max = Helper.sharedInstance.findMax(self.heartReadingsArray)
        
        self.maxHeartRateLbl?.text = NSString(format:"%@",String(max)) as String
        self.avgHeartRateLbl?.text = NSString(format:"%@",String(avg)) as String
    }
    */
  
    func showStats(){
        
        let baseY =  CGFloat(90.0)
        let deltaY = CGFloat(17.0)
        
        self.readingLbl?.hidden = true
        self.currentHeartRateView?.hidden = false
        self.maxHeartRateView?.hidden = false
       // let heartRate = (heartReadingsArray.lastObject) as? CGFloat
       // let heartRate = heartReadingValue
        self.currentHeartRateLbl?.text = NSString(format: "%0.0f", (self.heartReadingValue)) as String
        self.intensityTextLabel?.hidden = false
        self.intensityLabel?.hidden = false
        self.zoneLabel?.hidden = false
        
        let age = 22
        let maxHeartRate :CGFloat = CGFloat(220-age)
       // let currentBpm = array.lastObject as! Double
      //  print(currentBpm/maxHeartRate)
        var intensity  = Double(self.heartReadingValue/maxHeartRate)
        
        if heartReadingValue <= baseY
        {
            self.zoneLabel?.text = "Rest"
            self.zoneLabel?.textColor = UIColor.whiteColor()
        }
        else if heartReadingValue > baseY && heartReadingValue <= baseY + deltaY
        {
            self.zoneLabel?.text = "Warm-up"
            self.zoneLabel?.textColor =  UIColor(
                red: 33/255.0,
                green: 125/255.0,
                blue: 168/255.0,
                alpha: 1.0)
        }
        else if heartReadingValue > baseY + deltaY && heartReadingValue <= baseY + 2 * deltaY
        {
            self.zoneLabel?.text = "Endurance"
            self.zoneLabel?.textColor =  UIColor(
                red: 51/255.0,
                green: 199/255.0,
                blue: 231/255.0,
                alpha: 1.0)
        }
        else if heartReadingValue > baseY + 2 * deltaY && heartReadingValue <= baseY + 3 * deltaY
        {
            self.zoneLabel?.text = "Cardio"
            self.zoneLabel?.textColor =  UIColor(
                red: 243/255.0,
                green: 226/255.0,
                blue: 28/255.0,
                alpha: 1.0)
        }
        else if heartReadingValue > baseY + 3 * deltaY && heartReadingValue <= baseY + 4 * deltaY
        {
            self.zoneLabel?.text = "Power"
            self.zoneLabel?.textColor =  UIColor(
                
                red: 225/255.0,
                green: 152/255.0,
                blue: 60/255.0,
                alpha: 1.0)
        }
        else if heartReadingValue > baseY + 4 * deltaY
        {
            self.zoneLabel?.text = "VO2 MAX"
            self.zoneLabel?.textColor =  UIColor(
                red: 211/255.0,
                green: 0/255.0,
                blue: 69/255.0,
                alpha: 1.0)
        }
        if intensity > 1.0
        {
            intensity = 1.0
        }
        self.intensityLabel?.text = NSString(format: "%d%@", Int(intensity*100),"%") as String
        
        let avg = Helper.sharedInstance.findAvg(self.heartReadingsArray)
        let max = Helper.sharedInstance.findMax(self.heartReadingsArray)
        
        self.maxHeartRateLbl?.text = NSString(format:"%@",String(max)) as String
        self.avgHeartRateLbl?.text = NSString(format:"%@",String(avg)) as String
    }
    //Graph Methods
    func shiftGraphToLeft(){
        
        for view in self.barGraphView!.subviews {
            
            if view.tag == VERTICAL_LINE {
                view.center = CGPointMake(view.center.x - (widthOfBar + deltaXBars), view.center.y )
                if (view.center.x < -10){
                    view.removeFromSuperview()
                }
            }
        }
    }
    
  
    
    func createHorizontalLines(){
        let helper:Helper = Helper.sharedInstance
        let graphViewHeight = Int((self.barGraphView?.frame.size.height)!)
        
        let deltaY =  17
 
        
      /*  let darkBlueLineY = graphViewHeight - 90 ;
        let lightBlueLineY = darkBlueLineY - deltaY ;
        let yelloLineY = darkBlueLineY - 2 * deltaY ;
        let orangeLineY = darkBlueLineY - 3 * deltaY;
        let redLineY = darkBlueLineY - 4 * deltaY;*/
        
         let darkBlueLineY = graphViewHeight - 90 ;
         let lightBlueLineY = graphViewHeight - 107 ;
         let yelloLineY = graphViewHeight - 125 ;
         let orangeLineY = graphViewHeight - 144;
         let redLineY = graphViewHeight - 168;

      //  let greyLineView = UIView(frame: CGRectMake(0,CGFloat(greyLineY),(self.barGraphView?.frame.size.width)!, 1))
        
        
        var widthOfHorizontalLine = self.barGraphView?.frame.size.width
        if widthOfHorizontalLine < view.frame.size.width {
            widthOfHorizontalLine = view.frame.size.width
        }
        
        let darkBlueLineView = UIView(frame: CGRectMake(0,CGFloat(darkBlueLineY),(widthOfHorizontalLine)!, 1))
        let lightBlueLineView = UIView(frame: CGRectMake(0,CGFloat(lightBlueLineY),(widthOfHorizontalLine)!, 1))
        let yellowLineView = UIView(frame: CGRectMake(0,CGFloat(yelloLineY),(widthOfHorizontalLine)!, 1))
        let orangeLineView = UIView(frame: CGRectMake(0,CGFloat(orangeLineY),(widthOfHorizontalLine)!, 1))
        let redLineView = UIView(frame: CGRectMake(0,CGFloat(redLineY),(widthOfHorizontalLine)!, 1))
        let baseLineView = UIView(frame: CGRectMake(0,CGFloat(graphViewHeight),(widthOfHorizontalLine)!, 1))
        
       // greyLineView.backgroundColor = helper.greyColor
        darkBlueLineView.backgroundColor = helper.darkBlueColor
        lightBlueLineView.backgroundColor = helper.lightBlueColor
        yellowLineView.backgroundColor = helper.yellowColor
        orangeLineView.backgroundColor = helper.orangeColor
        redLineView.backgroundColor = helper.redColor
        baseLineView.backgroundColor = UIColor.whiteColor()
        
        //greyLineView.tag = HORIZONTAL_LINE
        darkBlueLineView.tag = HORIZONTAL_LINE
        lightBlueLineView.tag = HORIZONTAL_LINE
        yellowLineView.tag = HORIZONTAL_LINE
        orangeLineView.tag = HORIZONTAL_LINE
        redLineView.tag = HORIZONTAL_LINE
        baseLineView.tag = HORIZONTAL_LINE
        
        //self.barGraphView?.addSubview(greyLineView)
        self.barGraphView?.addSubview(darkBlueLineView)
        self.barGraphView?.addSubview(lightBlueLineView)
        self.barGraphView?.addSubview(yellowLineView)
        self.barGraphView?.addSubview(orangeLineView)
        self.barGraphView?.addSubview(redLineView)
        self.barGraphView?.addSubview(baseLineView)
    }
    func plotReading(){
        
        if heartReadingValue > (self.barGraphView?.frame.size.height)! {
             heartReadingValue = (self.barGraphView?.frame.size.height)!
        }
        
        let heightOfBar:CGFloat =  heartReadingValue
        let view = UIView(frame: CGRectMake((self.barGraphView?.frame.size.width)! - 2 * widthOfBar,(self.barGraphView?.frame.size.height)! - heightOfBar,widthOfBar, heightOfBar))
      //  view.center.x =
        view.backgroundColor = Helper.sharedInstance.getBarColor(heartReadingValue)
        view.tag = VERTICAL_LINE
        shiftGraphToLeft()
        self.barGraphView?.addSubview(view)
        self.showStats()
       
        
      //  self.generateAndPlotRandomReadingAfterRandomTime()
    }
    
    func generateAndPlotRandomReadingAfterRandomTime(){
        
       // 1 to 5
        if !isWorkoutPaused {
            var randomTime = Double(Int(arc4random_uniform(5) + 1))
            randomTime = 1
            heartReadingValue = CGFloat(Int(arc4random_uniform(160) + 60))
            self.heartReadingsArray.append(heartReadingValue)
            NSTimer.scheduledTimerWithTimeInterval(randomTime, target: self, selector: #selector(RealTimeVC.plotReading), userInfo: nil, repeats: false)
        }
   
        
    }
    //CAlled ever after after workout is started
    func update()
    {
        
        self.timerLabel?.text = self.getTimeInStringFormat()
       // if  !isWorkoutPaused {
            
           // self.shiftGraphToLeft()
       // }
        
       // HealthManager.sharedInstance.timerValue += 1
      //  HealthManager.sharedInstance.updateGraphSecDelegate?.updateGraphEverySecond()
    }
    
    
    /*
    
    func saveHeartRate()
    {
        dispatch_async(dispatch_get_main_queue())
        {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd/MM/YYYY HH:mm:ss a"
            
            let timeStr = formatter.stringFromDate(NSDate())
            let  heartRate = Double(self.heartInfo)*100
            HealthManager.sharedInstance.updateRealTimeUI(heartRate, smpleDate: timeStr)
            
            self.heartInfo = self.heartInfo + 1
        }
    }
    */
    
    func clearBarsOnGraphAfterWorkoutEnd(){
        
        for view in (self.barGraphView?.subviews)! {
            if (view.tag == VERTICAL_LINE) {
                view.removeFromSuperview()
            }
        }
    }
    
    func getTimeInStringFormat()->String
    {
        var timeStr = "";
        //for testing race view
        if (sec < 59)
        {
            sec += 1;
        }
        else if (sec >= 59 && min < 59)
        {
            min = min + 1;
            sec = 0;
        }
        else if (min >= 59 && hrs < 23)
        {
            min = 0;
            hrs += 1;
        }
        else
        {
            hrs = 0;
            min = 0;
            sec = 0;
        }
        timeStr = NSString(format: "%02d:%02d:%02d", hrs,min,sec) as String
        return timeStr
    }
    
}
