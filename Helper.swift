//
//  Helper.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 9/30/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.


import Foundation
import CoreGraphics
import UIKit

class Helper {
    let greyColor = UIColor(red:155/255.0,green:155/255.0,blue:155/255.0,alpha:1)
    let darkBlueColor =   UIColor(red:33/255.0,green:125/255.0,blue:168/255.0,alpha:1)
    let lightBlueColor =  UIColor(red:51/255.0,green:199/255.0,blue:231/255.0,alpha:1)
    let yellowColor =  UIColor(red:243/255.0,green:226/255.0,blue:28/255.0,alpha:1)
    let orangeColor =  UIColor(red:255/255.0,green:152/255.0,blue:60/255.0,alpha:1)
    let redColor =  UIColor(red:211/255.0,green:0/255.0,blue:69/255.0,alpha:1)
    
    var parentData = NSArray()
    let currentDate = NSDate()
    var averageTimeHeartRate :Float = 0.0
    
    class var sharedInstance :Helper{
        struct Singleton {
            static let instance = Helper()
        }
        
        return Singleton.instance
        
    }
    /*
 if a<=90
 {
 CGContextSetRGBFillColor(context,155/255.0,155/255.0,155/255.0,1)
 }
 else if a>=91 && a<=107
 {
 CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
 }
 else if a>=108 && a<=125
 {
 CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
 }
 else if a>=126 && a<=144
 {
 CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
 }
 else if a>=145 && a<=168
 {
 CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
 }
 else if a>=169
 {
 CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
 }
*/
    func getBarColor(heartReadingValue:CGFloat)->UIColor{
 
    //    let baseY = CGFloat(90.0)
     //   let deltaY = CGFloat(17.0)
        let a = heartReadingValue
        if a <= 90
        {
            return greyColor
        }
        else if a >= 91 && a <= 107 //91 to 107
        {
            return   darkBlueColor
            //  CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if a >= 108 && a <= 125 //108 to 125
        {
            return lightBlueColor
            // CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if  a >= 126 && a <= 144
        {
            return yellowColor
            //CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if  a >= 145 && a <= 168
        {
            return orangeColor
            // CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if a >= 169
        {
            return redColor
            // CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
        else{
            return redColor
        }
        
        
        
    }
    func findMax(array : [CGFloat]) ->Int
    {
        var max: Int = Int(array[0])
        if array.count>1
        {
            for i in 1  ..< array.count 
            {
                let next: Int = Int(array[i])
                
                if max < next
                {
                    max = next
                }
            }
        }
        
        return max
    }
    
    
    func findAvg(array : [CGFloat]) ->Int
    {
        var sum :Int = 0
        var avg :Int = 0
        let total :Int = array.count
        
        for i in 0  ..< array.count
        {
            let next: Int = Int(array[i])
            sum = sum + next
        }
        avg = sum/total
        return avg
    }
    
    func showTime(var hour:Int, var minute:Int, var second:Int) -> NSString
    {
        var timeStr : String = ""
        var secondStr : NSString = ""
        var minuteStr : NSString = ""
        let hourStr : NSString = ""
        
        if hour < 0
        {
            hour = hour*(-1)
        }
        if minute < 0
        {
            minute = minute*(-1)
        }
        if second < 0
        {
            second = second*(-1)
        }
        
        if hour != 0
        {
            minuteStr = NSString(format: "%d%@", hour,"h")
        }
        
        if minute == 0 && second == 0
        {
            timeStr  = NSString(format: "%d%@", hour,"h") as String
        }
        else
        {
            if second != 0
            {
                secondStr = NSString(format: "%d%@", second,"s")
            }
            minuteStr = NSString(format: "%d%@", minute,"m")
        }
        
        timeStr = NSString(format: "%@ %@ %@", hourStr,minuteStr,secondStr) as String
        return timeStr
    }
    
    
        


    func arrivalTimeOfHeartRate(date1:NSDate, date2:NSDate)-> Int
    {
        var timeInSecs :Int = 0
        
        let calendar = NSCalendar.currentCalendar();
        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]
        let components = calendar.components(unitFlags, fromDate: date1, toDate: date2, options: [])
        
        let hour = Int(components.hour)
        let min = Int(components.minute)
        let sec = Int(components.second)
        
        if hour > 0
        {
            timeInSecs = timeInSecs + (hour * 3600)
        }
        
        if min > 0
        {
            timeInSecs = timeInSecs + (min * 60)
        }
        if sec > 0
        {
            timeInSecs = timeInSecs + sec
        }
        
        return timeInSecs
    }
    
    
    func zoneTimeValue( value : Int) -> (String)
    {
        var s = ""
        var secs = value
        var mins = 0
        var hour = 0
        if value < 60
        {
            s = NSString(format: "%@%02d", "00:00:",secs) as String
        }
        else
        {
            hour = secs/3600
            secs = secs%3600
            if secs != 0
            {
                mins = secs/60
                secs = secs%60
            }
            
            s = NSString(format: "%02d%@%02d%@%02d",hour,":",mins,":",secs) as String
        }
        
        return s
    }
    
    
    func getTimeInStringFormat(var sec:Int, var min: Int, var hrs: Int)->String{
        var timeStr = "";
        //for testing race view
        if (sec < 59)
        {
            sec += 1;
        }
        else if (sec >= 59 && min < 59)
        {
            min = min + 1;
            sec = 0;
        }
        else if (min >= 59 && hrs < 23)
        {
            min = 0;
            hrs += 1;
        }
        else
        {
            hrs = 0;
            min = 0;
            sec = 0;
        }
        timeStr = NSString(format: "%02d:%02d:%02d", hrs,min,sec) as String
        return timeStr

    }
}
