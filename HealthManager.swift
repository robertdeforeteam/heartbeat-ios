
//
//  HealthManager.swift


import Foundation
import HealthKit


protocol clearGraphDelegate {
    func clearGraphOnStart()
}


protocol updateGraphEverySecDelegate {
    func updateGraphEverySecond()
}

protocol updateProfileData {
    func updateAboutNewprofile()
}


protocol UpdateObserverDelegate {
    func updateObserver(array:NSArray)
}

protocol AddUpdateObserverDelegate {
    func updateObserverCall(heartRate:Double ,sampleTime:String)
}

protocol AddHistoryUpdateDelegate {
   
    func historyUpdate(historyStructure:[HKWorkout])
}

protocol AddWorkoutUpdateDelegate {
    func workoutDetailUpdate(historyStructure:[HKQuantitySample])
   
}

class HealthManager {
    
    var isAnyNewWorkout:Bool = false
    var isPause  = false
    var barXValue : Int?
    
     var graphCount : Int?
    var historyWorkouts: [HKWorkout] = []
    var queryHeartRate : [Bool] = []
    var queryRate = false
    
    var heartRateArr : [[Double]] = []
    var heartRateArray :[Double] = []
    
    var historyWorkoutArr : [HKWorkout] = []
    var timeSecs = 0;
    var callDrawFunc :Bool?
    var loadWorkoutData :Bool?
    
    var lastSavedWorkOut : HKWorkout?
    
    var selectedWorkOutData : HKWorkout?
    
    var workouts: [HKWorkout] = []
    
    var removeAllValues = false
    
    var timerValue = 0
    
    var healthKitStore:HKHealthStore = HKHealthStore()
    var anchor = Int(HKAnchoredObjectQueryNoAnchor)
    
    var fetchHistoryCallAgain = true
    
    var TotalCalories :Double?
    
    var numberOfSectionInHistoryTable = 0
    
    var historyStartDate : NSDate?
    
    var usersWeight :Double?
    
    var usersHeight :Double?
    
    var samplesIndex : [Int] = []
    
    var usersAge :Int?
    
    var biologicalSex:HKBiologicalSexObject?
    
    var clearBarGraphDelegate : clearGraphDelegate?
    
    var workoutUpdateDelegate : AddWorkoutUpdateDelegate?
    
    var updateGraphSecDelegate :updateGraphEverySecDelegate?
    
    var updateProfileDelegate :updateProfileData?
    
    var updateObserverDelegate: AddUpdateObserverDelegate?
    
    var updateHistoryDelegate : AddHistoryUpdateDelegate?
    
    var observerDelegateForRealTime : UpdateObserverDelegate?
    
    var historyStructure:[HKQuantitySample]=[];
    
    var historyWorkoutTimeType : Int = 0
    
    var totalWorkoutTimeInterval:Int = 0
    
    var workoutStart :Bool = false
    
    class var sharedInstance :HealthManager
    {
        struct Singleton {
            static let instance = HealthManager()
        }
        return Singleton.instance
    }
    
    func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        let healthKitTypesToRead = Set(arrayLiteral:
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
            HKObjectType.workoutType()
        )
        
        // 2. Set the types you want to write to HK Store
        let healthKitTypesToWrite = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            
            HKQuantityType.workoutType()
        )
        
        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "com.apple.tutorials.healthkit", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return;
        }
        
        // 4.  Request HealthKit authorization
        
        healthKitStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
            if( completion != nil )
            {
                completion(success:success,error:error)
            }
        }
    }
    
    func readProfile() -> (Int?)
    {
        var error:NSError?
        
        // 1. Request birthday and calculate age
        do {
            let birthDay = try healthKitStore.dateOfBirth()
            let today = NSDate()
            _ = NSCalendar.currentCalendar()
            let differenceComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDay, toDate: today, options: NSCalendarOptions(rawValue: 0) )
            usersAge = differenceComponents.year
            
            let ageUserDefault = NSUserDefaults.standardUserDefaults().objectForKey("Age")
            if ageUserDefault == nil
            {
                NSUserDefaults.standardUserDefaults().setObject(self.usersAge, forKey: "Age")
                NSUserDefaults.standardUserDefaults().synchronize()
            }
            // print("age",usersAge)
        } catch let error1 as NSError {
            error = error1
        }
        if error != nil {
            print("Error reading Birthday: \(error)")
            
        }
        
        // 2. Read biological sex
        do {
            biologicalSex = try healthKitStore.biologicalSex()
        } catch let error1 as NSError {
            error = error1
            biologicalSex = nil
        };
        print("bio",biologicalSex)
        if error != nil {
            print("Error reading Biological Sex: \(error)")
        }
        //        // 3. Read blood type
        //        var bloodType:HKBloodTypeObject?
        //        do {
        //            bloodType = try healthKitStore.bloodType()
        //        } catch let error1 as NSError {
        //            error = error1
        //            bloodType = nil
        //        };
        //        if error != nil {
        //            print("Error reading Blood Type: \(error)")
        //        }
        
        updateUsersWeight()
        
        if usersAge != nil && usersHeight != nil && usersWeight != nil && biologicalSex != nil
        {
            
            return 0
        }
        else
        {
            return (usersAge)
        }
        
    }
    
    
    func updateUsersWeight()
    {
        // Query to get the user's latest weight, if it exists.
        let weightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!
        
        aapl_mostRecentQuantitySampleOfType(weightType, predicate: nil) {mostRecentQuantity, error in
            if mostRecentQuantity == nil {
                NSLog("Either an error occured fetching the user's weight information or none has been stored yet. In your app, try to handle this gracefully.")
                
            }
            else
            {
                // Determine the weight in the required unit.
                let weightUnit = HKUnit.gramUnit()
                self.usersWeight = mostRecentQuantity!.doubleValueForUnit(weightUnit)
                print("weight",self.usersWeight)
            }
            
            self.updateUsersHeight()
        }
    }
    
    func updateUsersHeight()
    {
        let lengthFormatter = NSLengthFormatter()
        lengthFormatter.unitStyle = NSFormattingUnitStyle.Long
        
        // self.heightUnitLabel.text = String(format: localizedHeightUnitDescriptionFormat, heightUnitString)
        
        let heightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!
        
        // Query to get the user's latest height, if it exists.
        aapl_mostRecentQuantitySampleOfType(heightType, predicate: nil) {mostRecentQuantity, error in
            if mostRecentQuantity == nil {
                NSLog("Either an error occured fetching the user's height information or none has been stored yet. In your app, try to handle this gracefully.")
                
                dispatch_async(dispatch_get_main_queue()) {
                    //self.heightValueLabel.text = NSLocalizedString("Not available", comment: "")
                }
            } else {
                // Determine the height in the required unit.
                let heightUnit = HKUnit.meterUnit()
                self.usersHeight = mostRecentQuantity!.doubleValueForUnit(heightUnit)
                self.usersHeight = self.usersHeight!
                print("Height",self.usersHeight)
            }
            self.updateProfileDelegate?.updateAboutNewprofile()
        }
    }
    
    
    func aapl_mostRecentQuantitySampleOfType(quantityType: HKQuantityType, predicate: NSPredicate?, completion: ((HKQuantity?, NSError?)->Void)?) {
        let timeSortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let healthStore = HKHealthStore()
        // Since we are interested in retrieving the user's latest sample, we sort the samples in descending order, and set the limit to 1. We are not filtering the data, and so the predicate is set to nil.
        let query = HKSampleQuery(sampleType: quantityType, predicate: nil, limit: 1, sortDescriptors: [timeSortDescriptor]) {query, results, error in
            if results == nil {
                completion?(nil, error)
                
                return
            }
            
            if completion != nil {
                // If quantity isn't in the database, return nil in the completion block.
                let quantitySample = results!.first as? HKQuantitySample
                let quantity = quantitySample?.quantity
                
                completion!(quantity, error)
            }
        }
        healthStore.executeQuery(query)
    }
    
    
    func readTotalEnergy(sampleType:HKSampleType ,startDate:NSDate, completion: ((HKSample!, NSError!) -> Void)!)
    {
        let now  = startDate
        
        let periodComponents = NSDateComponents()
        periodComponents.second = -1
        
        let then = NSCalendar.currentCalendar().dateByAddingComponents(
            periodComponents,
            toDate:now,
            options: [])!
        
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(then, endDate:now, options: .None)
        
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                
                if let _ = error {
                    completion(nil,error)
                    return;
                }
                for result in results! {
                    //print("history \(result)")
                    if let sample = result as? HKQuantitySample {
                        let unit = HKUnit.kilocalorieUnit()
                        self.TotalCalories = sample.quantity.doubleValueForUnit(unit)
                    }
                }
        }
        self.healthKitStore.executeQuery(sampleQuery)
    }
    
    func reterieveSavedWorkouts()
    {
        self.healthKitStore = HKHealthStore()
        let sampleType = HKObjectType.workoutType()
     //   let startDate = NSDate()
       // let endDate = NSDate.distantPast()
        
        
        //let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
       // let limit = 0
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        self.workouts.removeAll()
        let query = HKSampleQuery(
            sampleType: sampleType,
            predicate: nil,
            limit: Int(HKObjectQueryNoLimit),
            sortDescriptors: [ sortDescriptor ]) { (query: HKSampleQuery, results: [HKSample]?, error: NSError?) -> Void in
                

                if let results = results {
                    for result in results {
                        if let workout = result as? HKWorkout {
                            // Here's a HKWorkout object
                            let sourceRevision = workout.sourceRevision
                            let currentSource = HKSource.defaultSource()
                            
                            if sourceRevision.source == currentSource {
                                self.workouts.append(workout)
                            }
                        }
                    }
                    print(self.workouts)
                    self.updateHistoryDelegate?.historyUpdate(self.workouts);
                    
                }
                else {
                       NSLog("Error:" + error!.description)
                    // No results were returned, check the error
                    //                    self.updateHistoryDelegate?.historyUpdate(self.historyStructure);
                }
        }
        
        
        self.healthKitStore.executeQuery(query)
    }
    
    
    func deleteHeartRateData(heartRateSampleType:HKSampleType,startDate:NSDate,endDate:NSDate)
    {
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: .None)
        
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        
        let sampleQuery = HKSampleQuery(sampleType: heartRateSampleType, predicate: predicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                
                if let _ = error {
                    
                    return;
                }
                
                
                for result in results! {
                    if let sample = result as? HKQuantitySample {
                        self.healthKitStore.deleteObject(sample, withCompletion: { (deleted, error) -> Void in
                            
                        })
                    }
                }
        }
        self.healthKitStore.executeQuery(sampleQuery)
    }
    
    func readMostRecentSample(sampleType:HKSampleType ,startDate:NSDate,endDate:NSDate, completion: ((HKSample!, NSError!) -> Void)!)
    {
        // 1. Build the Predicate
        // let diffComponents = NSCalendar.currentCalendar().components(unitFlags, fromDate: startDate, toDate: endDate, options: [])
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate:endDate, options: .None)
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: Int(HKObjectQueryNoLimit), sortDescriptors:[sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                let now  = startDate
                let currentDate = NSDate()
                let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second,.Day,.Month,.Year]
                let components = NSCalendar.currentCalendar().components(unitFlags,fromDate: currentDate)
                let currentDateYear = Int(components.year)
                let periodComponents = NSDateComponents()
                periodComponents.month = -1
                if periodComponents.month == 0
                {
                    periodComponents.month = 12
                    periodComponents.year = -1
                }
                //periodComponents.hour = -1
                
                let then = NSCalendar.currentCalendar().dateByAddingComponents(
                    periodComponents,
                    toDate:now,
                    options: [])!

                if let _ = error {
                    completion(nil,error)
                    return;
                }
                
                
                periodComponents.day = +1
                self.historyStartDate = NSCalendar.currentCalendar().dateByAddingComponents(
                    periodComponents,
                    toDate:then,
                    options: [])!
                
                var sameDateObjectArray :[HKQuantitySample]=[];
                var index=0;
                var previousYear =  0
                var previousMonth = 0
                var previousDay = 0
                
                //                if self.historyStructure.count > 0
                //                {
                //                    self.historyStructure .removeAll()
                //                }
                print(self.workouts.count)
                self.historyStructure.removeAll()
                
                for result in results!
                {
                    if let sample = result as? HKQuantitySample {
                        //                                    let heartRateUnit = HKUnit(fromString: "count/min")
                        //                                    let heartRate : Double = sample.quantity.doubleValueForUnit(heartRateUnit)
                        let date:NSDate=sample.endDate;
                        let calendar = NSCalendar.currentCalendar();
                        let unitFlags: NSCalendarUnit = [.Hour, .Day, .Month, .Year]
                        let components = calendar.components(unitFlags , fromDate: date)
                        let year = components.year
                        let month = components.month
                        let day = components.day
                        
                        var sampleTotalIndex = 0
                        
                        let diffYear = currentDateYear - year
                        
                        if diffYear <= 4
                        {
                            self.fetchHistoryCallAgain = true
                            if(index==0){
                                
                                self.numberOfSectionInHistoryTable = 1
                                self.historyStructure.append(sample);
                                //                                            sameDateObjectArray.append(sample);
                                previousYear=year;
                                previousMonth=month;
                                previousDay=day;
                                index += 1;
                            }else{
                                if(previousYear==year && previousMonth==month && previousDay==day){
                                    //                                                sameDateObjectArray.append(sample);
                                    self.historyStructure.append(sample);
                                    index += 1;
                                    //                                                if(index==results?.count){
                                    //                                                    self.historyStructure.append(sameDateObjectArray);
                                    //                                                }
                                }else{
                                    
                                    self.numberOfSectionInHistoryTable = +1
                                    //                                                self.historyStructure.append(sameDateObjectArray);
                                    self.historyStructure.append(sample);
                                    //                                                sameDateObjectArray.removeAll();
                                    //                                                sameDateObjectArray.append(sample);
                                    previousYear=year;
                                    previousMonth=month;
                                    previousDay=day;
                                    index += 1;
                                }
                                
                                if previousYear == year
                                {
                                    if month != previousMonth
                                    {
                                        self.numberOfSectionInHistoryTable = +1
                                    }
                                    else
                                    {
                                        
                                    }
                                }
                                else
                                {
                                    self.numberOfSectionInHistoryTable = +1
                                }
                                
                            }
                        }
                        else
                        {
                            self.fetchHistoryCallAgain = false
                        }
                    }
                   
                }
              //   print("history count \(self.historyStructure.count)")
               self.workoutUpdateDelegate?.workoutDetailUpdate(self.historyStructure);
        }
        self.healthKitStore.executeQuery(sampleQuery)
        
        // let limit = 10
    }
    
    
    func updateFuncForNewHeartRateUsingAnchorObjectQuery()
    {
        let heartSampleType = HKObjectType.workoutType()
        let now = NSDate()
        let past = NSDate.distantPast()
        var anchorValue = Int(HKAnchoredObjectQueryNoAnchor)
        // Match samples with a start date after the workout start
        let predicate = HKQuery.predicateForSamplesWithStartDate(past, endDate: now, options: .None)
        
        if #available(iOS 9.0, *)
        {
            anchorValue = anchor
            
            let query1 = HKAnchoredObjectQuery(type: heartSampleType, predicate: predicate, anchor: anchorValue, limit: Int(HKObjectQueryNoLimit), completionHandler: { (query, results, newAnchor, error) -> Void in
                
                if error != nil {
                    // Perform proper error handling here...
                    print("An error occured while setting up the heartrate observer.")
                    abort()
                }
                
                self.anchor = newAnchor;
                
                for result in results! {
                    print(result)
                    }
            });
            
            self.healthKitStore.executeQuery(query1)
        }
    }
    
    func updateRealTimeUI(heartRate: Double, smpleDate:String)
    {
        //print("%f %@",heartRate,"heart rate value passed")
        self.updateObserverDelegate?.updateObserverCall(heartRate,sampleTime:smpleDate)
    }
    
    func updateHeartRateUsingObserverQuery()
    {
        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        let now = NSDate()
        let predicate = HKQuery.predicateForSamplesWithStartDate(now, endDate: nil, options: .None)
        
        
        let query = HKObserverQuery(sampleType: sampleType!, predicate: predicate) {
            query, completionHandler, error in
            
            if error != nil {
                // Perform Proper Error Handling Here...
                print("*** An error occured while setting up the HeartRate observer. \(error!.localizedDescription) ***")
                abort()
            }
            
            HealthManager.sharedInstance.updateFuncForNewHeartRateUsingAnchorObjectQuery()
            
            //              let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
            //                    HealthManager.sharedInstance.readMostRecentSample(sampleType!, completion: { (mostRecentHeartRate, error) -> Void in
            //
            //                        if( error != nil )
            //                        {
            //                            print("Error reading weight from HealthKit Store: \(error.localizedDescription)")
            //                            return;
            //                        }
            //                    });
            //
        }
        self.healthKitStore.executeQuery(query)
    }
    
    
    
    func saveHeartRateQuantitySample(heartRateSample:HKQuantitySample)
    {
        healthKitStore.saveObject(heartRateSample, withCompletion: { (success, error) -> Void in
            if( error != nil )
            {
                print("Error saving Heartrate sample: \(error!.localizedDescription)")
            }
            else
            {
                print("Heartrate sample saved successfully!")
            }
        })
    }
    
    
    
    func saveHeartRateSample(heartRate:Double, date:NSDate )
    {
        // 1. Create a Heartrate Sample
        
        let heartRateUnit = HKUnit(fromString: "count/s")
        
        HKUnit.secondUnit()
        let heartRateType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        let heartRateQuantity = HKQuantity(unit: heartRateUnit, doubleValue: heartRate)
        let heartRateSample = HKQuantitySample(type: heartRateType!, quantity: heartRateQuantity, startDate: date, endDate: date)
        
        // 2. Save the sample in the store
        healthKitStore.saveObject(heartRateSample, withCompletion: { (success, error) -> Void in
            if( error != nil )
            {
                print("Error saving Heartrate sample: \(error!.localizedDescription)")
            }
            else
            {
                print("Heartrate sample saved successfully!")
            }
        })
    }
    
    func updateFuncForNewHeight()
    {
        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        let now = NSDate()
        let predicate = HKQuery.predicateForSamplesWithStartDate(now, endDate: nil, options: .None)
        
        
        let query = HKObserverQuery(sampleType: sampleType!, predicate: predicate) {
            query, completionHandler, error in
            
            if error != nil {
                // Perform Proper Error Handling Here...
                print("*** An error occured while setting up the HeartRate observer. \(error!.localizedDescription) ***")
                abort()
            }
            
            let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
            let now = NSDate()
            let past = NSDate.distantPast()
            var anchorValue = Int(HKAnchoredObjectQueryNoAnchor)
            // Match samples with a start date after the workout start
            let predicate = HKQuery.predicateForSamplesWithStartDate(past, endDate: now, options: .None)
            
            if #available(iOS 9.0, *)
            {
                anchorValue = self.anchor
                
                let query1 = HKAnchoredObjectQuery(type: heartSampleType!, predicate: predicate, anchor: anchorValue, limit: Int(HKObjectQueryNoLimit), completionHandler: { (query, results, newAnchor, error) -> Void in
                    
                    if error != nil {
                        // Perform proper error handling here...
                        print("An error occured while setting up the heartrate observer.")
                        abort()
                    }
                    
                    self.anchor = newAnchor;
                    
                    for result in results! {
                        if let sample = result as? HKQuantitySample {
                            let heightUnit = HKUnit(fromString: "meter")
                            let height : Double = sample.quantity.doubleValueForUnit(heightUnit)
                            self.usersHeight = height
                            self.updateProfileDelegate?.updateAboutNewprofile()
                        }
                    }
                });
                self.healthKitStore.executeQuery(query1)
            }
        }
        self.healthKitStore.executeQuery(query)
        
    }
    
    func updateFuncForNewWeight()
    {
        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        let now = NSDate()
        let predicate = HKQuery.predicateForSamplesWithStartDate(now, endDate: nil, options: .None)
        
        
        let query = HKObserverQuery(sampleType: sampleType!, predicate: predicate) {
            query, completionHandler, error in
            
            if error != nil {
                // Perform Proper Error Handling Here...
                print("*** An error occured while setting up the HeartRate observer. \(error!.localizedDescription) ***")
                abort()
            }
            
            let heartSampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
            let now = NSDate()
            let past = NSDate.distantPast()
            var anchorValue = Int(HKAnchoredObjectQueryNoAnchor)
            // Match samples with a start date after the workout start
            let predicate = HKQuery.predicateForSamplesWithStartDate(past, endDate: now, options: .None)
            
            if #available(iOS 9.0, *)
            {
                anchorValue = self.anchor
                
                let query1 = HKAnchoredObjectQuery(type: heartSampleType!, predicate: predicate, anchor: anchorValue, limit: Int(HKObjectQueryNoLimit), completionHandler: { (query, results, newAnchor, error) -> Void in
                    
                    if error != nil {
                        // Perform proper error handling here...
                        print("An error occured while setting up the heartrate observer.")
                        abort()
                    }
                    
                    self.anchor = newAnchor;
                    
                    for result in results! {
                        if let sample = result as? HKQuantitySample {
                            let weightUnit = HKUnit(fromString: "gram")
                            let weight : Double = sample.quantity.doubleValueForUnit(weightUnit)
                            self.usersWeight = weight
                            print(weight)
                            self.updateProfileDelegate?.updateAboutNewprofile()
                        }
                    }
                });
                self.healthKitStore.executeQuery(query1)
            }
        }
        self.healthKitStore.executeQuery(query)
    }
    
}
