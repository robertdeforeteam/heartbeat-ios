//
//  GraphView.swift
//  GraphDemo
//
//  Created by Rohit Garg on 05/09/15.
//  Copyright (c) 2015 Rohit Garg. All rights reserved.
//

import Foundation
import UIKit
import HealthKit

class GraphView: UIView {
    
    var value1:Float?
    var value2:Float?
    var value3:Float?
    var value4:Float?
    var value5:Float?
    
    //For Lines
    var kGraphHeight: Float = 250
    var kDefaultGraphWidth: Float = 900
    var kOffsetX: Float = 0
    var kStepX: Float = 60
    var kOffsetY: Float = 0
    var kStepY: Float = 25
    var kGraphBottom: Float = 300
    var kGraphTop: Float = 0
    var textIndex :Int = 0
    var kBarStepX: Float = 21
    
    //For LineGraph
    var kCircleRadius: Float = 1.5
    
    var touchAreasArrayForBar :[CGRect] = []
    var dataArray: [Float] = []
    var HeartDataArray: [Double] = []
    var indexArray: [Int] = []
    
    var sampleStartDateArray : [String] = []
    var barGraphArray :[Float] = []
    var workoutStartDate : NSDate!
    var numberOfVerticalLines = 0
    
    var kCircleRadiusC : CGFloat = 0.07

    var kBarWidth: Float = 16.0
    
    var currentBarValue:Double=0
    
    var kBarTop: Float = 10
    var scrollView: UIScrollView?
    override func drawRect(rect: CGRect) {
        
        let context:CGContextRef = UIGraphicsGetCurrentContext()!
        dataArray.removeAll()
        HeartDataArray.removeAll()
        sampleStartDateArray.removeAll()
        
        if(HealthManager.sharedInstance.queryRate == true)
        {
            //HealthManager.sharedInstance.heartRateArray
            
            for  i in 0  ..< HealthManager.sharedInstance.heartRateArray.count 
            {
                
                let heartRate : Double = HealthManager.sharedInstance.heartRateArray[i]
                
                var HeartRate = Float(heartRate)/180
                
                if HeartRate > 1
                {
                    HeartRate = 1
                }
                if HeartRate < 0
                {
                    HeartRate = 0
                }
                HeartDataArray.append(heartRate)
                dataArray.append(HeartRate)
            }

        }
        else
        {
            for  i in 0  ..< HealthManager.sharedInstance.historyStructure.count 
            {
                let sample = HealthManager.sharedInstance.historyStructure[i]
                let heartRateUnit = HKUnit(fromString: "count/min")
                let startDate = sample.startDate
                
                if i == 0
                {
                    workoutStartDate = startDate
                }
                
                let heartRate : Double = sample.quantity.doubleValueForUnit(heartRateUnit)
                var HeartRate = Float(heartRate)/180
                
                if HeartRate > 1
                {
                    HeartRate = 1
                }
                if HeartRate < 0
                {
                    HeartRate = 0
                }
                HeartDataArray.append(heartRate)
                dataArray.append(HeartRate)
            }
        }
        if(dataArray.count != 0 && HeartDataArray.count != 0)
        {
            CGContextSetLineWidth(context, 0.6);
            CGContextSetStrokeColorWithColor(context, UIColor.darkGrayColor().CGColor);
            
            createHorizontalLines(context);
            self.createBar(context);
            bottomline(context)
        }
    }
    
    //------------For Grid----------------------
    func createVarticalLines(context : CGContextRef){
        let howMany = Int((Float(self.frame.width) - kOffsetX) / kStepX);
        numberOfVerticalLines = howMany
        //print(howMany)
        for index in 0...howMany{
            let valueX: Float = (kOffsetX + Float(index) * kStepX);
            CGContextMoveToPoint(context, CGFloat(valueX), CGFloat(kGraphTop));
            CGContextAddLineToPoint(context, CGFloat(valueX), CGFloat(kGraphBottom));
        }
    }
    
    
    func createHorizontalLines(context : CGContextRef)
    {
        for index in 1...5{
            //let valueY: Float = (Float(index) * kStepY)
            //            CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(valueY))
            //            CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(valueY))
            CGContextSetLineWidth(context, 1)
            if index == 1
            {
                value1 = 33;
                //red
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value1!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value1!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 211/255.0, green: 0/255.0, blue: 69/255.0, alpha: 1.0).CGColor)
            }
            else  if index == 2
            {
                value2 = 57;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value2!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value2!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 248/255.0, green: 167/255.0, blue: 66/255.0, alpha: 1.0).CGColor)
            }
            else if index == 3
            {
                value3 = 76;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value3!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value3!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 246/255.0, green: 229/255.0, blue: 25/255.0, alpha: 1.0).CGColor)
            }
            else if index == 4
            {
                value4 = 94;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value4!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value4!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 47/255.0, green: 181/255.0, blue: 219/255.0, alpha: 1.0).CGColor)
            }
            else if index == 5
            {
                value5 = 111;
                CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(value5!))
                CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(value5!))
                CGContextSetStrokeColorWithColor(context,UIColor(red: 28/255.0, green: 105/255.0, blue: 151/255.0, alpha: 1.0).CGColor)
            }
            CGContextStrokePath(context)
        }
        
    }
    
    
    //------------For Bar-----------------------
    
    func setscrollView(scrolView: UIScrollView){
        scrollView = scrolView;
    }
    
    //------------For Line----------------------
    
    func drawLineGraph(context : CGContextRef)
    {
        CGContextSetLineWidth(context, 2.0);
        CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor);
        let maxGraphHeight :Float = kGraphHeight - kOffsetY;
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, CGFloat(kOffsetX),  CGFloat(kGraphHeight - maxGraphHeight * dataArray[0]));
        // var timeInSecs : Int = 0
        
        for index in 0..<dataArray.count
        {
            print(CGFloat(kOffsetX + (Float(index) * kStepX)/10))
            CGContextAddLineToPoint(context, CGFloat(kOffsetX + (Float(index+4) * kStepX)/10), CGFloat(kGraphHeight - maxGraphHeight * dataArray[index]));
        }
        CGContextDrawPath(context, CGPathDrawingMode.FillStroke);
    }
    
    func createBar(context:CGContextRef)
    {
        if dataArray.count > 0
        {
            var max: Double = Double(dataArray[0])
             let maxBarHeight: Float = Float(kGraphHeight - kBarTop - kOffsetY);
            for var index = dataArray.count-1; index >= 0; index -= 1
            {
                //let index = barGraphArray.count-1
                if index > 1
                {
                    let next: Double = Double(dataArray[index])
                    
                    if max < next
                    {
                        max = next
                    }
                }
                
               // let width = self.frame.width
                let offSet = Int(kBarStepX)
                
                let barX:Float = Float( index  * offSet)+3;
                let barY:Float =  (maxBarHeight - maxBarHeight * Float(dataArray[index]));
                
               // let barHeight =  Float(HeartDataArray[index])+3;
                let barHeight =  Float(HeartDataArray[index]);
    
                let barRect:CGRect = CGRectMake(CGFloat(barX), CGFloat(barY), CGFloat(kBarWidth), CGFloat(barHeight));

                
                currentBarValue =  Double(HeartDataArray[index])
                barcolorwithvalue(context, a: currentBarValue)
                CGContextMoveToPoint(context, CGRectGetMinX(barRect), CGRectGetMinY(barRect));
                CGContextAddLineToPoint(context, CGRectGetMaxX(barRect), CGRectGetMinY(barRect));
                CGContextAddLineToPoint(context, CGRectGetMaxX(barRect), CGRectGetMaxY(barRect));
                CGContextAddArcToPoint(context, CGRectGetMinX(barRect), CGRectGetMaxY(barRect), CGRectGetMaxX(barRect), CGRectGetMaxY(barRect), 8.0);
                CGContextAddLineToPoint(context, CGRectGetMinX(barRect), CGRectGetMaxY(barRect));
                CGContextClosePath(context);
                CGContextFillPath(context);
            }
        }
    }

    //--------For color of bar
    
    func barcolorwithvalue(context:CGContextRef, a:Double)
    {
        if a<91
        {
            CGContextSetRGBFillColor(context,155/255.0,155/255.0,155/255.0,1)
        }
        else if a>=91 && a<=107
        {
            CGContextSetRGBFillColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if a>=108 && a<=125
        {
            CGContextSetRGBFillColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if a>=126 && a<=144
        {
            CGContextSetRGBFillColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if a>=145 && a<=168
        {
            CGContextSetRGBFillColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if a>=169
        {
            CGContextSetRGBFillColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
    }


    func drawLineGraphWithDotedPoint(context : CGContextRef)
    {
        CGContextSetLineWidth(context, 2.0);
        //        CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor);
        let maxGraphHeight :Float = kGraphHeight - kOffsetY;
        CGContextBeginPath(context);
        
        CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(kGraphHeight - maxGraphHeight * dataArray[0]));
        // var timeInSecs : Int = 0
        for index in 0 ..< dataArray.count
        {
            var x: Float = 0.0
            //            if HealthManager.sharedInstance.totalWorkoutTimeInterval == 1
            //            {
            x = kOffsetX + ((Float(index) * kStepX)/Helper.sharedInstance.averageTimeHeartRate)
            //            }
            var y: Float = kGraphHeight - maxGraphHeight * dataArray[index]
            
            if HealthManager.sharedInstance.historyWorkoutTimeType == 2
            {
                var i = index*15
                if i >= dataArray.count
                {
                    i = dataArray.count-1
                }
                y = kGraphHeight - maxGraphHeight * dataArray[i]
            }
            else if HealthManager.sharedInstance.historyWorkoutTimeType == 3
            {
                var i = index*450
                if i >= dataArray.count
                {
                    i = dataArray.count-1
                }
                y = kGraphHeight - maxGraphHeight * dataArray[i]
            }
            indexArray.append(index)
            CGContextAddLineToPoint(context, CGFloat(x), CGFloat(y))
            linewithcolorvalue(context, b: HeartDataArray[index])
            CGContextDrawPath(context, CGPathDrawingMode.Stroke);
            CGContextMoveToPoint(context, CGFloat(x), CGFloat(y));
            //            CGContextClosePath(context)
            
            //             CGContextBeginPath(context);
            //          CGContextMoveToPoint(context, CGFloat(x), CGFloat(y));
            
            //print(CGFloat(kOffsetX + ((Float(index) * kStepX)/60)), CGFloat(kGraphHeight - maxGraphHeight * dataArray[index]))
        }
      
        var count  = dataArray.count
        
        if HealthManager.sharedInstance.historyWorkoutTimeType == 2
        {
            count = count / 30
        }
        else if HealthManager.sharedInstance.historyWorkoutTimeType == 3
        {
            count = count / 60
        }
        for index in 0 ..< count-1 {
            var x: Float = 0.0
            var i = 0
            //            if HealthManager.sharedInstance.totalWorkoutTimeInterval == 1
            //            {
            x = kOffsetX + ((Float(index) * kStepX)/Helper.sharedInstance.averageTimeHeartRate)
            //            }
            //            else if HealthManager.sharedInstance.totalWorkoutTimeInterval == 2
            //            {
            //                x = (kOffsetX + ((Float(index) * kStepX)/2))/60
            //            }
            //            else if HealthManager.sharedInstance.totalWorkoutTimeInterval == 3
            //            {
            //                x = (kOffsetX + ((Float(index) * kStepX)/2))/3600
            //            }
            
            
            var y: Float = kGraphHeight - maxGraphHeight * dataArray[index]
            
            if HealthManager.sharedInstance.historyWorkoutTimeType == 2
            {
                i = index*15
                if i >= dataArray.count
                {
                    i = dataArray.count-1
                }
                y = kGraphHeight - maxGraphHeight * dataArray[i]
            }
            else if HealthManager.sharedInstance.historyWorkoutTimeType == 3
            {
                 i = index*450
                if i >= dataArray.count
                {
                    i = dataArray.count-1
                }
                y = kGraphHeight - maxGraphHeight * dataArray[i]
            }
            if index <= indexArray.count-1
            {
                circlecolor(context,  b: HeartDataArray[indexArray[index]])
            }
            else
            {
                 circlecolor(context,  b: HeartDataArray[i])
            }
            let rect: CGRect = CGRectMake(CGFloat(x - kCircleRadius), CGFloat(y - kCircleRadius), CGFloat(2 * kCircleRadius), CGFloat(2 * kCircleRadius))
            CGContextAddEllipseInRect(context, rect);
            CGContextDrawPath(context, CGPathDrawingMode.FillStroke);
        }
       
    }
    
    // ---- for creating text --------
    func createtext(context:CGContextRef)
    {
        CGContextSetTextMatrix(context, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0))
        CGContextSetFillColorWithColor(context, UIColor.blackColor().CGColor)
        
        // var mins :Int = 0
        // var hours:Int = 0
        for  subview:UIView in self.scrollView!.subviews
        {
            if subview.isKindOfClass(UILabel)
            {
                subview.removeFromSuperview()
            }
        }
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm:ss a"
        
        //        let calendar = NSCalendar.currentCalendar();
        //        let unitFlags: NSCalendarUnit = [.Hour, .Minute, .Second]
        //let components = calendar.components(unitFlags , fromDate: workoutStartDate)
        var hour = 0
        var mins = 0
        var secs = 0
        
        for i in 0 ..< numberOfVerticalLines+1
        {
            //  let kStepX1:CGFloat = 110
            let valueX: Float = (kOffsetX + Float(i) * kStepX);
            
            var label1 = UILabel()
            
            if i != 0
            {
                
                if HealthManager.sharedInstance.historyWorkoutTimeType == 1
                {
                    secs = secs+10
                    if secs<60
                    {
                        label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 30, 30))
                        label1.text = NSString(format:"%@%d%@", ":",secs,"s") as String
                        
                    }
                    else
                    {
                        secs = secs - 60
                        mins = mins+1
                        if mins > 60
                        {
                            mins = mins-60
                            hour = hour + 1
                            
                            if hour > 12
                            {
                                hour = hour - 12
                            }
                        }
                        label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 70, 30))
                        label1.text = NSString(format:"%d%@%d%@%d",hour,":",mins,":",secs) as String
                    }
                    
                }
                else if HealthManager.sharedInstance.historyWorkoutTimeType == 2
                {
                    mins = mins+10
                    if mins<60
                    {
                        label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 30, 30))
                        label1.text = NSString(format:"%@%d%@", ":",mins,"m") as String
                        
                    }
                    else
                    {
                        mins = mins - 60
                        hour = hour+1
                        if hour > 12
                        {
                            hour = hour-12
                            
                        }
                        label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 70, 30))
                        label1.text = NSString(format:"%d%@%d%@%d",hour,":",mins,":",secs) as String
                    }
                }
                else if HealthManager.sharedInstance.historyWorkoutTimeType == 3
                {
                    hour = hour+1
                    if hour > 12
                    {
                        hour = hour-12
                    }
                    label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 70, 30))
                    label1.text = NSString(format:"%d%@%d%@%d",hour,":",mins,":",secs) as String
                }
                
            }
            else
            {
                label1 = UILabel(frame: CGRectMake(CGFloat(valueX), self.frame.size.height+6, 70, 30))
                label1.text = NSString(format: "%d%@%d%@%d", hour,":",mins,":",secs) as String
            }
            label1.font = UIFont(name: "Helvetica", size:12)
            label1.backgroundColor = UIColor.clearColor()
            label1.textColor = UIColor.whiteColor()
            self.scrollView!.addSubview(label1)
            self.scrollView!.bringSubviewToFront(label1)
        }
    }
    
    func bottomline(context:CGContextRef)
    {
        CGContextSetLineWidth(context, 20)
       let ypoint = self.frame.size.height
        CGContextMoveToPoint(context, 0, ypoint)
        
        CGContextMoveToPoint(context, CGFloat(0), CGFloat(ypoint))
        CGContextAddLineToPoint(context, CGFloat(self.frame.width), CGFloat(ypoint))
        
        CGContextSetLineWidth(context, 1)
        
        CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
        
        CGContextStrokePath(context)
        
    }
    
    func setViewDimensions()
    {
        // let height:CGFloat = CGRectGetHeight(scrollView.frame);
        scrollView!.contentSize = CGSizeMake(CGFloat(kOffsetX + Float(dataArray.count) * kStepX), CGFloat(0))
        if(CGFloat(kOffsetX + Float(dataArray.count) * kStepX) > UIScreen.mainScreen().bounds.width-50){
            self.frame = CGRectMake(0, 0, CGFloat(kOffsetX + Float(dataArray.count) * kStepX), self.frame.height)
        }
    }
    
    
    func drawLineGraphWithFillColor(context : CGContextRef)
    {
        CGContextSetLineWidth(context, 2.0);
        CGContextSetStrokeColorWithColor(context, UIColor.redColor().CGColor);
        let maxGraphHeight :Float = kGraphHeight - kOffsetY;
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, CGFloat(kOffsetX),  CGFloat(kGraphHeight - maxGraphHeight * dataArray[0]));
        
        //------For Line
        for index in 0..<dataArray.count {
            CGContextAddLineToPoint(context, CGFloat(kOffsetX + Float(index) * kStepX), CGFloat(kGraphHeight - maxGraphHeight * dataArray[index]));
        }
        CGContextDrawPath(context, CGPathDrawingMode.Stroke);
        
        //------For Dot
        CGContextSetFillColorWithColor(context, UIColor.redColor().CGColor);
        for index in 0..<dataArray.count-1{
            let x: Float = kOffsetX + Float(index) * kStepX;
            let y: Float = kGraphHeight - maxGraphHeight * dataArray[index];
            let rect: CGRect = CGRectMake(CGFloat(x - kCircleRadius), CGFloat(y - kCircleRadius), CGFloat(2 * kCircleRadius), CGFloat(2 * kCircleRadius));
            CGContextAddEllipseInRect(context, rect);
        }
        CGContextDrawPath(context, CGPathDrawingMode.FillStroke);
        
        //-------For Fill
        CGContextSetFillColorWithColor(context, UIColor.lightGrayColor().CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, CGFloat(kOffsetX), CGFloat(kGraphHeight));
        CGContextAddLineToPoint(context, CGFloat(kOffsetX), CGFloat(kGraphHeight - maxGraphHeight * dataArray[0]));
        for index in 0..<dataArray.count{
            CGContextAddLineToPoint(context, CGFloat(kOffsetX + Float(index) * kStepX), CGFloat(kGraphHeight - maxGraphHeight * dataArray[index]));
        }
        CGContextAddLineToPoint(context, CGFloat(kOffsetX + Float(dataArray.count - 1) * kStepX), CGFloat(kGraphHeight));
        CGContextClosePath(context);
        
        CGContextDrawPath(context, CGPathDrawingMode.Fill);
    }
    
    //-------------For line and dots color
    
    func linewithcolorvalue(context:CGContextRef, b:Double)
    {
        if b <= 90.0
        {
            CGContextSetRGBStrokeColor(context,1,1,1,1.0)
        }
        else if b >= 91.0 && b <= 107.0
        {
            CGContextSetRGBStrokeColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if b >= 108.0 && b <= 125.0
        {
            CGContextSetRGBStrokeColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if b >= 126.0 && b <= 144.0
        {
            CGContextSetRGBStrokeColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if b >= 145.0 && b <= 168.0
        {
            CGContextSetRGBStrokeColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if b >= 169.0
        {
            CGContextSetRGBStrokeColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
    }
    
    func circlecolor(context:CGContextRef, b:Double)
    {
        if b <= 91.0
        {
            CGContextSetRGBStrokeColor(context,1,1,1,1)
        }
        else if b >= 92.0 && b <= 107.0
        {
            CGContextSetRGBStrokeColor(context,33/255.0,125/255.0,168/255.0,1.0)
        }
        else if b >= 108.0 && b <= 125.0
        {
            CGContextSetRGBStrokeColor(context,51/255.0,199/255.0,231/255.0,1.0)
        }
        else if b >= 126.0 && b <= 144.0
        {
            CGContextSetRGBStrokeColor(context,243/255.0,226/255.0,28/255.0,1.0)
        }
        else if b >= 145.0 && b <= 168.0
        {
            CGContextSetRGBStrokeColor(context,225/255.0,152/255.0,60/255.0,1.0)
        }
        else if b >= 169.0
        {
            CGContextSetRGBStrokeColor(context,211/255.0,0/255.0,69/255.0,1.0)
        }
    }
    
    //testing
    func drawLine(context : CGContextRef){
    
    CGContextSetLineWidth(context, 2.0)
    
    let maxGraphHeight :Float = kGraphHeight - kOffsetY;
    
    
    for i in 0..<dataArray.count{
    
    
    linewithcolorvalue(context, b: Double(HeartDataArray[i]))
    drawline1(context, x: CGFloat(kOffsetX + ((Float(i) * kStepX)/Helper.sharedInstance.averageTimeHeartRate)), y: CGFloat(kGraphHeight - maxGraphHeight * dataArray[i]),m: i)
    
    
    
    }
    //             CGContextSetStrokeColorWithColor(context, UIColor.greenColor().CGColor);
    
    CGContextDrawPath(context, CGPathDrawingMode.Stroke)
    
    
    }
    
    func drawline1(context:CGContextRef, x:CGFloat,y:CGFloat,m:Int)
    {
      //  var maxGraphHeight :Float = kGraphHeight - kOffsetY;
//        if(index != 0)
        
        CGContextBeginPath(context);
        CGContextMoveToPoint(context,x,  y);
        CGContextAddLineToPoint(context, x, y);
        CGContextClosePath(context);
        
    }

    
}
