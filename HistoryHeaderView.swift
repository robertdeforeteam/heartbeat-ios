//
//  HistoryHeaderView.swift
//  HeartBeat
//
//  Created by Rupinder-Mac on 11/19/15.
//  Copyright © 2015 Deep-MacMini. All rights reserved.
//

import UIKit

class HistoryHeaderView: UITableViewCell {

    @IBOutlet var headerLbl : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
